/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-maintenance-response')
        .config(routes);

    routes.$inject = ['$stateProvider'];

    function routes($stateProvider) {
        var dialog;

        $stateProvider.state('openlmis.equipment.maintenance.response', {
            onEnter: onEnter,
            onExit: onExit,
            params: {
                maintenanceItemId: undefined,
                maintenanceItem: undefined
            },
            isOffline: true,
            url: '/details'
        });

        onEnter.$inject = ['openlmisModalService'];
        function onEnter(openlmisModalService) {
            dialog = openlmisModalService.createDialog({
                controller: 'EquipmentMaintenanceResponseController',
                controllerAs: 'vm',
                resolve: {
                    inventoryItem: function() {
                        return null;
                    }
                },
                templateUrl: 'equipment-maintenance-response/response.html'
            });
        }

        function onExit() {
            dialog.hide();
            dialog = undefined;
        }

    }

})();
