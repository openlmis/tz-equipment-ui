/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-maintenance-response.controller:EquipmentMaintenanceResponseController
     *
     * @description
     * Exposes equipment maintenance response to the view.
     */
    angular
        .module('equipment-maintenance-response')
        .controller('EquipmentMaintenanceResponseController', EquipmentMaintenanceResponseController);

    EquipmentMaintenanceResponseController.$inject = [
        'inventoryItem', '$state', 'loadingModalService', 'stateTrackerService',
        'notificationService', 'inventoryItemService', 'FUNCTIONAL_STATUS', '$filter'
    ];

    function EquipmentMaintenanceResponseController(inventoryItem, $state, loadingModalService,
                                                    stateTrackerService, notificationService,
                                                    inventoryItemService, FUNCTIONAL_STATUS, $filter) {
        var vm = this;

        vm.$onInit = onInit;
        vm.isEquipmentFunctioning = isEquipmentFunctioning;
        vm.toNames = toNames;
        vm.getFunctionalStatusLabel = getFunctionalStatusLabel;
        vm.save = save;

        /**
         * @ngdoc property
         * @methodOf equipment-maintenance-response.controller:EquipmentMaintenanceResponseController
         * @name maintenanceResponse
         * @type Object
         *
         * @description
         * Init method for EquipmentMaintenanceResponseController.
         */
        vm.maintenanceResponse = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-response.controller:EquipmentMaintenanceResponseController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentMaintenanceResponseController.
         */
        function onInit() {
            //Set the max-date of date picker to the end of the current day.
            vm.minDate = new Date();
            vm.minDate.setHours(23, 59, 59, 999);
            vm.maintenanceItem = $state.params.maintenanceItem;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-response.controller:EquipmentMaintenanceResponseController
         * @name save
         *
         * @description
         * Updates the equipment maintenance response and saves it on the server.
         */
        function save() {
            var loadingPromise = loadingModalService.open();

            var responseObject;
            responseObject = angular.copy(vm.maintenanceResponse);
            responseObject.request = angular.copy(vm.maintenanceItem);
            responseObject.resolved = true;
            responseObject.maintenanceDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            inventoryItemService.saveMaintenanceResponse(responseObject)
                .then(function(response) {
                    loadingPromise.then(function() {
                        notificationService
                            .success('equipmentInventoryItemDetails.maintenanceResponseSaved');
                    });
                    stateTrackerService.goToPreviousState('openlmis.equipment.maintenance', {
                        inventoryItem: response,
                        inventoryItemId: response.id
                    });
                }, loadingModalService.close);
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-response.controller:EquipmentMaintenanceResponseController
         * @name toNames
         *
         * @description
         * Convert array object to their names comma separated.
         */
        function toNames(entries) {
            return entries.map(function(item) {
                return item.name;
            }).join(',');
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-response.controller:EquipmentMaintenanceResponseController
         * @name getFunctionalStatusLabel
         *
         * @description
         * Returns a functional status label for the inventory item.
         *
         * @return  {String}    the label for the inventory item
         */
        function getFunctionalStatusLabel() {
            return FUNCTIONAL_STATUS.getLabel(vm.maintenanceItem.inventory.functionalStatus);
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-response.controller:EquipmentMaintenanceResponseController
         * @name isEquipmentFunctioning
         *
         * @description
         * Checks whether the given equipment status is functioning.
         *
         * @return  {Boolean}           true if the status is functioning, false otherwise
         */
        function isEquipmentFunctioning() {
            return vm.maintenanceItem.inventory.functionalStatus === FUNCTIONAL_STATUS.FUNCTIONING;
        }
    }
})();
