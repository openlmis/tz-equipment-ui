/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('openlmis.inventoryItem state', function() {

    var $state, paginationService, facilityInventoryItemFactory, $rootScope, EQUIPMENT_RIGHTS, state,
        permissionService, InventoryItemDataBuilder, inventoryItems, equipmentTypes, $stateParams,
        equipmentTypeService, EquipmentTypeDataBuilder;

    beforeEach(function() {
        module('openlmis-main-state');
        module('equipment-inventory-list');

        inject(function($injector) {
            $state = $injector.get('$state');
            $rootScope = $injector.get('$rootScope');
            EQUIPMENT_RIGHTS = $injector.get('EQUIPMENT_RIGHTS');
            facilityInventoryItemFactory = $injector.get('facilityInventoryItemFactory');
            equipmentTypeService = $injector.get('equipmentTypeService');
            paginationService = $injector.get('paginationService');
            permissionService = $injector.get('permissionService');
            InventoryItemDataBuilder = $injector.get('InventoryItemDataBuilder');
            EquipmentTypeDataBuilder = $injector.get('EquipmentTypeDataBuilder');
        });

        inventoryItems = [
            new InventoryItemDataBuilder().build(),
            new InventoryItemDataBuilder().build(),
            new InventoryItemDataBuilder().build()
        ];

        equipmentTypes = [
            new EquipmentTypeDataBuilder().build(),
            new EquipmentTypeDataBuilder().build()
        ];

        spyOn(facilityInventoryItemFactory, 'query').andReturn(inventoryItems);
        spyOn(equipmentTypeService, 'query').andReturn(equipmentTypes);
        spyOn(permissionService, 'hasPermissionWithAnyProgramAndAnyFacility');

        spyOn(paginationService, 'registerUrl').andCallFake(function(stateParams, method) {
            return method(stateParams);
        });

        state = $state.get('openlmis.equipment.inventory');

        $stateParams = {
            program: undefined,
            facility: undefined,
            page: 0,
            size: 10
        };
    });

    it('should use equipment-inventory-list.html template', function() {
        expect(state.templateUrl).toEqual('equipment-inventory-list/equipment-inventory-list.html');
    });

    it('should expose controller as vm', function() {
        expect(state.controllerAs).toEqual('vm');
    });

    it('should expose EquipmentInventoryListController', function() {
        expect(state.controller).toEqual('EquipmentInventoryListController');
    });

    it('should fetch a list of inventory items', function() {
        $stateParams.program = 'program-id';
        $stateParams.facility = 'facility-id';

        var result = state.resolve.inventoryItems(
            facilityInventoryItemFactory, paginationService, $stateParams
        );
        $rootScope.$apply();

        expect(result).toEqual(inventoryItems);
        expect(facilityInventoryItemFactory.query).toHaveBeenCalled();
    });

    it('should require EQUIPMENT_INVENTORY_EDIT and EQUIPMENT_INVENTORY_EDIT rights to enter', function() {
        expect(state.accessRights).toEqual([
            EQUIPMENT_RIGHTS.EQUIPMENT_INVENTORY_VIEW,
            EQUIPMENT_RIGHTS.EQUIPMENT_INVENTORY_EDIT
        ]);
    });

});
