/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-orderable-add')
        .config(routes);

    routes.$inject = ['modalStateProvider'];

    function routes(modalStateProvider) {

        modalStateProvider.state('openlmis.administration.equipmentsOrderable.add', {
            controller: 'EquipmentOrderableAddController',
            controllerAs: 'vm',
            resolve: {
                equipmentsOrderable: equipmentOrderableResolve,
                orderables: orderablesResolve,
                equipments: equipmentsResolve

            },
            parentResolves: [],
            templateUrl: 'equipment-orderable-add/equipment-orderable-add.html',
            url: '/'
        });
    }

    equipmentOrderableResolve.$inject = ['$stateParams'];
    function equipmentOrderableResolve($stateParams) {
        if ($stateParams.equipmentsOrderable) {
            return $stateParams.equipmentsOrderable;
        }
        return {};
    }

    orderablesResolve.$inject = ['equipmentOrderableService'];
    function orderablesResolve(equipmentOrderableService) {
        return equipmentOrderableService.getOrderables()
            .then(function(response) {
                return response.content;
            });
    }
    equipmentsResolve.$inject = ['equipmentService'];
    function equipmentsResolve(equipmentService) {
        return equipmentService.query()
            .then(function(response) {
                return response.content;
            });
    }

})();
