/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-orderable-add:EquipmentOrderableAddController
     *
     * @description
     * Provides methods for Add Equipment Type. Allows returning to previous states and saving
     * equipment type.
     */
    angular
        .module('equipment-orderable-add')
        .controller('EquipmentOrderableAddController', EquipmentOrderableAddController);

    EquipmentOrderableAddController.$inject = [
        'equipmentsOrderable', '$state', '$window', 'stateTrackerService', 'loadingModalService',
        'equipmentOrderableService', 'notificationService', 'orderables', 'equipments'
    ];

    function EquipmentOrderableAddController(equipmentsOrderable, $state, $window, stateTrackerService,
                                             loadingModalService, equipmentOrderableService,
                                             notificationService, orderables, equipments) {

        var vm = this;

        vm.$onInit = onInit;
        vm.save = save;
        vm.equipmentsOrderable = null;
        vm.orderables = null;
        vm.equipments = null;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;

        /**
         * @ngdoc method
         * @methodOf equipment-orderable-add:EquipmentOrderableAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentTypeAddController.
         */
        function onInit() {
            vm.equipmentsOrderable = equipmentsOrderable;
            vm.orderables = orderables;
            vm.equipments = equipments;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-orderable-add:EquipmentOrderableAddController
         * @name save
         *
         * @description
         * Saves the equipment type and takes user back to the previous state.
         */
        function save() {
            loadingModalService.open();
            return new equipmentOrderableService.save(vm.equipmentsOrderable)
                .then(function(equipmentsOrderable) {
                    notificationService.success('equipmentOrderable.typeHasBeenSaved');
                    stateTrackerService.goToPreviousState();
                    return equipmentsOrderable;
                })
                .catch(function() {
                    notificationService.error('equipmentOrderable.failedToSave');
                    loadingModalService.close();
                });
        }

    }

})();
