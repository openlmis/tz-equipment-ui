/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('equipmentCategoryService', function() {

    var $rootScope, $httpBackend;
    var equipmentUrlFactory, equipmentCategoryService, EquipmentCategoryDataBuilder;
    var equipmentCategories, equipmentCategory, EQUIPMENT_CATEGORY_ID;

    beforeEach(function() {
        module('equipment-category-list');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            equipmentUrlFactory = $injector.get('equipmentUrlFactory');
            equipmentCategoryService = $injector.get('equipmentCategoryService');
            EquipmentCategoryDataBuilder = $injector.get('EquipmentCategoryDataBuilder');
        });

        prepareTestData();
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentCategories/' + equipmentCategories[0].id))
                .respond(200, equipmentCategories[0]);
        });

        it('should return promise', function() {
            var result = equipmentCategoryService.get(equipmentCategories[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentCategory', function() {
            var result;

            equipmentCategoryService.get(equipmentCategories[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentCategories[0]);

            expect(angular.toJson(result)).toEqual(angular.toJson(expected));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentCategories/' + equipmentCategories[0].id));

            equipmentCategoryService.get(equipmentCategories[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var parameters = {
            page: 0,
            size: 10
        };

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentCategories?page=' + parameters.page +
                '&size=' + parameters.size)).respond(200, {
                content: equipmentCategories
            });
        });

        it('should return promise', function() {
            var result = equipmentCategoryService.getAll(parameters);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentCategories', function() {
            var result;

            equipmentCategoryService.getAll(parameters).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentCategories);

            expect(angular.toJson(result)).toEqual(angular.toJson({
                content: expected
            }));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentCategories?page=' + parameters.page +
                '&size=' + parameters.size));

            equipmentCategoryService.getAll(parameters);
            $httpBackend.flush();
        });
    });

    describe('save', function() {

        var result;

        beforeEach(function() {
            result = undefined;
        });

        it('should create equipmentCategory if ID is not given', function() {
            equipmentCategory = new EquipmentCategoryDataBuilder().withoutId()
                .build();

            var returned = angular.copy(equipmentCategory);

            returned.id = EQUIPMENT_CATEGORY_ID;

            $httpBackend.expect(
                'POST', equipmentUrlFactory('/api/equipmentCategories'), equipmentCategory
            ).respond(200, returned);

            equipmentCategoryService.save(equipmentCategory).then(function(equipmentCategory) {
                result = equipmentCategory;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(returned));
        });

        it('should update equipmentCategory if it has ID', function() {
            equipmentCategory = new EquipmentCategoryDataBuilder().withId(EQUIPMENT_CATEGORY_ID)
                .build();

            $httpBackend.expect(
                'PUT', equipmentUrlFactory('/api/equipmentCategories/' + EQUIPMENT_CATEGORY_ID), equipmentCategory
            ).respond(200, equipmentCategory);

            equipmentCategoryService.save(equipmentCategory).then(function(equipmentCategory) {
                result = equipmentCategory;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(equipmentCategory));
        });

    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    function prepareTestData() {
        EQUIPMENT_CATEGORY_ID = 'some-equipment-category-id';

        equipmentCategories = [
            new EquipmentCategoryDataBuilder().withId('1')
                .build(),
            new EquipmentCategoryDataBuilder().withId('2')
                .build()
        ];
    }
});
