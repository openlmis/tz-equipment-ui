/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-inventory-log.controller:EquipmentInventoryLogController
     *
     * @description
     * Manages role template selection screen.
     */
    angular
        .module('equipment-inventory-log')
        .controller('EquipmentInventoryLogController', controller);

    controller.$inject = ['$state', 'inventoryLogs', 'confirmService', 'loadingModalService',
        'notificationService', 'stateTrackerService', 'inventoryItemService', '$q', '$timeout'];

    function controller($state, inventoryLogs, confirmService, loadingModalService,
                        notificationService, stateTrackerService, inventoryItemService,
                        $q, $timeout) {
        var vm = this;

        vm.$onInit = onInit;
        vm.approve = approve;
        vm.reject = reject;

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-log.controller:EquipmentInventoryLogController
         * @name inventoryLogs
         * @type {Array}
         *
         * @description
         * Init method for EquipmentInventoryLogController.
         */
        vm.inventoryLogs = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-log.controller:EquipmentInventoryLogController
         * @name headerInfo
         * @type {Object}
         *
         * @description
         * Init method for EquipmentInventoryLogController.
         */
        vm.headerInfo = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-log.controller:EquipmentInventoryLogController
         * @name onInit
         *
         * @description
         * Init method for EquipmentInventoryLogController.
         */
        function onInit() {
            vm.inventoryLogs = inventoryLogs.map(function(item) {
                var status;
                if ('approved' in item) {
                    status = item.approved ? 'Approved' : 'Rejected';
                } else {
                    status = 'Pending Approval';
                }

                item.status = status;
                return item;
            });

            $timeout(function() {

                loadHeaderDetails().then(function(data) {

                    if (data.length > 0) {
                        vm.headerInfo = data[0];
                    }
                });

            }, 100);
        }

        /**
         * @ngdoc property
         * @methodOf equipment-inventory-log.controller:EquipmentInventoryLogController
         * @name approve
         *
         * @description
         */
        function approve(log) {
            confirmService.confirm('equipmentInventoryLog.approve.confirmationQuestion', 'equipmentInventoryLog.save')
                .then(function() {
                    loadingModalService.open();
                    inventoryItemService.approveMaintenanceLog(log).then(function(response) {
                        notificationService.success('equipmentInventoryLog.approved.success');
                        loadingModalService.close();
                        stateTrackerService.goToPreviousState('openlmis.equipment.inventory.log', {
                            equipmentInventoryId: response.id
                        });
                    })
                        .catch(function() {
                            loadingModalService.close();
                            notificationService.error('equipmentInventoryLog.approve.failure');
                        });
                });
        }

        /**
         * @ngdoc property
         * @methodOf equipment-inventory-log.controller:EquipmentInventoryLogController
         * @name reject
         *
         * @description
         */
        function reject(log) {
            confirmService.confirm('equipmentInventoryLog.reject.confirmationQuestion', 'equipmentInventoryLog.reject')
                .then(function() {
                    loadingModalService.open();
                    inventoryItemService.rejectMaintenanceLog(log).then(function(response) {
                        notificationService.success('equipmentInventoryLog.reject.success');
                        loadingModalService.close();
                        stateTrackerService.goToPreviousState('openlmis.equipment.inventory.log', {
                            equipmentInventoryId: response.id
                        });
                    })
                        .catch(function() {
                            loadingModalService.close();
                            notificationService.error('equipmentInventoryLog.reject.failure');
                        });
                });
        }

        /**
         * @ngdoc property
         * @methodOf equipment-inventory-log.controller:EquipmentInventoryLogController
         * @name reject
         *
         * @description
         */
        function loadHeaderDetails() {

            var deferred = $q.defer();

            var inventories = angular.copy(vm.inventoryLogs);

            var responseData = [];
            angular.forEach(inventories, function(request) {
                request.facility = {};
                inventoryItemService.getByFacilityId(request.request.inventory.facilityId)
                    .then(function(fac) {
                        request.facility = fac;
                    });
                responseData.push(request);
            });
            deferred.resolve(responseData);

            return deferred.promise;
        }
    }

})();