/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-contract.EquipmentContract
     *
     * @description
     * Represents a single stock equipmentContract.
     */
    angular
        .module('equipment-contract')
        .factory('EquipmentContract', EquipmentContract);

    EquipmentContract.$inject = ['$q'];

    function EquipmentContract($q) {

        EquipmentContract.prototype.addFacility = addFacility;
        EquipmentContract.prototype.removeFacility = removeFacility;
        EquipmentContract.prototype.validateContractFacilityExists = validateContractFacilityExists;
        EquipmentContract.prototype.isContractFacilityDuplicated = isContractFacilityDuplicated;
        EquipmentContract.prototype.validateContractFacilityDoesNotExist = validateContractFacilityDoesNotExist;
        EquipmentContract.prototype.addEquipment = addEquipment;
        EquipmentContract.prototype.removeEquipment = removeEquipment;
        EquipmentContract.prototype.validateContractEquipmentExists = validateContractEquipmentExists;
        EquipmentContract.prototype.isContractEquipmentDuplicated = isContractEquipmentDuplicated;
        EquipmentContract.prototype.validateContractEquipmentDoesNotExist = validateContractEquipmentDoesNotExist;
        EquipmentContract.prototype.addServiceType = addServiceType;
        EquipmentContract.prototype.removeServiceType = removeServiceType;
        EquipmentContract.prototype.validateContractServiceTypeExists = validateContractServiceTypeExists;
        EquipmentContract.prototype.isContractServiceTypeDuplicated = isContractServiceTypeDuplicated;
        EquipmentContract.prototype.validateContractServiceTypeDoesNotExist = validateContractServiceTypeDoesNotExist;
        return EquipmentContract;

        /**
         * @ngdoc method
         * @methodOf equipment-contract.EquipmentContract
         * @name EquipmentContract
         *
         * @description
         * Creates a new instance of the EquipmentContract class.
         *
         * @param  {Object}                json       the JSON representation of the EquipmentContract
         * @return {EquipmentContract}                           the EquipmentContract object
         */
        function EquipmentContract(json) {
            if (!json) {
                json = {};
            }

            this.id = json.id;
            this.identifier = json.identifier;
            this.startDate = json.startDate;
            this.endDate = json.endDate;
            this.description = json.description;
            this.terms = json.terms;
            this.coverage = json.coverage;
            this.date = json.date;
            this.vendor = json.vendor;
            this.facilities = json.facilities ? json.facilities : [];
            this.equipments = json.equipments ? json.equipments : [];
            this.serviceTypes = json.serviceTypes ? json.serviceTypes : [];
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract.EquipmentContract
         * @name addFacility
         *
         * @description
         * Adds the given facility to the list of equipmentContract facility. Also stores information if
         * the facility has been previously removed and not saved. If an already existing
         * facility is added this method will return a rejected promise.
         *
         * @param  {Object}  facility the facility to add
         * @return {Promise}            the promise resolved when facility was successfully added
         */
        function addFacility(facility) {
            var error = this.validateContractFacilityDoesNotExist(facility);
            if (error) {
                return $q.reject(error);
            }

            this.facilities.push(facility);

            return $q.resolve();
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract.EquipmentContract
         * @name removeFacility
         *
         * @description
         * Removes the given facility from the list of equipmentContract facilities. Also stores information
         * if the facility has been previously added and not saved.
         *
         * @param {Object} facility the facility to be removed
         */
        function removeFacility(facility) {
            var error = this.validateContractFacilityExists(facility);
            if (error) {
                return $q.reject(error);
            }

            this.facilities.splice(this.facilities.indexOf(facility), 1);
            return $q.resolve();
        }

        function validateContractFacilityDoesNotExist(facility) {
            if (this.isContractFacilityDuplicated(facility)) {
                return 'equipmentContractAdd.facilityDuplicated';
            }
        }

        function validateContractFacilityExists(facility) {
            if (this.facilities.indexOf(facility) === -1) {
                return 'equipmentContractAdd.facilityNotAdded';
            }
        }

        function isContractFacilityDuplicated(newFacility) {
            return this.facilities.filter(function(facility) {
                return facility === newFacility;
            }).length;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract.EquipmentContract
         * @name addEquipment
         *
         * @description
         * Adds the given equipment to the list of equipmentContract equipment. Also stores information if
         * the equipment has been previously removed and not saved. If an already existing
         * equipment is added this method will return a rejected promise.
         *
         * @param  {Object}  equipment the equipment to add
         * @return {Promise}            the promise resolved when equipment was successfully added
         */
        function addEquipment(equipment) {
            var error = this.validateContractEquipmentDoesNotExist(equipment);
            if (error) {
                return $q.reject(error);
            }

            this.equipments.push(equipment);

            return $q.resolve();
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract.EquipmentContract
         * @name removeEquipment
         *
         * @description
         * Removes the given equipment from the list of equipmentContract equipments. Also stores information
         * if the equipment has been previously added and not saved.
         *
         * @param {Object} equipment the equipment to be removed
         */
        function removeEquipment(equipment) {
            var error = this.validateContractEquipmentExists(equipment);
            if (error) {
                return $q.reject(error);
            }

            var index = this.equipments.findIndex(function(existing) {
                return existing.id === equipment.id;
            });
            this.equipments.splice(index, 1);
            return $q.resolve();
        }

        function validateContractEquipmentDoesNotExist(equipment) {
            if (this.isContractEquipmentDuplicated(equipment)) {
                return 'equipmentContractAdd.equipmentDuplicated';
            }
        }

        function validateContractEquipmentExists(equipment) {
            var index = this.equipments.findIndex(function(existing) {
                return existing.id === equipment.id;
            });
            if (index === -1) {
                return 'equipmentContractAdd.equipmentNotAdded';
            }
        }

        function isContractEquipmentDuplicated(newEquipment) {
            return this.equipments.filter(function(equipment) {
                return equipment.id === newEquipment.id;
            }).length;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract.EquipmentContract
         * @name addServiceType
         *
         * @description
         * Adds the given serviceType to the list of equipmentContract serviceType. Also stores information if
         * the serviceType has been previously removed and not saved. If an already existing
         * serviceType is added this method will return a rejected promise.
         *
         * @param  {Object}  serviceType the serviceType to add
         * @return {Promise}            the promise resolved when serviceType was successfully added
         */
        function addServiceType(serviceType) {
            var error = this.validateContractServiceTypeDoesNotExist(serviceType);
            if (error) {
                return $q.reject(error);
            }

            this.serviceTypes.push(serviceType);

            return $q.resolve();
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract.EquipmentContract
         * @name removeServiceType
         *
         * @description
         * Removes the given serviceType from the list of equipmentContract serviceTypes. Also stores information
         * if the serviceType has been previously added and not saved.
         *
         * @param {Object} serviceType the serviceType to be removed
         */
        function removeServiceType(serviceType) {
            var error = this.validateContractServiceTypeExists(serviceType);
            if (error) {
                return $q.reject(error);
            }

            var index = this.serviceTypes.findIndex(function(existing) {
                return existing.id === serviceType.id;
            });
            this.serviceTypes.splice(index, 1);
            return $q.resolve();
        }

        function validateContractServiceTypeDoesNotExist(serviceType) {
            if (this.isContractServiceTypeDuplicated(serviceType)) {
                return 'equipmentContractAdd.serviceTypeDuplicated';
            }
        }

        function validateContractServiceTypeExists(serviceType) {
            var index = this.serviceTypes.findIndex(function(existing) {
                return existing.id === serviceType.id;
            });
            if (index === -1) {
                return 'equipmentContractAdd.serviceTypeNotAdded';
            }
        }

        function isContractServiceTypeDuplicated(newServiceType) {
            return this.serviceTypes.filter(function(serviceType) {
                return serviceType.id === newServiceType.id;
            }).length;
        }
    }

})();
