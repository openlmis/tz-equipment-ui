/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentContract', function() {

    var EquipmentContractDataBuilder, EquipmentContract, equipmentContract, json, $rootScope;

    beforeEach(function() {
        module('equipment-contract');

        inject(function($injector) {
            EquipmentContract = $injector.get('EquipmentContract');
            EquipmentContractDataBuilder = $injector.get('EquipmentContractDataBuilder');
            $rootScope = $injector.get('$rootScope');
        });

        json = new EquipmentContractDataBuilder().buildJson();
    });

    describe('constructor', function() {

        it('should set properties if json is given', function() {
            var result = new EquipmentContract(json);

            expect(result.id).toEqual(json.id);
            expect(result.identifier).toEqual(json.identifier);
            expect(result.facilities).toEqual(json.facilities);
        });

        it('should set defaults if json is not given', function() {
            var result = new EquipmentContract();

            expect(result.id).toBeUndefined();
            expect(result.identifier).toBeUndefined();
            expect(result.facilities).toEqual([]);
        });

    });

    describe('addFacilities', function() {

        beforeEach(function() {
            equipmentContract = new EquipmentContract(json);
        });

        it('should reject if facility already exist', function() {
            var result;
            equipmentContract.addFacility(equipmentContract.facilities[0])
                .catch(function(error) {
                    result = error;
                });
            $rootScope.$apply();

            expect(result).toEqual('equipmentContractAdd.facilityDuplicated');
        });

        it('should add facility if it is not duplicate', function() {
            var facility = 'new-facility-1';

            equipmentContract.addFacility(facility);
            $rootScope.$apply();

            expect(equipmentContract.facilities.length).toBe(3);
            expect(equipmentContract.facilities[2]).toEqual(facility);
        });

    });

    describe('removeFacility', function() {

        beforeEach(function() {
            equipmentContract = new EquipmentContract(json);
        });

        it('should resolve when facility was remove successfully', function() {
            equipmentContract.removeFacility(equipmentContract.facilities[0]);
            $rootScope.$apply();

            expect(equipmentContract.facilities.length).toBe(1);
        });

        it('should return resolved promise if remove was successful', function() {
            var resolved;
            equipmentContract.removeFacility(equipmentContract.facilities[0])
                .then(function() {
                    resolved = true;
                });
            $rootScope.$apply();

            expect(resolved).toBe(true);
        });

        it('should return rejected promise if trying to remove non-existent facilities', function() {
            var result;
            equipmentContract.removeFacility('new-facility-99')
                .catch(function(error) {
                    result = error;
                });
            $rootScope.$apply();

            expect(result).toBe('equipmentContractAdd.facilityNotAdded');
        });

    });

});