/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-equipment-contract.EquipmentContractService
     *
     * @description
     * Responsible for retrieving all equipment contract information from server.
     */
    angular
        .module('equipment-contract')
        .service('equipmentContractService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/contracts/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/contracts'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/contracts/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;
        this.save = save;

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-contract.EquipmentContractService
         * @name get
         *
         * @description
         * Retrieves equipment contract by id.
         *
         * @param  {String}  equipmentContractId equipmentContract UUID
         * @return {Promise}                equipmentContract promise
         */
        function get(equipmentContractId) {
            return resource.get({
                id: equipmentContractId
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-contract.EquipmentContractService
         * @name query
         *
         * @description
         * Retrieves all equipment contracts by ids.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Page of equipment contracts
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-contract.EquipmentContractService
         * @name update
         *
         * @description
         * Updates existing equipment contract.
         *
         * @param  {Object}  equipmentContract equipment contract that will be saved
         * @return {Promise}              updated equipment contract
         */
        function update(equipmentContract) {
            return resource.update({
                id: equipmentContract.id
            }, equipmentContract).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-contract.EquipmentContractService
         * @name create
         *
         * @description
         * Creates new equipment contract.
         *
         * @param  {Object}  equipmentContract equipment contract that will be created
         * @return {Promise}              created equipment contract
         */
        function create(equipmentContract) {
            return resource.save(equipmentContract).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-contract.EquipmentContractService
         * @name save
         *
         * @description
         * Saves equipmentContract in the repository.
         *
         * @param  {Object}  equipmentContract equipment contract that will be created
         * @return {Promise} the promise resolving to saved EquipmentContract, rejected if save was unsuccessful
         */
        function save(equipmentContract) {
            if (equipmentContract.id) {
                return update(equipmentContract);
            }
            return create(equipmentContract);
        }
    }
})();
