/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-contract')
        .factory('EquipmentContractDataBuilder', EquipmentContractDataBuilder);

    EquipmentContractDataBuilder.$inject = ['EquipmentContract'];

    function EquipmentContractDataBuilder(EquipmentContract) {

        EquipmentContractDataBuilder.prototype.build = build;
        EquipmentContractDataBuilder.prototype.buildJson = buildJson;
        EquipmentContractDataBuilder.prototype.buildResponse = buildResponse;
        EquipmentContractDataBuilder.prototype.withId = withId;
        EquipmentContractDataBuilder.prototype.withFacilities = withFacilities;
        EquipmentContractDataBuilder.prototype.withoutId = withoutId;

        return EquipmentContractDataBuilder;

        function EquipmentContractDataBuilder() {
            EquipmentContractDataBuilder.instanceNumber = (EquipmentContractDataBuilder.instanceNumber || 0) + 1;

            this.id = 'equipmentContract-id' + EquipmentContractDataBuilder.instanceNumber;
            this.identifier = 'Identifier' + EquipmentContractDataBuilder.instanceNumber;
            this.facilities = ['facility-1', 'facility-2'];
        }

        function build() {
            return new EquipmentContract(this.buildJson());
        }

        function buildJson() {
            var json = this.buildResponse();

            json.facilities = this.facilities;

            return json;
        }

        function buildResponse() {
            return {
                id: this.id,
                identifier: this.identifier
            };
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withFacilities(facilities) {
            this.facilities = facilities;
            return this;
        }

        function withoutId() {
            this.id = undefined;
            return this;
        }

    }

})();
