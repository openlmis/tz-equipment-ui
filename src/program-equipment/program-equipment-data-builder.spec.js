/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('program-equipment')
        .factory('ProgramEquipmentTypeDataBuilder', ProgramEquipmentTypeDataBuilder);

    ProgramEquipmentTypeDataBuilder.$inject = ['ProgramEquipment'];

    function ProgramEquipmentTypeDataBuilder(ProgramEquipment) {

        ProgramEquipmentTypeDataBuilder.prototype.build = build;
        ProgramEquipmentTypeDataBuilder.prototype.buildJson = buildJson;
        ProgramEquipmentTypeDataBuilder.prototype.withId = withId;
        ProgramEquipmentTypeDataBuilder.prototype.withEnableTestCount = withEnableTestCount;
        ProgramEquipmentTypeDataBuilder.prototype.withDisplayOrder = withDisplayOrder;
        ProgramEquipmentTypeDataBuilder.prototype.withEnableTotalColumn = withEnableTotalColumn;
        ProgramEquipmentTypeDataBuilder.prototype.withProgramId = withProgramId;
        ProgramEquipmentTypeDataBuilder.prototype.withEquipmentTypeId = withEquipmentTypeId;
        ProgramEquipmentTypeDataBuilder.prototype.withoutId = withoutId;

        return ProgramEquipmentTypeDataBuilder;

        function ProgramEquipmentTypeDataBuilder() {
            this.id = '35b8eeca-bfad-47f3-b966-c9cb726b872f';
            this.displayOrder =  1;
            this.enableTestCount = true;
            this.enableTotalColumn = true;
            this.equipmentTypeId = {
                id: 'equipment-type-id-123',
                name: 'Equipment Type'
            };
            this.programId = {
                id: 'program-id-123',
                name: 'Program'
            };

        }

        function build() {
            return new ProgramEquipment(this.buildJson());
        }

        function withId(newId) {
            this.id = newId;
            return this;
        }

        function withEnableTestCount(enableTestCount) {
            this.enableTestCount = enableTestCount;
            return this;
        }

        function withDisplayOrder(displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        function withoutId() {
            this.id = undefined;
            return this;
        }

        function withEnableTotalColumn(enableTotalColumn) {
            this.enableTotalColumn = enableTotalColumn;
            return this;
        }

        function withProgramId(programId) {
            this.programId = programId;
            return this;
        }

        function withEquipmentTypeId(equipmentTypeId) {
            this.equipmentTypeId = equipmentTypeId;
            return this;
        }

        function buildJson() {
            return {
                id: this.id,
                displayOrder: this.displayOrder,
                equipmentTypeId: this.equipmentTypeId,
                programId: this.programId,
                enableTestCount: this.enableTestCount,
                enableTotalColumn: this.enableTotalColumn
            };
        }
    }

})();
