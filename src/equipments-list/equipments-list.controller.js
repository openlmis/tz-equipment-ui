/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipments-list.controller:EquipmentListController
     *
     * @description
     * Controller for managing equipments list screen.
     */
    angular
        .module('equipments-list')
        .controller('EquipmentListController', controller);

    controller.$inject = ['equipments', 'programs', 'programEquipmentService',
        '$q', 'equipmentService'];

    function controller(equipments, programs, programEquipmentService, $q, equipmentService) {

        var vm = this;

        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @propertyOf equipments-list.controller:EquipmentListController
         * @name equipments
         * @type {Array}
         *
         * @description
         * Contains page of equipments.
         */
        vm.equipments = undefined;
        vm.programs = undefined;
        vm.programEquipments = null;
        vm.filterTypeByProgramId = filterTypeByProgramId;
        vm.filterEquipmentByType = filterEquipmentByType;
        vm.selectedProgramId = null;
        vm.equipmentType = null;

        /**
         * @ngdoc method
         * @methodOf equipments-list.controller:EquipmentListController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentListController.
         */
        function onInit() {
            vm.equipments = equipments;
            vm.programs = programs;
        }

        function filterTypeByProgramId(vm) {
            if (vm.selectedProgramId !== null) {
                programEquipmentTypesResolve(vm.selectedProgramId);
            }
        }

        function filterEquipmentByType(vm) {
            if (vm.selectedProgramId !== null && vm.equipmentType !== null) {
                var deferred = $q.defer();
                var content = null;
                equipmentService.getByEquipmentType({
                    programId: vm.selectedProgramId,
                    equipmentTypeId: vm.equipmentType
                }).then(function(response) {
                    deferred.resolve(response.content);
                    content = response.content;
                    vm.equipments = content;
                }, deferred.reject);

                return content;
            }
        }

        function programEquipmentTypesResolve(programId) {

            var deferred = $q.defer();
            var content = null;
            programEquipmentService.getAll({
                programId: programId
            }).then(function(response) {
                deferred.resolve(response.content);
                content = response.content;
                vm.programEquipments = content;
            }, deferred.reject);

            return content;
        }

    }
})();
