/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('disciplineService', function() {

    var $rootScope, $httpBackend;
    var equipmentUrlFactory, disciplineService, DisciplineDataBuilder;
    var discipline, disciplines, DISCIPLINE_ID;

    beforeEach(function() {
        module('discipline-list');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            equipmentUrlFactory = $injector.get('equipmentUrlFactory');
            disciplineService = $injector.get('disciplineService');
            DisciplineDataBuilder = $injector.get('DisciplineDataBuilder');
        });

        prepareTestData();
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/disciplines/' + disciplines[0].id))
                .respond(200, disciplines[0]);
        });

        it('should return promise', function() {
            var result = disciplineService.get(disciplines[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to discipline', function() {
            var result;

            disciplineService.get(disciplines[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(disciplines[0]);

            expect(angular.toJson(result)).toEqual(angular.toJson(expected));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/disciplines/' + disciplines[0].id));

            disciplineService.get(disciplines[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var parameters = {
            page: 0,
            size: 10
        };

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/disciplines?page=' + parameters.page +
                '&size=' + parameters.size)).respond(200, {
                content: disciplines
            });
        });

        it('should return promise', function() {
            var result = disciplineService.getAll(parameters);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to disciplines', function() {
            var result;

            disciplineService.getAll(parameters).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(disciplines);

            expect(angular.toJson(result)).toEqual(angular.toJson({
                content: expected
            }));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/disciplines?page=' + parameters.page +
                '&size=' + parameters.size));

            disciplineService.getAll(parameters);
            $httpBackend.flush();
        });
    });

    describe('save', function() {

        var result;

        beforeEach(function() {
            result = undefined;
        });

        it('should create discipline if ID is not given', function() {
            discipline = new DisciplineDataBuilder().withoutId()
                .build();

            var returned = angular.copy(discipline);

            returned.id = DISCIPLINE_ID;

            $httpBackend.expect(
                'POST', equipmentUrlFactory('/api/disciplines'), discipline
            ).respond(200, returned);

            disciplineService.save(discipline).then(function(discipline) {
                result = discipline;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(returned));
        });

        it('should update discipline if it has ID', function() {
            discipline = new DisciplineDataBuilder().withId(DISCIPLINE_ID)
                .build();

            $httpBackend.expect(
                'PUT', equipmentUrlFactory('/api/disciplines/' + DISCIPLINE_ID), discipline
            ).respond(200, discipline);

            disciplineService.save(discipline).then(function(discipline) {
                result = discipline;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(discipline));
        });

    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    function prepareTestData() {
        DISCIPLINE_ID = 'some-discipline-id';

        disciplines = [
            new DisciplineDataBuilder().withId('35b8eeca-bfad-47f3-b966-c9cb726b872f')
                .build()

        ];
    }
});
