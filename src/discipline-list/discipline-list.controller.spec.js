/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('DisciplineListController', function() {

    beforeEach(function() {
        module('discipline-list');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$state = $injector.get('$state');
            this.DisciplineDataBuilder = $injector.get('DisciplineDataBuilder');
        });

        this.disciplines = [
            new this.DisciplineDataBuilder().build(),
            new this.DisciplineDataBuilder().build()
        ];

        this.stateParams = {
            page: 0,
            size: 10
        };

        this.vm = this.$controller('DisciplineListController', {
            disciplines: this.disciplines,
            $stateParams: this.stateParams
        });
        this.vm.$onInit();
    });

    describe('onInit', function() {

        it('should expose disciplines array', function() {
            expect(this.vm.disciplines).toEqual(this.disciplines);
        });

    });

    describe('goToAddDisciplinePage', function() {

        it('should go to add discipline state', function() {
            spyOn(this.$state, 'go');
            this.vm.goToAddDisciplinePage();

            expect(this.$state.go).toHaveBeenCalledWith('openlmis.administration.discipline.add');
        });
    });

});
