/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name disciplineService
     *
     * @description
     * Responsible for retrieving disciplines from the server.
     */
    angular
        .module('discipline-list')
        .service('disciplineService', service);

    service.$inject = ['equipmentUrlFactory', '$resource'];

    function service(equipmentUrlFactory, $resource) {

        var resource = $resource(equipmentUrlFactory('/api/disciplines/:id'), {}, {
            getAll: {
                url: equipmentUrlFactory('/api/disciplines'),
                method: 'GET'
            },
            get: {
                url: equipmentUrlFactory('/api/disciplines/:id'),
                method: 'GET'
            },
            update: {
                method: 'PUT'
            }
        });
        this.getAll = getAll;
        this.get = get;
        this.save = save;

        function getAll(paginationParams) {
            return resource.getAll(paginationParams).$promise;
        }

        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        function save(discipline) {
            if (discipline.id) {
                return resource.update({
                    id: discipline.id
                }, discipline).$promise;
            }
            return resource.save({}, discipline).$promise;
        }

    }
})();
