/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-test-type-add.controller:EquipmentTestTypeAddController
     *
     * @description
     * Controller for adding equipment test type.
     */
    angular
        .module('equipment-test-type-add')
        .controller('EquipmentTestTypeAddController', EquipmentTestTypeAddController);

    EquipmentTestTypeAddController.$inject = [
        'equipmentTestType', 'confirmService', 'stateTrackerService', '$state', 'equipmentTestTypeService',
        'loadingModalService', 'equipmentCategories', 'notificationService'
    ];

    function EquipmentTestTypeAddController(equipmentTestType, confirmService, stateTrackerService, $state,
                                            equipmentTestTypeService, loadingModalService, equipmentCategories,
                                            notificationService) {

        var vm = this;
        vm.$onInit = onInit;
        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;

        function onInit() {
            vm.equipmentCategories = equipmentCategories;
        }

        function save() {

            loadingModalService.open();
            return new equipmentTestTypeService.save(vm.equipmentTestType)
                .then(function(equipmentTestType) {
                    notificationService.success('equipmentTestType.hasBeenSaved');
                    stateTrackerService.goToPreviousState();
                    return equipmentTestType;
                })
                .catch(function() {
                    notificationService.error('equipmentTestType.failedToSave');
                    loadingModalService.close();
                });

        }
    }

})();
