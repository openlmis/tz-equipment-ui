/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentTestTypeEditController', function() {

    beforeEach(function() {
        module('equipment-test-type-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.$q = $injector.get('$q');
            this.equipmentTestTypeService = $injector.get('equipmentTestTypeService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentCategoryDataBuilder = $injector.get('EquipmentCategoryDataBuilder');
            this.EquipmentTestTypeDataBuilder = $injector.get('EquipmentTestTypeDataBuilder');
        });

        this.equipmentTestType = new this.EquipmentTestTypeDataBuilder().build();

        this.saveDeferred = this.$q.defer();

        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.equipmentTestTypeService, 'save').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.equipmentCategories = [
            new this.EquipmentCategoryDataBuilder().build(),
            new this.EquipmentCategoryDataBuilder().build(),
            new this.EquipmentCategoryDataBuilder().build()
        ];

        this.vm = this.$controller('EquipmentTestTypeEditController', {
            equipmentTestType: this.equipmentTestType,
            equipmentCategories: this.equipmentCategories
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose equipmentTestType', function() {
            expect(this.vm.equipmentTestType).toEqual(this.equipmentTestType);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.saveDeferred.resolve(this.equipmentTestType);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            it('should show notification if equipment test type was saved successfully', function() {
                this.vm.save();

                this.saveDeferred.resolve(this.equipmentTestType);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipment.hasBeenUpdated');
            });

            it('should show notification if equipment test type save has failed', function() {
                this.vm.save();

                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipment.failedToUpdate');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });
});