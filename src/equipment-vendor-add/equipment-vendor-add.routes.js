/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular.module('equipment-vendor-add').config(routes);

    routes.$inject = ['$stateProvider', 'EQUIPMENT_RIGHTS', 'ADMINISTRATION_RIGHTS'];

    function routes($stateProvider, EQUIPMENT_RIGHTS, ADMINISTRATION_RIGHTS) {
        $stateProvider.state('openlmis.administration.equipmentVendorsAdd', {
            controller: 'EquipmentVendorAddController',
            controllerAs: 'vm',
            label: 'equipmentVendorAdd.add',
            templateUrl: 'equipment-vendor-add/equipment-vendor-add.html',
            url: '/equipmentVendorsAdd',
            accessRights: [EQUIPMENT_RIGHTS.EQUIPMENT_MANAGE, ADMINISTRATION_RIGHTS.USERS_MANAGE],
            resolve: {
                equipmentVendor: function(EquipmentVendor) {
                    return new EquipmentVendor({});
                },
                users: function(ReferenceDataUserResource) {
                    return new ReferenceDataUserResource().query()
                        .then(function(users) {
                            return users.content;
                        });
                },
                usersMap: function(users, ObjectMapper) {
                    return new ObjectMapper().map(users);
                },
                activeUsers: function(users) {
                    return users.filter(
                        function(user) {
                            return user.active;
                        }
                    );
                },
                vendors: function(equipmentVendorService) {
                    return equipmentVendorService
                        .query()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                }
            }
        });
    }

})();