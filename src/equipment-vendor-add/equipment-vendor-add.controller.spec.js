/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentVendorAddController', function() {

    var vm, $controller, equipmentVendor, activeUsers, usersMap, $rootScope,
        UserDataBuilder, EquipmentVendorDataBuilder, vendors, $q;

    beforeEach(function() {
        module('equipment-vendor-add');

        inject(function($injector) {
            $q = $injector.get('$q');
            $controller = $injector.get('$controller');
            $rootScope = $injector.get('$rootScope');
            this.equipmentVendorService = $injector.get('equipmentVendorService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            UserDataBuilder = $injector.get('UserDataBuilder');
            EquipmentVendorDataBuilder = $injector.get('EquipmentVendorDataBuilder');
        });

        activeUsers = [
            new UserDataBuilder().build(),
            new UserDataBuilder().build()
        ];

        usersMap = {};
        usersMap[activeUsers[0].id] = activeUsers[0];
        usersMap[activeUsers[1].id] = activeUsers[1];

        vendors = [
            new EquipmentVendorDataBuilder().build(),
            new EquipmentVendorDataBuilder().build()
        ];

        equipmentVendor = new EquipmentVendorDataBuilder().build();

        vm = $controller('EquipmentVendorAddController', {
            vendors: vendors,
            activeUsers: activeUsers,
            usersMap: usersMap,
            equipmentVendor: equipmentVendor
        });
    });

    describe('$onInit', function() {

        it('should init properly', function() {
            vm.$onInit();

            expect(vm.equipmentVendor).toEqual(equipmentVendor);
            expect(vm.activeUsers).toEqual(activeUsers);
            expect(vm.usersMap).toEqual(usersMap);
        });

    });

    describe('validateEquipmentVendorName', function() {

        beforeEach(function() {
            vm.$onInit();
        });

        it('should return undefined if the equipmentVendor name is empty', function() {
            vm.equipmentVendor.name = undefined;

            expect(vm.validateEquipmentVendorName()).toBeUndefined();
        });

        it('should return message key if equipmentVendor name is duplicated', function() {
            vm.equipmentVendor.name = vendors[0].name;

            expect(vm.validateEquipmentVendorName()).toEqual('equipmentVendorAdd.equipmentVendorNameDuplicated');
        });

        it('should return undefined if the equipmentVendor is edited', function() {
            vm.equipmentVendor.id = vendors[0].id;
            vm.equipmentVendor.name = vendors[0].name;

            expect(vm.validateEquipmentVendorName()).toBeUndefined();
        });

        it('should return undefined if equipmentVendor name is not duplicated', function() {
            vm.equipmentVendor.name = 'Some different equipmentVendor name';

            expect(vm.validateEquipmentVendorName()).toBeUndefined();
        });

    });

    describe('addAssociatedUser', function() {

        beforeEach(function() {
            vm.$onInit();
            spyOn(equipmentVendor, 'addAssociatedUser');
        });

        it('should clear form after associatedUser was added', function() {
            equipmentVendor.addAssociatedUser.andReturn($q.resolve());

            vm.selectedUser = vm.activeUsers[0];

            vm.addAssociatedUser();

            expect(vm.selectedUser).toEqual(vm.activeUsers[0]);

            $rootScope.$apply();

            expect(vm.selectedUser).toBeUndefined();
        });

        it('should not clear form if associatedUser failed to be added', function() {
            equipmentVendor.addAssociatedUser.andReturn($q.reject());

            vm.selectedUser = vm.activeUsers[0];

            vm.addAssociatedUser();
            $rootScope.$apply();

            expect(vm.selectedUser).toEqual(vm.activeUsers[0]);
        });

    });

});