/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */
(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-vendor-add.controller:EquipmentVendorAddController
     *
     * @description
     * Exposes method for creating/updating equipmentVendor to the modal view.
     */
    angular
        .module('equipment-vendor-add')
        .controller('EquipmentVendorAddController', controller);

    controller.$inject = ['vendors', 'equipmentVendor', 'activeUsers', 'usersMap', 'EquipmentVendor',
        'equipmentVendorService', 'stateTrackerService', 'loadingModalService', 'notificationService'];

    function controller(equipmentVendors, equipmentVendor, activeUsers, usersMap,
                        EquipmentVendor, equipmentVendorService,
                        stateTrackerService, loadingModalService, notificationService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.addAssociatedUser = addAssociatedUser;
        vm.removeAssociatedUser = removeAssociatedUser;
        vm.toUserIdFullName = toUserIdFullName;
        vm.toUserIdUsername = toUserIdUsername;
        vm.save = save;
        vm.validateEquipmentVendorName = validateEquipmentVendorName;
        vm.validateEquipmentVendorEmail = validateEquipmentVendorEmail;
        vm.validateEquipmentVendorWebsite = validateEquipmentVendorWebsite;
        vm.validateEquipmentVendorPrimaryPhone = validateEquipmentVendorPrimaryPhone;

        /**
         * @ngdoc property
         * @propertyOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @type {EquipmentVendor}
         * @name equipmentVendor
         *
         * @description
         * EquipmentVendor that is being created.
         */
        vm.equipmentVendor = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @type {Array}
         * @name activeUsers
         *
         * @description
         * The list of active users available in the system.
         */
        vm.activeUsers = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @type {Object}
         * @name usersMap
         *
         * @description
         * The map of users by ids.
         */
        vm.usersMap = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentVendorAddController.
         */
        function onInit() {
            vm.usersMap = usersMap;
            vm.equipmentVendor = equipmentVendor;
            vm.activeUsers = activeUsers;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name toUserIdFullName
         *
         * @description
         * Gets users full name from first and last name.
         *
         * @return {String} the full name.
         */
        function toUserIdFullName(userId) {
            var user = vm.usersMap[userId];
            return user.firstName + ' ' + user.lastName;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name toUserIdUsername
         *
         * @description
         * Gets users full name from first and last name.
         *
         * @return {String} the full name.
         */
        function toUserIdUsername(userId) {
            var user = vm.usersMap[userId];
            return user.username;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name addAssociatedUser
         *
         * @description
         * Adds valid equipmentVendor assignment.
         *
         * @return {Promise} the promise resolving to the added assignment.
         */
        function addAssociatedUser() {
            return vm.equipmentVendor.addAssociatedUser(vm.selectedUser.id)
                .then(function() {
                    vm.selectedUser = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name removeAssociatedUser
         *
         * @description
         * Remove equipmentVendor associated user.
         *
         * @return {Promise} the promise resolving to the removed user.
         */
        function removeAssociatedUser(userId) {
            return vm.equipmentVendor.removeAssociatedUser(userId)
                .then(function() {
                    vm.selectedUser = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name validateEquipmentVendorName
         *
         * @description
         * Validates the entered equipmentVendor name.
         *
         * @return {String} the error message key, undefined if equipmentVendor is valid
         */
        function validateEquipmentVendorName() {
            if (isEquipmentVendorNameDuplicated()) {
                return 'equipmentVendorAdd.equipmentVendorNameDuplicated';
            }
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name validateEquipmentVendorWebsite
         *
         * @description
         * Validates the entered equipmentVendor website.
         *
         * @return {String} the error message key, undefined if equipmentVendor is valid
         */
        function validateEquipmentVendorWebsite() {

        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name validateEquipmentVendorEmail
         *
         * @description
         * Validates the entered equipmentVendor email.
         *
         * @return {String} the error message key, undefined if equipmentVendor is valid
         */
        function validateEquipmentVendorEmail() {

        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name validateEquipmentVendorPrimaryPhone
         *
         * @description
         * Validates the entered equipmentVendor primaryPhone.
         *
         * @return {String} the error message key, undefined if equipmentVendor is valid
         */
        function validateEquipmentVendorPrimaryPhone() {

        }

        function isEquipmentVendorNameDuplicated() {
            if (!vm.equipmentVendor || !vm.equipmentVendor.name) {
                return false;
            }

            return equipmentVendors.filter(function(equipmentVendor) {
                return (
                    (equipmentVendor.name.toUpperCase() === vm.equipmentVendor.name.toUpperCase())
                    && equipmentVendor.id !== vm.equipmentVendor.id
                );
            }).length;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-add.controller:EquipmentVendorAddController
         * @name save
         *
         * @description
         * Saves the vendor and takes user back to the previous state.
         */
        function save() {
            loadingModalService.open();
            return equipmentVendorService.save(vm.equipmentVendor)
                .then(function(vendor) {
                    notificationService.success('equipmentVendorAdd.equipmentVendorSavedSuccessfully');
                    stateTrackerService.goToPreviousState();
                    return vendor;
                })
                .catch(function() {
                    notificationService.error('equipmentVendorAdd.equipmentVendorFailedToSave');
                    loadingModalService.close();
                });
        }

    }
})();