/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('program-equipment-add')
        .config(routes);

    routes.$inject = ['modalStateProvider'];

    function routes(modalStateProvider) {

        modalStateProvider.state('openlmis.administration.programEquipments.add', {
            controller: 'ProgramEquipmentAddController',
            controllerAs: 'vm',
            resolve: {
                programEquipment: programEquipmentResolve,
                equipmentType: equipmentTypesResolve,
                programs: programsResolve
            },
            parentResolves: [],
            templateUrl: 'program-equipment-add/program-equipment-add.html',
            url: '/'
        });
    }

    programEquipmentResolve.$inject = ['$stateParams'];
    function programEquipmentResolve($stateParams) {
        if ($stateParams.programEquipment) {
            return $stateParams.programEquipment;
        }
        return {};
    }

    equipmentTypesResolve.$inject = ['equipmentTypeService'];
    function equipmentTypesResolve(equipmentTypeService) {
        return equipmentTypeService.getAll({
            active: true
        })
            .then(function(response) {
                return response.content;
            });
    }

    programsResolve.$inject = ['programEquipmentService'];
    function programsResolve(programEquipmentService) {
        return  programEquipmentService.getPrograms();
    }

})();
