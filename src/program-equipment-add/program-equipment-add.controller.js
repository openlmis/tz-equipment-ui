/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-type-add.controller:EquipmentTypeAddController
     *
     * @description
     * Provides methods for Add Equipment Type. Allows returning to previous states and saving
     * equipment type.
     */
    angular
        .module('program-equipment-add')
        .controller('ProgramEquipmentAddController', ProgramEquipmentAddController);

    ProgramEquipmentAddController.$inject = [
        'equipmentType', '$state',  'stateTrackerService', 'loadingModalService',
        'programEquipmentService', 'notificationService', 'programEquipment', 'programs'
    ];

    function ProgramEquipmentAddController(equipmentType, $state, stateTrackerService,
                                           loadingModalService, programEquipmentService, notificationService,
                                           programEquipment, programs) {

        var vm = this;

        vm.$onInit = onInit;

        vm.equipmentType = undefined;
        vm.programs = undefined;
        /**
         * @ngdoc method
         * @methodOf equipment-type-add.controller:EquipmentTypeAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentTypeAddController.
         */
        function onInit() {
            vm.save = save;
            vm.goToPreviousState = stateTrackerService.goToPreviousState;
            vm.programEquipment = programEquipment;
            vm.equipmentType = equipmentType;
            vm.programs = programs;
            vm.programEquipment.enableTestCount = false;
            vm.programEquipment.enableTotalColumn = false;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-type-add.controller:EquipmentTypeAddController
         * @name save
         *
         * @description
         * Saves the equipment type and takes user back to the previous state.
         */
        function save() {
            loadingModalService.open();
            return new programEquipmentService.save(vm.programEquipment)
                .then(function(programEquipment) {
                    notificationService.success('programEquipment.typeHasBeenSaved');
                    stateTrackerService.goToPreviousState();
                    return programEquipment;
                })
                .catch(function() {
                    notificationService.error('programEquipment.failedToSaveType');
                    loadingModalService.close();
                });
        }

    }

})();
