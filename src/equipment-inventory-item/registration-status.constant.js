/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name equipment-inventory-item.REGISTRATION_STATUS
     *
     * @description
     * Stores the list of all available REGISTRATION statuses and a method to get their labels.
     */
    angular
        .module('equipment-inventory-item')
        .constant('REGISTRATION_STATUS', REGISTRATION_STATUS());

    function REGISTRATION_STATUS() {
        var REGISTRATION_STATUS = {
                NEW: 'NEW',
                RECONDITIONED: 'RECONDITIONED',
                USED: 'USED',
                getLabel: getLabel,
                getStatuses: getStatuses
            },
            labels = {
                NEW: 'equipmentInventoryItem.new',
                RECONDITIONED: 'equipmentInventoryItem.reconditioned',
                USED: 'equipmentInventoryItem.used'
            };

        return REGISTRATION_STATUS;

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.REGISTRATION_STATUS
         * @name getLabel
         *
         * @description
         * Returns a label for the given status. Throws an exception if the status is not
         * recognized.
         *
         * @param   {String}    status  the energy status
         * @return  {String}            the label
         */
        function getLabel(status) {
            var label = labels[status];

            if (!label) {
                throw '"' + status + '" is not a valid status';
            }

            return label;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.REGISTRATION_STATUS
         * @name getStatuses
         *
         * @description
         * Returns all available statuses as a list.
         *
         * @return  {Array} the list of available statuses
         */
        function getStatuses() {
            return [
                REGISTRATION_STATUS.NEW,
                REGISTRATION_STATUS.RECONDITIONED,
                REGISTRATION_STATUS.USED
            ];
        }

    }

})();
