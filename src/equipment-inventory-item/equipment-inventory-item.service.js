/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-inventory-item.inventoryItemService
     *
     * @description
     * Responsible for retrieving equipment inventory items from the server.
     */
    angular
        .module('equipment-inventory-item')
        .factory('inventoryItemService', service);

    service.$inject = ['equipmentUrlFactory', '$resource', 'referencedataUrlFactory'];

    function service(equipmentUrlFactory, $resource, referencedataUrlFactory) {

        var resource = $resource(equipmentUrlFactory('/api/equipmentInventories/:id'), {}, {
            get: {
                transformResponse: transformGetResponse
            },
            query: {
                method: 'GET',
                url: equipmentUrlFactory('/api/equipmentInventories'),
                transformResponse: transformGetAllResponse
            },
            update: {
                method: 'PUT'
            },
            getOrderablesEquipments: {
                url: equipmentUrlFactory('/api/equipmentInventories/orderables'),
                method: 'GET'
            },
            saveMaintenanceRequest: {
                url: equipmentUrlFactory('/api/equipmentMaintenanceRequests'),
                method: 'POST'
            },
            getVendors: {
                url: equipmentUrlFactory('/api/vendors'),
                method: 'GET'
            },
            getMaintenanceRequestReasons: {
                url: equipmentUrlFactory('/api/equipmentMaintenanceRequestReasons'),
                method: 'GET'
            },
            getMaintenanceRequestsByVendor: {
                url: equipmentUrlFactory('/api/equipmentMaintenanceRequests/byVendor'),
                method: 'GET'
            },

            getByFacilityId: {
                url: referencedataUrlFactory('/api/facilities/:facilityId'),
                method: 'GET'
            },
            saveMaintenanceResponse: {
                url: equipmentUrlFactory('/api/equipmentServiceLogs'),
                method: 'POST'
            },
            getMaintenanceLogs: {
                url: equipmentUrlFactory('/api/equipmentServiceLogs/:itemId/byItem'),
                method: 'GET'
            },
            approveMaintenanceLog: {
                url: equipmentUrlFactory('/api/equipmentServiceLogs/:id/approve'),
                method: 'PUT'
            }

        });

        return {
            get: get,
            query: query,
            save: save,
            getOrderablesEquipments: getOrderablesEquipments,
            saveMaintenanceRequest: saveMaintenanceRequest,
            getMaintenanceRequestReasons: getMaintenanceRequestReasons,
            getVendors: getVendors,
            getMaintenanceRequestsByVendor: getMaintenanceRequestsByVendor,
            getByFacilityId: getByFacilityId,
            saveMaintenanceResponse: saveMaintenanceResponse,
            getMaintenanceLogs: getMaintenanceLogs,
            approveMaintenanceLog: approveMaintenanceLog,
            rejectMaintenanceLog: rejectMaintenanceLog
        };

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name get
         *
         * @description
         * Gets equipment inventory item by id.
         *
         * @param  {String}  id equipment inventory item UUID
         * @return {Promise}    equipment inventory item info
         */
        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name query
         *
         * @description
         * Query equipment inventory items.
         *
         * @param  {Object} params query parameters
         * @return {Promise}       Page of all equipment inventory items
         */
        function query(params) {
            return resource.query(params).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name update
         *
         * @description
         * Saves the given inventory item. It will create a new one if the ID is not defined and
         * update the existing one otherwise.
         *
         * @param  {Object}     inventoryItem   the updated inventory item
         * @return {Promise}                    the promise resolving to the updated item
         */
        function save(inventoryItem) {
            if (inventoryItem.id) {
                return resource.update({
                    id: inventoryItem.id
                }, inventoryItem).$promise;
            }
            return resource.save({}, inventoryItem).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name getOrderablesEquipments
         *
         * @description
         * Query equipment inventory items which are associated with the specified orderables.
         *
         * @param  {Object} params query parameters
         * @return {Promise}       Map of orderable and associated equipment inventory items
         */
        function getOrderablesEquipments(params) {
            return resource.getOrderablesEquipments(params).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name saveMaintenanceRequest
         *
         * @description
         * Saves the given maintenance requests. It will create a new one if the ID is not defined and
         * update the existing one otherwise.
         *
         * @param  {Object}     inventoryItemRequest   the updated inventory item
         * @return {Promise}                    the promise resolving to the updated item
         */
        function saveMaintenanceRequest(inventoryItemRequest) {
            if (inventoryItemRequest.id) {
                return resource.saveMaintenanceRequest({
                    id: inventoryItemRequest.id
                }, inventoryItemRequest).$promise;
            }
            return resource.saveMaintenanceRequest({}, inventoryItemRequest).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name getMaintenanceRequestReasons
         *
         * @description
         * Query equipment inventory items which are associated with the specified reasons.
         *
         * @return {Promise}       List of equipment maintenance inventory items
         */
        function getMaintenanceRequestReasons() {
            return resource.getMaintenanceRequestReasons().$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name getVendors
         *
         * @description
         * Query equipment inventory items which are associated with the specified vendors.
         *
         * @return {Promise}       List of vendors.
         */
        function getVendors() {
            return resource.getVendors().$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name getMaintenanceRequestsByVendor
         *
         * @description
         * Query equipment inventory items which are associated with the specified vendors.
         *
         * @return {Promise}       List of maintenance requests by vendor.
         */
        function getMaintenanceRequestsByVendor() {
            return resource.getMaintenanceRequestsByVendor().$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name get
         *
         * @description
         * Gets Maintenance request by facility id.
         *
         * @param  {String}  facilityId facility UUID
         * @return {Promise}    Facility info
         */
        function getByFacilityId(facilityId) {
            return resource.getByFacilityId({
                facilityId: facilityId
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name saveMaintenanceResponse
         *
         * @description
         * Saves the given maintenance response. It will create a new one if the ID is not defined and
         * update the existing one otherwise.
         *
         * @param  {Object}     response   the updated inventory item
         * @return {Promise}               the promise resolving to the updated item
         */
        function saveMaintenanceResponse(response) {
            if (response.id) {
                return resource.saveMaintenanceResponse({
                    id: response.id
                }, response).$promise;
            }
            return resource.saveMaintenanceResponse({}, response).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name getMaintenanceLogs
         *
         * @description
         * Gets equipment inventory item logs by id.
         *
         * @param  {String}  itemId equipment inventory item UUID
         * @return {Promise}    equipment inventory item info
         */
        function getMaintenanceLogs(itemId) {
            return resource.getMaintenanceLogs({
                itemId: itemId
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name approveMaintenanceLog
         *
         * @description
         * Approve specified maintenance log.
         *
         * @param  {Object}     mainternanceLog   maintenance log entry to approve.
         * @return {Promise}                    the promise resolving to the approved item
         */
        function approveMaintenanceLog(mainternanceLog) {
            return resource.approveMaintenanceLog(
                {
                    id: mainternanceLog.id
                },
                {
                    approved: true
                }
            ).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.inventoryItemService
         * @name rejectMaintenanceLog
         *
         * @description
         * Reject specified maintenance log.
         *
         * @param  {Object}     mainternanceLog   maintenance log entry to reject.
         * @return {Promise}                    the promise resolving to the rejectd item
         */
        function rejectMaintenanceLog(mainternanceLog) {
            return resource.approveMaintenanceLog(
                {
                    id: mainternanceLog.id
                },
                {
                    approved: false
                }
            ).$promise;
        }

        function transformGetResponse(data, headers, status) {
            return transformResponse(data, status, transformInventoryItem);
        }

        function transformGetAllResponse(data, headers, status) {
            return transformResponse(data, status, function(response) {
                angular.forEach(response.content, function(inventoryItem) {
                    transformInventoryItem(inventoryItem);
                });
                return response;
            });
        }

        function transformInventoryItem(inventoryItem) {
            inventoryItem.decommissionDate = new Date(inventoryItem.decommissionDate);
            return inventoryItem;
        }

        function transformResponse(data, status, transformer) {
            if (status === 200) {
                return transformer(angular.fromJson(data));
            }
            return data;
        }
    }
})();
