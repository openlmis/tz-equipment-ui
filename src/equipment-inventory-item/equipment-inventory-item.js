/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-inventory-item.InventoryItem
     *
     * @description
     * Represents a single inventory item.
     */
    angular
        .module('equipment-inventory-item')
        .factory('InventoryItem', inventoryItem);

    // inventoryItem.$inject = ['EQUIPMENT_STATUS', 'ENERGY_SOURCE'];

    function inventoryItem() {
        return InventoryItem;

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item.InventoryItem
         * @name InventoryItem
         *
         * @description
         * Creates a new instance of the InventoryItem class.
         *
         * @param  {Object} source          the inventory item to be updated
         * @param  {Object} facility        full facility instance
         * @param  {Object} program         full program instance
         * @return {Object}                 the inventory item with default options
         */
        function InventoryItem(source, facility, program) {
            angular.copy(source, this);

            if (facility !== undefined) {
                if (this.facilityId === facility.id) {
                    this.facility = facility;
                    this.facilityId = facility.id;
                } else {
                    throw 'Parameter facility has different ID than facility from provided inventory item!';
                }
            }
            if (program !== undefined) {
                if (this.programId === program.id) {
                    this.program = program;
                    this.programId = program.id;
                } else {
                    throw 'Parameter program has different ID than program from provided inventory item!';
                }
            }
        }
    }
})();
