/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EQUIPMENT_STATUS', function() {

    var EQUIPMENT_STATUS;

    beforeEach(function() {
        module('equipment-inventory-item');

        inject(function($injector) {
            EQUIPMENT_STATUS = $injector.get('EQUIPMENT_STATUS');
        });
    });

    describe('getLabel', function() {

        it('should return label for valid status', function() {
            expect(
                EQUIPMENT_STATUS.getLabel('YES')
            ).toEqual('equipmentInventoryItem.yes');

            expect(
                EQUIPMENT_STATUS.getLabel('NO')
            ).toEqual('equipmentInventoryItem.no');

            expect(
                EQUIPMENT_STATUS.getLabel('UNKNOWN')
            ).toEqual('equipmentInventoryItem.unknown');

            expect(
                EQUIPMENT_STATUS.getLabel('NOT_APPLICABLE')
            ).toEqual('equipmentInventoryItem.notApplicable');
        });

        it('should throw exception for invalid source', function() {
            expect(function() {
                EQUIPMENT_STATUS.getLabel('NON_EXISTENT_SOURCE');
            }).toThrow('"NON_EXISTENT_SOURCE" is not a valid status');

            expect(function() {
                EQUIPMENT_STATUS.getLabel(undefined);
            }).toThrow('"undefined" is not a valid status');

            expect(function() {
                EQUIPMENT_STATUS.getLabel(null);
            }).toThrow('"null" is not a valid status');

            expect(function() {
                EQUIPMENT_STATUS.getLabel('');
            }).toThrow('"" is not a valid status');
        });

    });

    describe('getStatuses', function() {

        it('should return a list of statuses', function() {
            expect(EQUIPMENT_STATUS.getStatuses()).toEqual([
                'YES',
                'NO',
                'UNKNOWN',
                'NOT_APPLICABLE'
            ]);
        });

    });

});
