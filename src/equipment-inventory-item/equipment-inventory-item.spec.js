/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('InventoryItem', function() {

    var FacilityProgramInventoryItemDataBuilder;

    beforeEach(function() {
        module('equipment-inventory-item');

        inject(function($injector) {
            FacilityProgramInventoryItemDataBuilder = $injector.get('FacilityProgramInventoryItemDataBuilder');
        });
    });

    it('should merge facilities and programs', function() {
        var item = new FacilityProgramInventoryItemDataBuilder().build();

        expect(item.facility.name).not.toBeUndefined();
        expect(item.program.name).not.toBeUndefined();
    });

    it('should throw exception when facility param has different ID than facility from provided inventory item',
        function() {
            expect(function() {
                new FacilityProgramInventoryItemDataBuilder().withFacility('bad-facility')
                    .build();
            }).toThrow(new Error('Parameter facility has different ID than facility from provided inventory item!'));
        });

});
