/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipments')
        .factory('EquipmentDataBuilder', EquipmentDataBuilder);

    EquipmentDataBuilder.$inject = ['Equipment'];

    function EquipmentDataBuilder(Equipment) {

        EquipmentDataBuilder.prototype.build = build;
        EquipmentDataBuilder.prototype.withId = withId;
        EquipmentDataBuilder.prototype.withName = withName;
        EquipmentDataBuilder.prototype.withManufacturer = withManufacturer;
        EquipmentDataBuilder.prototype.withEnergyType = withEnergyType;
        EquipmentDataBuilder.prototype.withModel = withModel;
        EquipmentDataBuilder.prototype.withCategory = withCategory;

        return EquipmentDataBuilder;

        function EquipmentDataBuilder() {
            EquipmentDataBuilder.instanceNumber = (EquipmentDataBuilder.instanceNumber || 0) + 1;

            this.id = 'equipment-id-' + EquipmentDataBuilder.instanceNumber;
            this.manufacturer = '123';
            this.name = 'Test Model';
            this.energyType = {
                id: 'energy-type-id-123',
                name: 'Energy Type'
            };
            this.model = {
                id: 'model-id-123',
                name: 'Model'
            };
            this.category = {
                id: 'category-id-123',
                name: 'Category'
            };
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withManufacturer(manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        function withEnergyType(energyType) {
            this.energyType = energyType;
            return this;
        }

        function withModel(model) {
            this.model = model;
            return this;
        }

        function withCategory(category) {
            this.category = category;
            return this;
        }

        function build() {
            return new Equipment(
                this.id,
                this.manufacturer,
                this.name,
                this.energyType,
                this.model,
                this.category
            );
        }

    }

})();