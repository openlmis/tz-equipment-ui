/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipments.Equipment
     *
     * @description
     * Represents a single equipment.
     */
    angular
        .module('equipments')
        .factory('Equipment', Equipment);

    function Equipment() {

        return Equipment;

        /**
         * @ngdoc method
         * @methodOf equipments.Equipment
         * @name Equipment
         *
         * @description
         * Creates a new instance of the Equipment class.
         *
         * @param  {String}  id             the UUID of the equipment to be created
         * @param  {String}  manufacturer           the manufacturer of the equipment to be created
         * @param  {String}  name           the name of the equipment to be created
         * @param  {Object}  energyType    the energy type of the equipment to be created
         * @param {Object} model the model of the equipment
         * @param {Object} category the category of the equipment
         */
        function Equipment(id, manufacturer, name, energyType, model, category) {
            this.id = id;
            this.manufacturer = manufacturer;
            this.name = name;
            this.energyType = energyType;
            this.model = model;
            this.category = category;
        }

    }

})();
