/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipments.equipmentService
     *
     * @description
     * Responsible for retrieving all equipment information from server.
     */
    angular
        .module('equipments')
        .service('equipmentService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/equipments/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/equipments'),
                method: 'GET',
                isArray: false
            },
            getByEquipmentType: {
                url: referencedataUrlFactory('/api/equipments/byProgram'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/equipments/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;
        this.getByEquipmentType = getByEquipmentType;

        /**
         * @ngdoc method
         * @methodOf equipments.equipmentService
         * @name get
         *
         * @description
         * Retrieves equipment by id.
         *
         * @param  {String}  equipmentId equipment UUID
         * @return {Promise}                equipment promise
         */
        function get(equipmentId) {
            return resource.get({
                id: equipmentId
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipments.equipmentService
         * @name getAll
         *
         * @description
         * Retrieves all equipments by ids.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Page of equipments
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        function getByEquipmentType(queryParams) {
            return resource.getByEquipmentType(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipments.equipmentService
         * @name update
         *
         * @description
         * Updates existing Equipment.
         *
         * @param  {Object}  equipment equipment that will be saved
         * @return {Promise}              updated equipment
         */
        function update(equipment) {
            return resource.update({
                id: equipment.id
            }, equipment).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipments.equipmentService
         * @name create
         *
         * @description
         * Creates new Equipment.
         *
         * @param  {Object}  equipment equipment that will be created
         * @return {Promise}              created equipment
         */
        function create(equipment) {
            return resource.save(equipment).$promise;
        }
    }
})();
