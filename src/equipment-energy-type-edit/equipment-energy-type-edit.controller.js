/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-energy-type-edit.controller:EquipmentEnergyTypeEditController
     *
     * @description
     * Provides methods for Edit equipment energy type modal. Allows returning to previous state and editing equipment
     * energy type.
     */
    angular
        .module('equipment-energy-type-edit')
        .controller('EquipmentEnergyTypeEditController', EquipmentEnergyTypeEditController);

    EquipmentEnergyTypeEditController.$inject = [
        'equipmentEnergyType', 'confirmService', 'equipmentEnergyTypeService', 'stateTrackerService',
        '$state', 'loadingModalService', 'notificationService'
    ];

    function EquipmentEnergyTypeEditController(equipmentEnergyType, confirmService, equipmentEnergyTypeService,
                                               stateTrackerService, $state, loadingModalService, notificationService) {
        var vm = this;

        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @methodOf equipment-energy-type-edit.controller:EquipmentEnergyTypeEditController
         * @name equipmentEnergyType
         * @type {Object}
         *
         * @description
         * Current equipment energy type.
         */
        vm.equipmentEnergyType = undefined;

        /**
         * @ngdoc property
         * @methodOf equipment-energy-type-edit.controller:EquipmentEnergyTypeEditController
         * @name editMode
         * @type {boolean}
         *
         * @description
         * Indicates if equipment energy type is already created.
         */
        vm.editMode = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-energy-type-edit.controller:EquipmentEnergyTypeEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentEnergyTypeEditController.
         */
        function onInit() {
            vm.equipmentEnergyType = equipmentEnergyType ? equipmentEnergyType : {};
            vm.editMode = !!equipmentEnergyType;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-energy-type-edit.controller:EquipmentEnergyTypeEditController
         * @name save
         *
         * @description
         * Saves the equipment energy type after confirm.
         */
        function save() {
            confirmService.confirm(
                vm.editMode ? 'equipmentEnergyTypeEdit.save.question' : 'equipmentEnergyTypeEdit.create.question',
                vm.editMode ? 'equipmentEnergyTypeEdit.save' : 'equipmentEnergyTypeEdit.create'
            ).then(function() {
                loadingModalService.open();
                getSavePromise()
                    .then(function() {
                        notificationService.success(vm.editMode ?
                            'equipmentEnergyTypeEdit.save.success' : 'equipmentEnergyTypeEdit.create.success');
                        stateTrackerService.goToPreviousState();
                    })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error(
                            vm.editMode
                                ? 'equipmentEnergyTypeEdit.save.failure'
                                : 'equipmentEnergyTypeEdit.create.failure'
                        );
                    });
            });
        }

        function getSavePromise() {
            return vm.editMode ?
                equipmentEnergyTypeService.update(vm.equipmentEnergyType) :
                equipmentEnergyTypeService.create(vm.equipmentEnergyType);
        }
    }
})();
