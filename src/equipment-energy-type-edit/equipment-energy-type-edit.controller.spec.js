/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentEnergyTypeEditController', function() {

    beforeEach(function() {
        module('equipment-energy-type-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.confirmService = $injector.get('confirmService');
            this.$q = $injector.get('$q');
            this.equipmentEnergyTypeService = $injector.get('equipmentEnergyTypeService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentEnergyTypeDataBuilder = $injector.get('EquipmentEnergyTypeDataBuilder');
        });

        this.equipmentEnergyType = new this.EquipmentEnergyTypeDataBuilder().build();
        this.confirmDeferred = this.$q.defer();
        this.saveDeferred = this.$q.defer();

        spyOn(this.confirmService, 'confirm').andReturn(this.confirmDeferred.promise);
        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.equipmentEnergyTypeService, 'update').andReturn(this.saveDeferred.promise);
        spyOn(this.equipmentEnergyTypeService, 'create').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('EquipmentEnergyTypeEditController', {
            equipmentEnergyType: this.equipmentEnergyType
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose equipmentEnergyType', function() {
            expect(this.vm.equipmentEnergyType).toEqual(this.equipmentEnergyType);
        });

        it('should expose resolved fields', function() {
            expect(this.vm.editMode).toEqual(true);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.confirmDeferred.resolve();
            this.saveDeferred.resolve(this.equipmentEnergyType);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            beforeEach(function() {
                this.vm.editMode = true;
            });

            it('should prompt user to confirm save', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentEnergyTypeEdit.save.question',
                    'equipmentEnergyTypeEdit.save'
                );
            });

            it('should not save equipment energy type if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentEnergyTypeService.update).not.toHaveBeenCalled();
                expect(this.equipmentEnergyTypeService.create).not.toHaveBeenCalled();
            });

            it('should save equipment energy type and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentEnergyTypeService.update).toHaveBeenCalledWith(this.vm.equipmentEnergyType);
                expect(this.equipmentEnergyTypeService.create).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment energy type was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentEnergyType);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentEnergyTypeEdit.save.success');
            });

            it('should show notification if energy type save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentEnergyTypeEdit.save.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });

        describe('while creating', function() {

            beforeEach(function() {
                this.vm.editMode = false;
            });

            it('should prompt user to save energy type', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentEnergyTypeEdit.create.question',
                    'equipmentEnergyTypeEdit.create'
                );
            });

            it('should not save equipment energy type if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentEnergyTypeService.create).not.toHaveBeenCalled();
                expect(this.equipmentEnergyTypeService.update).not.toHaveBeenCalled();
            });

            it('should save equipment energy type and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentEnergyTypeService.create).toHaveBeenCalledWith(this.vm.equipmentEnergyType);
                expect(this.equipmentEnergyTypeService.update).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment energy type was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentEnergyType);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentEnergyTypeEdit.create.success');
            });

            it('should show notification if energy type save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentEnergyTypeEdit.create.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });

    describe('goToPreviousState', function() {

        it('should redirect to equipment energy type list screen', function() {
            this.vm.goToPreviousState();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });
    });
});
