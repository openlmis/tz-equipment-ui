/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-maintenance-list')
        .config(routes);

    routes.$inject = ['$stateProvider', 'EQUIPMENT_RIGHTS'];

    function routes($stateProvider, EQUIPMENT_RIGHTS) {

        $stateProvider.state('openlmis.equipment.maintenance', {
            showInNavigation: true,
            label: 'equipmentMaintenanceList.equipmentMaintenance',
            url: '/equipmentMaintenance?page&size&facility&program&supervised&functionalStatus',
            params: {
                expand: 'lastModifier'
            },
            controller: 'EquipmentMaintenanceListController',
            templateUrl: 'equipment-maintenance-list/equipment-maintenance-list.html',
            controllerAs: 'vm',
            accessRights: [EQUIPMENT_RIGHTS.EQUIPMENT_INVENTORY_VIEW, EQUIPMENT_RIGHTS.EQUIPMENT_INVENTORY_EDIT],
            resolve: {
                user: function(authorizationService) {
                    return authorizationService.getUser();
                },
                inventoryItems: function(facilityInventoryItemFactory, paginationService, $stateParams) {
                    return paginationService.registerUrl($stateParams, function(stateParams) {
                        if (stateParams.facility && stateParams.program) {
                            var stateParamsCopy = angular.copy(stateParams);
                            stateParamsCopy.facilityId = stateParams.facility;
                            stateParamsCopy.programId = stateParams.program;

                            delete stateParamsCopy.facility;
                            delete stateParamsCopy.program;
                            delete stateParamsCopy.supervised;
                            delete stateParamsCopy.expand;

                            return facilityInventoryItemFactory.query(stateParamsCopy);
                        }
                    });
                },
                canEdit: function(permissionService, user, EQUIPMENT_RIGHTS) {
                    return permissionService
                        .hasPermissionWithAnyProgramAndAnyFacility(user.user_id, {
                            right: EQUIPMENT_RIGHTS.EQUIPMENT_INVENTORY_EDIT
                        })
                        .then(function() {
                            return true;
                        })
                        .catch(function() {
                            return false;
                        });
                },
                maintenanceRequestByVendor: function($q, inventoryItemService) {
                    var deferred = $q.defer();

                    inventoryItemService.getMaintenanceRequestsByVendor()
                        .then(function(response) {
                            var responseData = [];
                            angular.forEach(response.content, function(request) {
                                request.facilityObject = {};

                                inventoryItemService.getByFacilityId(request.inventory.facilityId)
                                    .then(function(fac) {
                                        request.facilityObject = fac;
                                    });
                                responseData.push(request);
                            });
                            deferred.resolve(responseData);
                        }, deferred.reject);

                    return deferred.promise;
                }
            }
        });
    }
})();
