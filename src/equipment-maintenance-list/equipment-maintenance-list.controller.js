/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-maintenance-list.controller:EquipmentMaintenanceListController
     *
     * @description
     * Controller for equipment maintenance item list screen.
     */
    angular
        .module('equipment-maintenance-list')
        .controller('EquipmentMaintenanceListController', EquipmentMaintenanceListController);

    EquipmentMaintenanceListController.$inject = [
        'inventoryItems', '$state', '$stateParams', 'FUNCTIONAL_STATUS', 'EQUIPMENT_RIGHTS', 'messageService',
        'REASON_FOR_NOT_WORKING', 'canEdit', 'maintenanceRequestByVendor'
    ];

    function EquipmentMaintenanceListController(inventoryItems, $state, $stateParams, FUNCTIONAL_STATUS,
                                                EQUIPMENT_RIGHTS, messageService, REASON_FOR_NOT_WORKING,
                                                canEdit, maintenanceRequestByVendor) {

        var vm = this;

        vm.$onInit = onInit;
        vm.getStatusLabel = getStatusLabel;
        vm.goToStatusUpdate = goToStatusUpdate;
        vm.getReasonLabel = getReasonLabel;
        vm.search = search;
        vm.getFunctionalStatusClass = FUNCTIONAL_STATUS.getClass;
        vm.goToResponse = goToResponse;

        /**
         * @ngdoc property
         * @propertyOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name maintenanceRequests
         * @type {Array}
         *
         * @description
         * Init method for EquipmentMaintenanceListController.
         */
        vm.maintenanceRequests = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name inventoryItems
         * @type {Array}
         *
         * @description
         * Init method for EquipmentMaintenanceListController.
         */
        vm.inventoryItems = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name userHasRightToEdit
         * @type {Boolean}
         *
         * @description
         * Flag defining whether user has right for editing the list of maintenance items.
         */
        vm.userHasRightToEdit = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name isSupervised
         * @type {Boolean}
         *
         * @description
         * Holds currently selected facility selection type:
         *  false - my facility
         *  true - supervised facility
         */
        vm.isSupervised = undefined;

        /**
         * ngdoc property
         * @propertyOf equipment-maintenance-list.controller.EquipmentMaintenanceListController
         * @name functionalStatus
         * @type {string}
         *
         * @description
         * The selected functional status.
         */
        vm.functionalStatus = undefined;

        /**
         * ngdoc property
         * @propertyOf equipment-maintenance-list.controller.EquipmentMaintenanceListController
         * @name functionalStatuses
         * @type {Array}
         *
         * @description
         * The list of available functional statuses.
         */
        vm.functionalStatuses = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name onInit
         *
         * @description
         * Init method for EquipmentMaintenanceListController.
         */
        function onInit() {
            vm.inventoryItems = inventoryItems;
            vm.functionalStatuses = FUNCTIONAL_STATUS.getStatuses();
            vm.functionalStatus = $stateParams.functionalStatus;
            vm.userHasRightToEdit = canEdit;
            vm.isSupervised = $stateParams.supervised;
            vm.maintenanceRequests = maintenanceRequestByVendor;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name getStatusLabel
         *
         * @description
         * Return localized label for the functional status.
         *
         * @param   {string}    status  the status to get the label for
         * @return  {string}            the localized status label
         */
        function getStatusLabel(status) {
            return messageService.get(FUNCTIONAL_STATUS.getLabel(status));
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name getReasonLabel
         *
         * @description
         * Return localized label for the reason.
         *
         * @param   {string}    reason  the reason to get the label for
         * @return  {string}            the localized reason label
         */
        function getReasonLabel(reason) {
            if (!reason) {
                return '';
            }
            return messageService.get(REASON_FOR_NOT_WORKING.getLabel(reason));
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name goToStatusUpdate
         *
         * @description
         * Redirects user to maintenance item status update modal screen.
         *
         * @param {maintenanceItem} maintenanceItem the maintenance item which status will be updated
         */
        function goToStatusUpdate(maintenanceItem) {
            $state.go('openlmis.equipment.maintenance.item.statusUpdate', {
                maintenanceItem: maintenanceItem,
                maintenanceItemId: maintenanceItem.id
            });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name search
         *
         * @description
         * Reloads page with the new search parameters.
         */
        function search() {
            var stateParams = angular.copy($stateParams);

            delete stateParams.facilityId;
            delete stateParams.programId;

            stateParams.facility = vm.facility.id;
            stateParams.program = vm.program.id;
            stateParams.functionalStatus = vm.functionalStatus;
            stateParams.supervised = vm.isSupervised;

            $state.go('openlmis.equipment.maintenance', stateParams, {
                reload: true
            });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-maintenance-list.controller:EquipmentMaintenanceListController
         * @name goToStatusUpdate
         *
         * @description
         * Redirects user to maintenance item status update modal screen.
         *
         * @param {maintenanceItem} maintenanceItem the maintenance item which status will be updated
         */
        function goToResponse(maintenanceItem) {
            $state.go('openlmis.equipment.maintenance.response', {
                maintenanceItem: maintenanceItem,
                maintenanceItemId: maintenanceItem.id
            });

        }

    }

})();
