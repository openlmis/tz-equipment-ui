/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('ProgramEquipmentListController', function() {

    beforeEach(function() {
        module('program-equipment-list');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$state = $injector.get('$state');
            this.ProgramEquipmentTypeDataBuilder = $injector.get('ProgramEquipmentTypeDataBuilder');
        });

        this.equipmentTypes = [
            new this.ProgramEquipmentTypeDataBuilder().build(),
            new this.ProgramEquipmentTypeDataBuilder().build()
        ];

        this.stateParams = {
            page: 0,
            size: 10
        };

        this.programs = [];

        this.vm = this.$controller('ProgramEquipmentListController', {
            programEquipments: this.programEquipments,
            $stateParams: this.stateParams,
            equipmentTypes: this.equipmentTypes,
            programs: this.programs
        });
        this.vm.$onInit();
    });

    describe('onInit', function() {

        it('should expose  type array', function() {
            expect(this.vm.programEquipments).toEqual(this.programEquipments);
        });

    });

    describe('goToAddProgramEquipmentPage', function() {

        it('should go to add program equipment type state', function() {
            spyOn(this.$state, 'go');
            this.vm.goToAddProgramEquipmentPage();

            expect(this.$state.go).toHaveBeenCalledWith('openlmis.administration.programTypes.add');
        });
    });

});
