/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('programEquipmentService', function() {

    var $rootScope, $httpBackend;
    var equipmentUrlFactory, programEquipmentService, ProgramEquipmentTypeDataBuilder;
    var programEquipment, programEquipments, PROGRAM_EQUIPMENT_ID;

    beforeEach(function() {
        module('program-equipment-list');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            equipmentUrlFactory = $injector.get('equipmentUrlFactory');
            programEquipmentService = $injector.get('programEquipmentService');
            ProgramEquipmentTypeDataBuilder = $injector.get('ProgramEquipmentTypeDataBuilder');
        });

        prepareTestData();
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentTypeProgram/' + programEquipments[0].id))
                .respond(200, programEquipments[0]);
        });

        it('should return promise', function() {
            var result = programEquipmentService.get(programEquipments[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to program equipment', function() {
            var result;

            programEquipmentService.get(programEquipments[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(programEquipments[0]);

            expect(angular.toJson(result)).toEqual(angular.toJson(expected));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentTypeProgram/' + programEquipments[0].id));

            programEquipmentService.get(programEquipments[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var parameters = {
            page: 0,
            size: 10
        };

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentTypeProgram?page=' + parameters.page +
                '&size=' + parameters.size)).respond(200, {
                content: programEquipments
            });
        });

        it('should return promise', function() {
            var result = programEquipmentService.getAll(parameters);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentTypes', function() {
            var result;

            programEquipmentService.getAll(parameters).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(programEquipments);

            expect(angular.toJson(result)).toEqual(angular.toJson({
                content: expected
            }));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentTypeProgram?page=' + parameters.page +
                '&size=' + parameters.size));

            programEquipmentService.getAll(parameters);
            $httpBackend.flush();
        });
    });

    describe('save', function() {

        var result;

        beforeEach(function() {
            result = undefined;
        });

        it('should create equipmentType if ID is not given', function() {
            programEquipment = new ProgramEquipmentTypeDataBuilder().withoutId()
                .build();

            var returned = angular.copy(programEquipment);

            returned.id = PROGRAM_EQUIPMENT_ID;

            $httpBackend.expect(
                'POST', equipmentUrlFactory('/api/equipmentTypeProgram'), programEquipment
            ).respond(200, returned);

            programEquipmentService.save(programEquipment).then(function(programEquipment) {
                result = programEquipment;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(returned));
        });

        it('should update program equipment if it has ID', function() {
            programEquipment = new ProgramEquipmentTypeDataBuilder().withId(PROGRAM_EQUIPMENT_ID)
                .build();

            $httpBackend.expect(
                'PUT', equipmentUrlFactory('/api/equipmentTypeProgram/' + PROGRAM_EQUIPMENT_ID), programEquipment
            ).respond(200, programEquipment);

            programEquipmentService.save(programEquipment).then(function(programEquipment) {
                result = programEquipment;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(programEquipment));
        });

    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    function prepareTestData() {
        PROGRAM_EQUIPMENT_ID = 'some-program-equipment-id';

        programEquipments = [
            new ProgramEquipmentTypeDataBuilder().withId('35b8eeca-bfad-47f3-b966-c9cb726b872f')
                .build()

        ];
    }
});
