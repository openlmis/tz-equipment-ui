/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-type-list.controller:ProgramEquipmentListController
     *
     * @description
     * Controller for lab equipment type list.
     */
    angular
        .module('program-equipment-list')
        .controller('ProgramEquipmentListController', ProgramEquipmentListController);

    ProgramEquipmentListController.$inject = [
        '$state', '$stateParams', 'EQUIPMENT_RIGHTS', 'messageService', 'programEquipments',
        'programs', 'equipmentTypes', 'loadingModalService', 'programEquipmentService',
        'notificationService', '$q'
    ];

    function ProgramEquipmentListController($state, $stateParams, EQUIPMENT_RIGHTS, messageService,
                                            programEquipments, programs, equipmentTypes,
                                            loadingModalService, programEquipmentService,
                                            notificationService, $q) {
        var vm = this;
        vm.$onInit = onInit;

        vm.goToAddProgramEquipmentPage = goToAddProgramEquipmentPage;
        vm.filterByProgramId = filterByProgramId;
        vm.programs = undefined;
        vm.equipmentTypes = undefined;
        vm.remove = remove;

        /**
         * @ngdoc property
         * @propertyOf equipment-type-list.controller:EquipmentTypeListController
         * @name equipmentTypes
         * @type {Array}
         *
         * @description
         * Contains page of Equipment Types.
         */
        vm.programEquipments = undefined;
        /**
         * @ngdoc method
         * @methodOf equipment-type-list.controller:EquipmentTypeListController
         * @name onInit
         *
         * @description
         * Init method for EquipmentTypeListController.
         */
        function onInit() {
            vm.programEquipments = programEquipments;
            vm.programs = programs;
            vm.equipmentTypes = equipmentTypes;
        }

        function filterByProgramId(vm) {
            if (vm.selectedProgramId !== null) {
                loadingModalService.open();
                programEquipmentTypesResolve(vm.selectedProgramId);

            }
        }

        function programEquipmentTypesResolve(programId) {

            var deferred = $q.defer();
            var content = null;
            programEquipmentService.getAll({
                programId: programId
            }).then(function(response) {
                deferred.resolve(response.content);
                content = response.content;
                vm.programEquipments = content;
                loadingModalService.close();
            }, deferred.reject);

            return content;
        }

        function goToAddProgramEquipmentPage() {
            $state.go('openlmis.administration.programTypes.add');
        }

        function remove(id) {
            loadingModalService.open();
            return new programEquipmentService.remove(id)
                .then(function(programEquipment) {
                    notificationService.success('programEquipment.typeHasBeenDeleted');
                    $state.reload();
                    return programEquipment;
                })
                .catch(function() {
                    notificationService.error('programEquipment.failedToDeleteType');
                    loadingModalService.close();
                });
        }

    }

})();
