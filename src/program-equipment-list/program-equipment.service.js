/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name program-equipment-list.programEquipmentService
     *
     * @description
     * Responsible for retrieving program equipment from the server.
     */
    angular
        .module('program-equipment-list')
        .factory('programEquipmentService', service);

    service.$inject = ['equipmentUrlFactory', '$resource'];

    function service(equipmentUrlFactory, $resource) {

        var resource = $resource(equipmentUrlFactory('/api/equipmentTypeProgram/:id'), {}, {
            query: {
                url: equipmentUrlFactory('/api/equipmentTypeProgram'),
                method: 'GET',
                isArray: false
            },
            getAll: {
                url: equipmentUrlFactory('/api/equipmentTypeProgram'),
                method: 'GET'
            },
            get: {
                url: equipmentUrlFactory('/api/equipmentTypeProgram/:id'),
                method: 'GET'
            },
            remove: {
                url: equipmentUrlFactory('/api/equipmentTypeProgram/:id'),
                method: 'DELETE'
            },
            update: {
                method: 'PUT'
            },
            getPrograms: {
                url: equipmentUrlFactory('api/programs'),
                method: 'GET',
                isArray: true
            }
        });

        return {
            getAll: getAll,
            query: query,
            get: get,
            save: save,
            remove: remove,
            getPrograms: getPrograms
        };

        function getAll(paginationParams) {
            return resource.getAll(paginationParams).$promise;
        }

        function getPrograms(queryParams) {
            return resource.getPrograms(queryParams).$promise;
        }

        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipmentTypeService
             * @name getAll
             *
             * @description
             * Retrieves all equipment types by ids.
             *
             * @param  {Object}  queryParams the search parameters
             * @return {Promise} Page of equipment types
             */

        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        function remove(id) {
            return resource.remove({
                id: id
            }).$promise;
        }

        function save(type) {
            if (type.id) {
                return resource.update({
                    id: type.id
                }, type).$promise;
            }
            return resource.save({}, type).$promise;
        }

    }
})();
