/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipments-edit.controller:EquipmentEditController
     *
     * @description
     * Provides methods for Edit Equipment modal. Allows returning to previous state and editing Equipment.
     */
    angular
        .module('equipments-edit')
        .controller('EquipmentEditController', EquipmentEditController);

    EquipmentEditController.$inject = [
        'equipment', 'equipmentEnergyTypes', 'equipmentModels', 'equipmentCategories', 'confirmService',
        'equipmentService', 'stateTrackerService',
        '$state', 'loadingModalService', 'notificationService'
    ];

    function EquipmentEditController(equipment, equipmentEnergyTypes, equipmentModels, equipmentCategories,
                                     confirmService, equipmentService,
                                     stateTrackerService, $state, loadingModalService, notificationService) {
        var vm = this;

        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @methodOf equipments-edit.controller:EquipmentEditController
         * @name equipment
         * @type {Object}
         *
         * @description
         * Current Equipment.
         */
        vm.equipment = undefined;

        /**
         * @ngdoc property
         * @methodOf equipments-edit.controller:EquipmentEditController
         * @name editMode
         * @type {boolean}
         *
         * @description
         * Indicates if equipment is already created.
         */
        vm.editMode = undefined;

        /**
         * @ngdoc method
         * @methodOf equipments-edit.controller:EquipmentEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentEditController.
         */
        function onInit() {
            vm.equipment = equipment ? equipment : {};
            vm.equipmentEnergyTypes = equipmentEnergyTypes;
            vm.equipmentModels = equipmentModels;
            vm.equipmentCategories = equipmentCategories;
            vm.editMode = !!equipment;
        }

        /**
         * @ngdoc method
         * @methodOf equipments-edit.controller:EquipmentEditController
         * @name save
         *
         * @description
         * Saves the Equipment after confirm.
         */
        function save() {
            confirmService.confirm(
                vm.editMode ? 'equipmentEdit.save.question' : 'equipmentEdit.create.question',
                vm.editMode ? 'equipmentEdit.save' : 'equipmentEdit.create'
            ).then(function() {
                loadingModalService.open();
                getSavePromise()
                    .then(function() {
                        notificationService.success(vm.editMode ?
                            'equipmentEdit.save.success' : 'equipmentEdit.create.success');
                        stateTrackerService.goToPreviousState();
                    })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error(
                            vm.editMode
                                ? 'equipmentEdit.save.failure'
                                : 'equipmentEdit.create.failure'
                        );
                    });
            });
        }

        function getSavePromise() {
            return vm.editMode ?
                equipmentService.update(vm.equipment) :
                equipmentService.create(vm.equipment);
        }
    }
})();
