/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentEditController', function() {

    beforeEach(function() {
        module('equipments-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.confirmService = $injector.get('confirmService');
            this.$q = $injector.get('$q');
            this.equipmentService = $injector.get('equipmentService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentDataBuilder = $injector.get('EquipmentDataBuilder');
            this.EquipmentEnergyTypeDataBuilder = $injector.get('EquipmentEnergyTypeDataBuilder');
            this.EquipmentModelDataBuilder = $injector.get('EquipmentModelDataBuilder');
            this.EquipmentCategoryDataBuilder = $injector.get('EquipmentCategoryDataBuilder');
        });

        this.equipment = new this.EquipmentDataBuilder().build();
        this.equipmentEnergyTypes = [
            new this.EquipmentEnergyTypeDataBuilder().build(),
            new this.EquipmentEnergyTypeDataBuilder().build()
        ];

        this.equipmentModels = [
            new this.EquipmentModelDataBuilder().build(),
            new this.EquipmentModelDataBuilder().build()
        ];

        this.equipmentCategories = [
            new this.EquipmentCategoryDataBuilder().build(),
            new this.EquipmentCategoryDataBuilder().build()
        ];

        this.confirmDeferred = this.$q.defer();
        this.saveDeferred = this.$q.defer();

        spyOn(this.confirmService, 'confirm').andReturn(this.confirmDeferred.promise);
        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.equipmentService, 'update').andReturn(this.saveDeferred.promise);
        spyOn(this.equipmentService, 'create').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('EquipmentEditController', {
            equipment: this.equipment,
            equipmentEnergyTypes: this.equipmentEnergyTypes,
            equipmentModels: this.equipmentModels,
            equipmentCategories: this.equipmentCategories
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose equipment', function() {
            expect(this.vm.equipment).toEqual(this.equipment);
        });

        it('should expose resolved fields', function() {
            expect(this.vm.editMode).toEqual(true);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.confirmDeferred.resolve();
            this.saveDeferred.resolve(this.equipment);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            beforeEach(function() {
                this.vm.editMode = true;
            });

            it('should prompt user to confirm save', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentEdit.save.question',
                    'equipmentEdit.save'
                );
            });

            it('should not save equipment if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentService.update).not.toHaveBeenCalled();
                expect(this.equipmentService.create).not.toHaveBeenCalled();
            });

            it('should save equipment and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentService.update).toHaveBeenCalledWith(this.vm.equipment);
                expect(this.equipmentService.create).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipment);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentEdit.save.success');
            });

            it('should show notification if period save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentEdit.save.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });

        describe('while creating', function() {

            beforeEach(function() {
                this.vm.editMode = false;
            });

            it('should prompt user to save period', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentEdit.create.question',
                    'equipmentEdit.create'
                );
            });

            it('should not save equipment if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentService.create).not.toHaveBeenCalled();
                expect(this.equipmentService.update).not.toHaveBeenCalled();
            });

            it('should save equipment and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentService.create).toHaveBeenCalledWith(this.vm.equipment);
                expect(this.equipmentService.update).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipment);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentEdit.create.success');
            });

            it('should show notification if period save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentEdit.create.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });

    describe('goToPreviousState', function() {

        it('should redirect to Equipment List screen', function() {
            this.vm.goToPreviousState();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });
    });
});
