/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipments-edit')
        .config(routes);

    routes.$inject = ['modalStateProvider', 'EQUIPMENT_RIGHTS'];

    function routes(modalStateProvider, EQUIPMENT_RIGHTS) {
        modalStateProvider.state('openlmis.administration.equipments.edit', {
            controller: 'EquipmentEditController',
            controllerAs: 'vm',
            templateUrl: 'equipments-edit/equipments-edit.html',
            url: '/edit/:id',
            accessRights: [EQUIPMENT_RIGHTS.EQUIPMENT_MANAGE],
            resolve: {
                equipment: function($stateParams, equipmentService) {
                    if ($stateParams.id) {
                        return equipmentService.get($stateParams.id);
                    }
                    return undefined;
                },
                equipmentEnergyTypes: function(equipmentEnergyTypeService) {
                    return equipmentEnergyTypeService
                        .query()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                },
                equipmentModels: function(equipmentModelService) {
                    return equipmentModelService
                        .query()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                },
                equipmentCategories: function(equipmentCategoryService) {
                    return equipmentCategoryService
                        .getAll()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                }
            },
            parentResolves: []
        });
    }
})();
