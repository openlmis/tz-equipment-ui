/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentVendorListController', function() {

    var vm, $controller, vendors, EquipmentVendorDataBuilder;

    beforeEach(function() {
        module('equipment-vendor-list');

        inject(function($injector) {
            $controller = $injector.get('$controller');
            EquipmentVendorDataBuilder = $injector.get('EquipmentVendorDataBuilder');
        });

        vendors = [
            new EquipmentVendorDataBuilder().build(),
            new EquipmentVendorDataBuilder().build()
        ];

        vm = $controller('EquipmentVendorListController', {
            vendors: vendors
        });

        vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose vendors', function() {
            expect(vm.vendors).toEqual(vendors);
        });

    });

});