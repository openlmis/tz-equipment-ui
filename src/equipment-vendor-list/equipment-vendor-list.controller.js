/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-vendor-list.controller:VendorListController
     *
     * @description
     * Controller for managing vendors list screen.
     */
    angular
        .module('equipment-vendor-list')
        .controller('EquipmentVendorListController', controller);

    controller.$inject = ['vendors'];

    function controller(vendors) {
        var vm = this;

        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @propertyOf equipment-vendor-list.controller:VendorListController
         * @type {Array}
         * @name vendors
         *
         * @description
         * The list of all available vendors in the system.
         */
        vm.vendors = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-vendor-list.controller:VendorListController
         * @name $onInit
         *
         * @description
         * Initialization method of the VendorListController.
         */
        function onInit() {
            vm.vendors = vendors;
        }

    }

})();