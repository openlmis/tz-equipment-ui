/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-edit-inventory-item.controller:EditInventoryItemController
     *
     * @description
     * Provides methods for managing Edit Inventory Item modal.
     */
    angular
        .module('equipment-edit-inventory-item')
        .controller('EditInventoryItemController', controller);

    controller.$inject = [
        'inventoryItem', '$scope', '$state', 'equipmentFundSources', 'confirmService', 'REGISTRATION_STATUS',
        'UTILIZATION_STATUS', 'inventoryItemService', 'loadingModalService'
    ];

    function controller(inventoryItem, $scope, $state, equipmentFundSources, confirmService, REGISTRATION_STATUS,
                        UTILIZATION_STATUS, inventoryItemService, loadingModalService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.getUtilizationStatusLabel = UTILIZATION_STATUS.getLabel;
        vm.getRegistrationStatusLabel = REGISTRATION_STATUS.getLabel;
        vm.add = add;
        vm.cancel = cancel;

        /**
         * @ngdoc property
         * @propertyOf equipment-edit-inventory-item.controller:EditInventoryItemController
         * @type {Object}
         * @name inventoryItem
         *
         * @description
         * The exposed inventory item.
         */
        vm.inventoryItem = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @type {Array}
         * @name equipmentFundSources
         *
         * @description
         * The list of available equipments;
         */
        vm.equipmentFundSources = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-edit-inventory-item.controller:EditInventoryItemController
         * @name $onInit
         *
         * @description
         * Initialization method of the EditInventoryItemController.
         */
        function onInit() {
            vm.inventoryItem = angular.copy(inventoryItem);
            vm.utilizationStatuses = UTILIZATION_STATUS.getStatuses();
            vm.registrationStatuses = REGISTRATION_STATUS.getStatuses();
            vm.equipmentFundSources = equipmentFundSources;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-edit-inventory-item.controller:EditInventoryItemController
         * @name add
         *
         * @description
         * Takes the user to the status update screen.
         */
        function add() {
            if (vm.inventoryItem.id) {
                loadingModalService.open();
                inventoryItemService.save(vm.inventoryItem)
                    .then(function(inventoryItem) {
                        $state.go('openlmis.equipment.inventory.item.details', {
                            inventoryItem: inventoryItem,
                            inventoryItemId: inventoryItem.id
                        }, {
                            reload: true
                        });
                    })
                    .catch(loadingModalService.close);
            } else {
                $state.go('openlmis.equipment.inventory.item.statusUpdate', {
                    inventoryItem: vm.inventoryItem
                });
            }
        }

        /**
         * @ngdoc method
         * @methodOf equipment-edit-inventory-item.controller:EditInventoryItemController
         * @name cancel
         *
         * @description
         * Takes the user to the inventory item list screen. Will open a confirmation modal if user
         * interacted with the form.
         */
        function cancel() {
            if ($scope.editInventoryItemForm.$dirty) {
                confirmService.confirm(
                    'equipmentEditInventoryItem.closeAddInventoryItemModal',
                    'equipmentEditInventoryItem.yes',
                    'equipmentEditInventoryItem.no'
                ).then(doCancel);
            } else {
                doCancel();
            }
        }

        function doCancel() {
            if (inventoryItem.id) {
                $state.go('openlmis.equipment.inventory.item.details', {
                    inventoryItem: inventoryItem,
                    inventoryItemId: inventoryItem.id
                });
            } else {
                $state.go('openlmis.equipment.inventory');
            }
        }
    }

})();
