/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-fund-source')
        .factory('EquipmentFundSourceDataBuilder', EquipmentFundSourceDataBuilder);

    EquipmentFundSourceDataBuilder.$inject = ['EquipmentFundSource'];

    function EquipmentFundSourceDataBuilder(EquipmentFundSource) {

        EquipmentFundSourceDataBuilder.prototype.build = build;
        EquipmentFundSourceDataBuilder.prototype.withId = withId;
        EquipmentFundSourceDataBuilder.prototype.withName = withName;
        EquipmentFundSourceDataBuilder.prototype.withCode = withCode;

        return EquipmentFundSourceDataBuilder;

        function EquipmentFundSourceDataBuilder() {
            EquipmentFundSourceDataBuilder.instanceNumber = (EquipmentFundSourceDataBuilder.instanceNumber || 0) + 1;

            this.id = 'equipment-fund-source-id-' + EquipmentFundSourceDataBuilder.instanceNumber;
            this.code = '123';
            this.name = 'Test Fund Source';
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function build() {
            return new EquipmentFundSource(
                this.id,
                this.code,
                this.name
            );
        }

    }

})();