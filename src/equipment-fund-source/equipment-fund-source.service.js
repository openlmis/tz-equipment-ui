/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-fund-source.EquipmentFundSourceService
     *
     * @description
     * Responsible for retrieving all equipment fund source information from server.
     */
    angular
        .module('equipment-fund-source')
        .service('equipmentFundSourceService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/equipmentFundSources/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/equipmentFundSources'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/equipmentFundSources/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;

        /**
             * @ngdoc method
             * @methodOf equipment-fund-source.EquipmentFundSourceService
             * @name get
             *
             * @description
             * Retrieves equipment fund source by id.
             *
             * @param  {String}  equipmentFundSourceId equipmentFundSource UUID
             * @return {Promise}                equipmentFundSource promise
             */
        function get(equipmentFundSourceId) {
            return resource.get({
                id: equipmentFundSourceId
            }).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-fund-source.EquipmentFundSourceService
             * @name getAll
             *
             * @description
             * Retrieves all equipment fund sources by ids.
             *
             * @param  {Object}  queryParams the search parameters
             * @return {Promise} Page of equipment fund sources
             */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-fund-source.EquipmentFundSourceService
             * @name update
             *
             * @description
             * Updates existing equipment fund source.
             *
             * @param  {Object}  equipmentFundSource equipment fund source that will be saved
             * @return {Promise}              updated equipment fund source
             */
        function update(equipmentFundSource) {
            return resource.update({
                id: equipmentFundSource.id
            }, equipmentFundSource).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-fund-source.EquipmentFundSourceService
             * @name create
             *
             * @description
             * Creates new equipment fund source.
             *
             * @param  {Object}  equipmentFundSource equipment fund source that will be created
             * @return {Promise}              created equipment fund source
             */
        function create(equipmentFundSource) {
            return resource.save(equipmentFundSource).$promise;
        }
    }
})();
