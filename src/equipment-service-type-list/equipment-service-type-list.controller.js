/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-service-type-list.controller:EquipmentServiceTypeListController
     *
     * @description
     * Controller for managing equipment service types list screen.
     */
    angular
        .module('equipment-service-type-list')
        .controller('EquipmentServiceTypeListController', controller);

    controller.$inject = [
        'equipmentServiceTypes', 'confirmService', 'equipmentServiceTypeService',
        'loadingModalService', 'notificationService', 'messageService', '$state'
    ];

    function controller(equipmentServiceTypes, confirmService, equipmentServiceTypeService,
                        loadingModalService, notificationService, messageService, $state) {

        var vm = this;

        vm.$onInit = onInit;
        vm.deleteServiceType = deleteServiceType;

        /**
         * @ngdoc property
         * @propertyOf equipment-service-type-list.controller:EquipmentServiceTypeListController
         * @name equipmentServiceTypes
         * @type {Array}
         *
         * @description
         * Contains page of equipment service types.
         */
        vm.equipmentServiceTypes = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-service-type-list.controller:EquipmentServiceTypeListController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentServiceTypeListController.
         */
        function onInit() {
            vm.equipmentServiceTypes = equipmentServiceTypes;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-service-type-list.controller:EquipmentServiceTypeListController
         * @name deleteServiceType
         *
         * @description
         * Method that is used to delete service type.
         */
        function deleteServiceType(serviceType) {
            var confirmMessage = messageService.get('equipmentServiceTypeList.deleteServiceType.question', {
                period: serviceType.name
            });

            confirmService.confirm(confirmMessage,
                'equipmentServiceTypeList.deleteServiceType').then(function() {
                var loadingPromise = loadingModalService.open();
                equipmentServiceTypeService.deleteServiceType(serviceType.id).then(function() {
                    loadingPromise.then(function() {
                        notificationService.success('equipmentServiceTypeList.deleteServiceType.success');
                    });
                    $state.reload('openlmis.administration.equipmentServiceTypes');
                })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error('equipmentServiceTypeList.deleteServiceType.fail');
                    });
            });
        }
    }
})();
