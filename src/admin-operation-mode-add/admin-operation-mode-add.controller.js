/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name admin-operation-mode-add.controller:OperationModeAddController
     *
     * @description
     * Provides methods for Add OperationMode modal. Allows returning to previous states and saving
     * operationMode.
     */
    angular
        .module('admin-operation-mode-add')
        .controller('OperationModeAddController', OperationModeAddController);

    OperationModeAddController.$inject = [
        'operationMode', 'confirmService', 'stateTrackerService', '$state', 'loadingModalService',
        'notificationService', 'messageService', 'operationModeService'
    ];

    function OperationModeAddController(operationMode, confirmService, stateTrackerService,
                                        $state, loadingModalService, notificationService,
                                        messageService, operationModeService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;

        /**
         * @ngdoc method
         * @methodOf admin-operation-mode-add.controller:OperationModeAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the OperationModeAddController.
         */
        function onInit() {
            vm.operationMode = operationMode;
        }

        function save() {
            loadingModalService.open();
            return new operationModeService.save(vm.operationMode)
                .then(function(operationMode) {
                    notificationService.success('adminOperationModeAdd.operationModeHasBeenSaved');
                    stateTrackerService.goToPreviousState();
                    return operationMode;
                })
                .catch(function() {
                    notificationService.error('adminOperationModeAdd.failedToSaveOperationMode');
                    loadingModalService.close();
                });
        }
    }

})();
