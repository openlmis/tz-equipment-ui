/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-orderable-list.controller:EquipmentOderablesListController
     *
     * @description
     * Controller for lab equipment orderables list.
     */
    angular
        .module('equipment-orderable-list')
        .controller('EquipmentOderablesListController', EquipmentOderablesListController);

    EquipmentOderablesListController.$inject = [
        '$state', '$stateParams', 'EQUIPMENT_RIGHTS', 'messageService', 'equipmentsOrderables',
        'orderables', 'loadingModalService', 'equipmentOrderableService',
        'notificationService'
    ];

    function EquipmentOderablesListController($state, $stateParams, EQUIPMENT_RIGHTS, messageService,
                                              equipmentsOrderables, orderables,
                                              loadingModalService, equipmentOrderableService,
                                              notificationService) {
        var vm = this;
        vm.$onInit = onInit;

        vm.goToAddEquipmentOrderablePage = goToAddEquipmentOrderablePage;
        vm.orderables = undefined;
        vm.remove = remove;

        /**
         * @ngdoc property
         * @propertyOf equipment-orderable-list.controller:EquipmentOderablesListController
         * @name equipmentTypes
         * @type {Array}
         *
         * @description
         * Contains page of Equipment Types.
         */
        vm.equipmentsOrderables = undefined;
        /**
         * @ngdoc method
         * @methodOf equipment-orderable-list.controller:EquipmentOderablesListController
         * @name onInit
         *
         * @description
         * Init method for EquipmentOderablesListController.
         */
        function onInit() {
            vm.equipmentsOrderables = equipmentsOrderables;
            vm.orderables = orderables;
            vm.goToAddEquipmentOrderablePage = goToAddEquipmentOrderablePage;
        }

        function goToAddEquipmentOrderablePage() {
            $state.go('openlmis.administration.equipmentsOrderable.add');
        }

        function remove(id) {
            loadingModalService.open();
            return new equipmentOrderableService.remove(id)
                .then(function(programEquipment) {
                    notificationService.success('programEquipment.typeHasBeenDeleted');
                    $state.reload();
                    return programEquipment;
                })
                .catch(function() {
                    notificationService.error('programEquipment.failedToDeleteType');
                    loadingModalService.close();
                });
        }

    }

})();
