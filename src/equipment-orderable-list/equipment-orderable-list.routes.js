/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular.module('equipment-orderable-list').config(routes);

    routes.$inject = ['$stateProvider', 'EQUIPMENT_RIGHTS'];

    function routes($stateProvider, EQUIPMENT_RIGHTS) {
        $stateProvider.state('openlmis.administration.equipmentsOrderable', {
            showInNavigation: true,
            label: 'equipment.orderables',
            url: '/equipmentOrderables?page&size',
            controller: 'EquipmentOderablesListController',
            templateUrl: 'equipment-orderable-list/equipment-orderable-list.html',
            controllerAs: 'vm',
            accessRights: [EQUIPMENT_RIGHTS.EQUIPMENT_MANAGE],
            resolve: {
                equipmentsOrderables: function(paginationService, equipmentOrderableService, $stateParams) {
                    return paginationService.registerUrl($stateParams, function(stateParams) {
                        var params = {},
                            sortParam = {};

                        sortParam.sort = 'equipment,asc';

                        angular.merge(params, stateParams, sortParam);

                        return equipmentOrderableService.query(params);
                    });
                },
                orderables: function(equipmentOrderableService) {
                    return equipmentOrderableService.getOrderables()
                        .then(function(response) {
                            return response.content;
                        });
                }
            }

        });
    }
})();
