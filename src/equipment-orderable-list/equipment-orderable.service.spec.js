/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('equipmentOrderableService', function() {

    var $rootScope, $httpBackend;
    var equipmentUrlFactory, equipmentOrderableService, EquipmentOrderableDataBuilder;
    var equipmentOrderable, equipmentOrderables, EQUIPMENT_ORDERABLE_ID;

    beforeEach(function() {
        module('equipment-orderable-list');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            equipmentUrlFactory = $injector.get('equipmentUrlFactory');
            equipmentOrderableService = $injector.get('equipmentOrderableService');
            EquipmentOrderableDataBuilder = $injector.get('EquipmentOrderableDataBuilder');
        });

        prepareTestData();
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentOrderables/' + equipmentOrderables[0].id))
                .respond(200, equipmentOrderables[0]);
        });

        it('should return promise', function() {
            var result = equipmentOrderableService.get(equipmentOrderables[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipment orderables', function() {
            var result;

            equipmentOrderableService.get(equipmentOrderables[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentOrderables[0]);

            expect(angular.toJson(result)).toEqual(angular.toJson(expected));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentOrderables/' + equipmentOrderables[0].id));

            equipmentOrderableService.get(equipmentOrderables[0].id);
            $httpBackend.flush();
        });
    });

    describe('query', function() {

        var parameters = {
            page: 0,
            size: 10
        };

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentOrderables?page=' + parameters.page +
                '&size=' + parameters.size)).respond(200, {
                content: equipmentOrderables
            });
        });

        it('should return promise', function() {
            var result = equipmentOrderableService.query(parameters);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentOrderables', function() {
            var result;

            equipmentOrderableService.query(parameters).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentOrderables);

            expect(angular.toJson(result)).toEqual(angular.toJson({
                content: expected
            }));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentOrderables?page=' + parameters.page +
                '&size=' + parameters.size));

            equipmentOrderableService.query(parameters);
            $httpBackend.flush();
        });
    });

    describe('save', function() {

        var result;

        beforeEach(function() {
            result = undefined;
        });

        it('should create equipmentOrderables if ID is not given', function() {
            equipmentOrderable = new EquipmentOrderableDataBuilder().withoutId()
                .build();

            var returned = angular.copy(equipmentOrderable);

            returned.id = EQUIPMENT_ORDERABLE_ID;

            $httpBackend.expect(
                'POST', equipmentUrlFactory('/api/equipmentOrderables'), equipmentOrderable
            ).respond(200, returned);

            equipmentOrderableService.save(equipmentOrderable).then(function(equipmentOrderable) {
                result = equipmentOrderable;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(returned));
        });

    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    function prepareTestData() {
        EQUIPMENT_ORDERABLE_ID = 'some-equipment-orderable-id';

        equipmentOrderables = [
            new EquipmentOrderableDataBuilder().withId('35b8eeca-bfad-47f3-b966-c9cb726b872f')
                .build()

        ];
    }
});
