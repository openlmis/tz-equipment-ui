/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-orderable-list.equipmentOrderableService
     *
     * @description
     * Responsible for retrieving  equipment orderables from the server.
     */
    angular
        .module('equipment-orderable-list')
        .factory('equipmentOrderableService', service);

    service.$inject = ['equipmentUrlFactory', '$resource'];

    function service(equipmentUrlFactory, $resource) {

        var resource = $resource(equipmentUrlFactory('/api/equipmentOrderables/:id'), {}, {
            query: {
                url: equipmentUrlFactory('/api/equipmentOrderables'),
                method: 'GET',
                isArray: false
            },
            get: {
                url: equipmentUrlFactory('/api/equipmentOrderables/:id'),
                method: 'GET'
            },
            remove: {
                url: equipmentUrlFactory('/api/equipmentOrderables/:id'),
                method: 'DELETE'
            },
            getOrderables: {
                url: equipmentUrlFactory('api/orderables'),
                method: 'GET'
            }
        });

        return {
            query: query,
            get: get,
            save: save,
            remove: remove,
            getOrderables: getOrderables
        };

        function getOrderables(queryParams) {
            return resource.getOrderables(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-orderable-list.equipmentOrderableService
         * @name query
         *
         * @description
         * Retrieves all equipment orderables by params.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Page of equipment orderables
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        function remove(id) {
            return resource.remove({
                id: id
            }).$promise;
        }

        function save(type) {
            return resource.save(type).$promise;
        }

    }
})();
