/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-model-edit.controller:EquipmentModelEditController
     *
     * @description
     * Provides methods for Edit Equipment Model modal. Allows returning to previous state and editing Equipment Model.
     */
    angular
        .module('equipment-model-edit')
        .controller('EquipmentModelEditController', EquipmentModelEditController);

    EquipmentModelEditController.$inject = [
        'equipmentModel', 'equipmentTypes', 'confirmService', 'equipmentModelService', 'stateTrackerService',
        '$state', 'loadingModalService', 'notificationService'
    ];

    function EquipmentModelEditController(equipmentModel, equipmentTypes, confirmService, equipmentModelService,
                                          stateTrackerService, $state, loadingModalService, notificationService) {
        var vm = this;

        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @methodOf equipment-model-edit.controller:EquipmentModelEditController
         * @name equipmentModel
         * @type {Object}
         *
         * @description
         * Current Equipment Model.
         */
        vm.equipmentModel = undefined;

        /**
         * @ngdoc property
         * @methodOf equipment-model-edit.controller:EquipmentModelEditController
         * @name editMode
         * @type {boolean}
         *
         * @description
         * Indicates if equipment Model is already created.
         */
        vm.editMode = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-model-edit.controller:EquipmentModelEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentModelEditController.
         */
        function onInit() {
            vm.equipmentModel = equipmentModel ? equipmentModel : {};
            vm.equipmentTypes = equipmentTypes;
            vm.editMode = !!equipmentModel;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-model-edit.controller:EquipmentModelEditController
         * @name save
         *
         * @description
         * Saves the Equipment Model after confirm.
         */
        function save() {
            confirmService.confirm(
                vm.editMode ? 'equipmentModelEdit.save.question' : 'equipmentModelEdit.create.question',
                vm.editMode ? 'equipmentModelEdit.save' : 'equipmentModelEdit.create'
            ).then(function() {
                loadingModalService.open();
                getSavePromise()
                    .then(function() {
                        notificationService.success(vm.editMode ?
                            'equipmentModelEdit.save.success' : 'equipmentModelEdit.create.success');
                        stateTrackerService.goToPreviousState();
                    })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error(
                            vm.editMode
                                ? 'equipmentModelEdit.save.failure'
                                : 'equipmentModelEdit.create.failure'
                        );
                    });
            });
        }

        function getSavePromise() {
            return vm.editMode ?
                equipmentModelService.update(vm.equipmentModel) :
                equipmentModelService.create(vm.equipmentModel);
        }
    }
})();
