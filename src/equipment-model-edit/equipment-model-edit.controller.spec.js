/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentModelEditController', function() {

    beforeEach(function() {
        module('equipment-model-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.confirmService = $injector.get('confirmService');
            this.$q = $injector.get('$q');
            this.equipmentModelService = $injector.get('equipmentModelService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentModelDataBuilder = $injector.get('EquipmentModelDataBuilder');
            this.EquipmentTypeDataBuilder = $injector.get('EquipmentTypeDataBuilder');
        });

        this.equipmentModel = new this.EquipmentModelDataBuilder().build();
        this.equipmentTypes = [
            new this.EquipmentTypeDataBuilder().build(),
            new this.EquipmentTypeDataBuilder().build()
        ];

        this.confirmDeferred = this.$q.defer();
        this.saveDeferred = this.$q.defer();

        spyOn(this.confirmService, 'confirm').andReturn(this.confirmDeferred.promise);
        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.equipmentModelService, 'update').andReturn(this.saveDeferred.promise);
        spyOn(this.equipmentModelService, 'create').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('EquipmentModelEditController', {
            equipmentModel: this.equipmentModel,
            equipmentTypes: this.equipmentTypes
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose equipmentModel', function() {
            expect(this.vm.equipmentModel).toEqual(this.equipmentModel);
        });

        it('should expose resolved fields', function() {
            expect(this.vm.editMode).toEqual(true);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.confirmDeferred.resolve();
            this.saveDeferred.resolve(this.equipmentModel);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            beforeEach(function() {
                this.vm.editMode = true;
            });

            it('should prompt user to confirm save', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentModelEdit.save.question',
                    'equipmentModelEdit.save'
                );
            });

            it('should not save equipment model if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentModelService.update).not.toHaveBeenCalled();
                expect(this.equipmentModelService.create).not.toHaveBeenCalled();
            });

            it('should save equipment model and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentModelService.update).toHaveBeenCalledWith(this.vm.equipmentModel);
                expect(this.equipmentModelService.create).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment model was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentModel);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentModelEdit.save.success');
            });

            it('should show notification if period save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentModelEdit.save.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });

        describe('while creating', function() {

            beforeEach(function() {
                this.vm.editMode = false;
            });

            it('should prompt user to save period', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentModelEdit.create.question',
                    'equipmentModelEdit.create'
                );
            });

            it('should not save equipment model if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentModelService.create).not.toHaveBeenCalled();
                expect(this.equipmentModelService.update).not.toHaveBeenCalled();
            });

            it('should save equipment model and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentModelService.create).toHaveBeenCalledWith(this.vm.equipmentModel);
                expect(this.equipmentModelService.update).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment model was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentModel);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentModelEdit.create.success');
            });

            it('should show notification if period save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentModelEdit.create.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });

    describe('goToPreviousState', function() {

        it('should redirect to Equipment Model List screen', function() {
            this.vm.goToPreviousState();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });
    });
});
