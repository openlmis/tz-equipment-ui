/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-operation-mode')
        .factory('OperationModeDataBuilder', OperationModeDataBuilder);

    OperationModeDataBuilder.$inject = ['OperationMode'];

    function OperationModeDataBuilder(OperationMode) {

        OperationModeDataBuilder.prototype.build = build;
        OperationModeDataBuilder.prototype.withId = withId;
        OperationModeDataBuilder.prototype.withName = withName;
        OperationModeDataBuilder.prototype.withCode = withCode;
        OperationModeDataBuilder.prototype.withDisplayOrder = withDisplayOrder;
        OperationModeDataBuilder.prototype.withoutId = withoutId;
        OperationModeDataBuilder.prototype.deactivated = deactivated;

        return OperationModeDataBuilder;

        function OperationModeDataBuilder() {
            OperationModeDataBuilder.instanceNumber = (OperationModeDataBuilder.instanceNumber || 0) + 1;

            this.id = 'operation-mode-id-' + OperationModeDataBuilder.instanceNumber;
            this.code = 'code';
            this.name = 'name';
            this.description = 'description';
            this.displayOrder = 2;
            this.active = true;
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function withDisplayOrder(displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        function withoutId() {
            this.id = undefined;
            return this;
        }

        function deactivated() {
            this.active = false;
            return this;
        }

        function build() {
            return new OperationMode(
                this.id,
                this.code,
                this.name,
                this.description,
                this.displayOrder,
                this.active
            );
        }

    }
})();