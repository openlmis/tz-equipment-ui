/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-vendor.EquipmentVendor
     *
     * @description
     * Represents a single stock equipmentVendor.
     */
    angular
        .module('equipment-vendor')
        .factory('EquipmentVendor', EquipmentVendor);

    EquipmentVendor.$inject = ['$q'];

    function EquipmentVendor($q) {

        EquipmentVendor.prototype.addAssociatedUser = addAssociatedUser;
        EquipmentVendor.prototype.removeAssociatedUser = removeAssociatedUser;

        return EquipmentVendor;

        /**
         * @ngdoc method
         * @methodOf equipment-vendor.EquipmentVendor
         * @name EquipmentVendor
         *
         * @description
         * Creates a new instance of the EquipmentVendor class.
         *
         * @param  {Object}                json       the JSON representation of the EquipmentVendor
         * @return {EquipmentVendor}                           the EquipmentVendor object
         */
        function EquipmentVendor(json) {
            if (!json) {
                json = {};
            }

            this.id = json.id;
            this.name = json.name;
            this.website = json.website;
            this.contactPerson = json.contactPerson;
            this.primaryPhone = json.primaryPhone;
            this.email = json.email;
            this.description = json.description;
            this.specialization = json.specialization;
            this.geographicalCoverage = json.geographicalCoverage;
            this.associatedUsers = json.associatedUsers ? json.associatedUsers : [];
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor.EquipmentVendor
         * @name addAssociatedUser
         *
         * @description
         * Adds the given associatedUser to the list of equipmentVendor associatedUser. Also stores information if
         * the associatedUser has been previously removed and not saved. If an already existing
         * associatedUser is added this method will return a rejected promise.
         *
         * @param  {Object}  associatedUser the associatedUser to add
         * @return {Promise}            the promise resolved when associatedUser was successfully added
         */
        function addAssociatedUser(associatedUser) {
            var error = validateVendorAssociatedUserDoesNotExist(this, associatedUser);
            if (error) {
                return $q.reject(error);
            }

            this.associatedUsers.push(associatedUser);

            return $q.resolve();
        }

        /**
         * @ngdoc method
         * @methodOf equipment-vendor.EquipmentVendor
         * @name removeAssociatedUser
         *
         * @description
         * Removes the given associatedUser from the list of equipmentVendor associatedUsers. Also stores information
         * if the associatedUser has been previously added and not saved.
         *
         * @param {Object} associatedUser the associatedUser to be removed
         */
        function removeAssociatedUser(associatedUser) {
            var error = validateVendorAssociatedUserExists(this, associatedUser);
            if (error) {
                return $q.reject(error);
            }

            this.associatedUsers.splice(this.associatedUsers.indexOf(associatedUser), 1);
            return $q.resolve();
        }
        function validateVendorAssociatedUserDoesNotExist(vendor, user) {
            if (isVendorAssociatedUserDuplicated(vendor, user)) {
                return 'equipmentVendorAdd.associatedUserDuplicated';
            }
        }

        function validateVendorAssociatedUserExists(vendor, user) {
            if (vendor.associatedUsers.indexOf(user) === -1) {
                return 'equipmentVendorAdd.associatedUserNotAdded';
            }
        }

        function isVendorAssociatedUserDuplicated(vendor, newUser) {
            return vendor.associatedUsers.filter(function(user) {
                return user === newUser;
            }).length;
        }

    }

})();
