/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-vendor')
        .factory('EquipmentVendorDataBuilder', EquipmentVendorDataBuilder);

    EquipmentVendorDataBuilder.$inject = ['EquipmentVendor'];

    function EquipmentVendorDataBuilder(EquipmentVendor) {

        EquipmentVendorDataBuilder.prototype.build = build;
        EquipmentVendorDataBuilder.prototype.buildJson = buildJson;
        EquipmentVendorDataBuilder.prototype.buildResponse = buildResponse;
        EquipmentVendorDataBuilder.prototype.withId = withId;
        EquipmentVendorDataBuilder.prototype.withAssociatedUsers = withAssociatedUsers;
        EquipmentVendorDataBuilder.prototype.withoutId = withoutId;

        return EquipmentVendorDataBuilder;

        function EquipmentVendorDataBuilder() {
            EquipmentVendorDataBuilder.instanceNumber = (EquipmentVendorDataBuilder.instanceNumber || 0) + 1;

            this.id = 'equipmentVendor-id' + EquipmentVendorDataBuilder.instanceNumber;
            this.name = 'Name' + EquipmentVendorDataBuilder.instanceNumber;
            this.associatedUsers = ['user-1', 'user-2'];
        }

        function build() {
            return new EquipmentVendor(this.buildJson());
        }

        function buildJson() {
            var json = this.buildResponse();

            json.associatedUsers = this.associatedUsers;

            return json;
        }

        function buildResponse() {
            return {
                id: this.id,
                name: this.name
            };
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withAssociatedUsers(associatedUsers) {
            this.associatedUsers = associatedUsers;
            return this;
        }

        function withoutId() {
            this.id = undefined;
            return this;
        }

    }

})();
