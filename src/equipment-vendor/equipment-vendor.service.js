/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-equipment-vendor.EquipmentVendorService
     *
     * @description
     * Responsible for retrieving all equipment vendor information from server.
     */
    angular
        .module('equipment-vendor')
        .service('equipmentVendorService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/vendors/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/vendors'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/vendors/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;
        this.save = save;

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-vendor.EquipmentVendorService
         * @name get
         *
         * @description
         * Retrieves equipment vendor by id.
         *
         * @param  {String}  equipmentVendorId equipmentVendor UUID
         * @return {Promise}                equipmentVendor promise
         */
        function get(equipmentVendorId) {
            return resource.get({
                id: equipmentVendorId
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-vendor.EquipmentVendorService
         * @name query
         *
         * @description
         * Retrieves all equipment vendors by ids.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Page of equipment vendors
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-vendor.EquipmentVendorService
         * @name update
         *
         * @description
         * Updates existing equipment vendor.
         *
         * @param  {Object}  equipmentVendor equipment vendor that will be saved
         * @return {Promise}              updated equipment vendor
         */
        function update(equipmentVendor) {
            return resource.update({
                id: equipmentVendor.id
            }, equipmentVendor).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-vendor.EquipmentVendorService
         * @name create
         *
         * @description
         * Creates new equipment vendor.
         *
         * @param  {Object}  equipmentVendor equipment vendor that will be created
         * @return {Promise}              created equipment vendor
         */
        function create(equipmentVendor) {
            return resource.save(equipmentVendor).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-equipment-vendor.EquipmentVendorService
         * @name save
         *
         * @description
         * Saves equipmentVendor in the repository.
         *
         * @param  {Object}  equipmentVendor equipment vendor that will be created
         * @return {Promise} the promise resolving to saved EquipmentVendor, rejected if save was unsuccessful
         */
        function save(equipmentVendor) {
            if (equipmentVendor.id) {
                return update(equipmentVendor);
            }
            return create(equipmentVendor);
        }
    }
})();
