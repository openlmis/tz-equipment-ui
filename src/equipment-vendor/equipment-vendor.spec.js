/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentVendor', function() {

    var EquipmentVendorDataBuilder, EquipmentVendor, equipmentVendor, json, $rootScope;

    beforeEach(function() {
        module('equipment-vendor');

        inject(function($injector) {
            EquipmentVendor = $injector.get('EquipmentVendor');
            EquipmentVendorDataBuilder = $injector.get('EquipmentVendorDataBuilder');
            $rootScope = $injector.get('$rootScope');
        });

        json = new EquipmentVendorDataBuilder().buildJson();
    });

    describe('constructor', function() {

        it('should set properties if json is given', function() {
            var result = new EquipmentVendor(json);

            expect(result.id).toEqual(json.id);
            expect(result.name).toEqual(json.name);
            expect(result.associatedUsers).toEqual(json.associatedUsers);
        });

        it('should set defaults if json is not given', function() {
            var result = new EquipmentVendor();

            expect(result.id).toBeUndefined();
            expect(result.name).toBeUndefined();
            expect(result.associatedUsers).toEqual([]);
        });

    });

    describe('addAssociatedUsers', function() {

        beforeEach(function() {
            equipmentVendor = new EquipmentVendor(json);
        });

        it('should reject if associatedUser already exist', function() {
            var result;
            equipmentVendor.addAssociatedUser(equipmentVendor.associatedUsers[0])
                .catch(function(error) {
                    result = error;
                });
            $rootScope.$apply();

            expect(result).toEqual('equipmentVendorAdd.associatedUserDuplicated');
        });

        it('should add associatedUser if it is not duplicate', function() {
            var associatedUser = 'new-user-1';

            equipmentVendor.addAssociatedUser(associatedUser);
            $rootScope.$apply();

            expect(equipmentVendor.associatedUsers.length).toBe(3);
            expect(equipmentVendor.associatedUsers[2]).toEqual(associatedUser);
        });

    });

    describe('removeAssociatedUser', function() {

        beforeEach(function() {
            equipmentVendor = new EquipmentVendor(json);
        });

        it('should resolve when associatedUser was remove successfully', function() {
            equipmentVendor.removeAssociatedUser(equipmentVendor.associatedUsers[0]);
            $rootScope.$apply();

            expect(equipmentVendor.associatedUsers.length).toBe(1);
        });

        it('should return resolved promise if remove was successful', function() {
            var resolved;
            equipmentVendor.removeAssociatedUser(equipmentVendor.associatedUsers[0])
                .then(function() {
                    resolved = true;
                });
            $rootScope.$apply();

            expect(resolved).toBe(true);
        });

        it('should return rejected promise if trying to remove non-existent associatedUsers', function() {
            var result;
            equipmentVendor.removeAssociatedUser('new-user-99')
                .catch(function(error) {
                    result = error;
                });
            $rootScope.$apply();

            expect(result).toBe('equipmentVendorAdd.associatedUserNotAdded');
        });

    });

});