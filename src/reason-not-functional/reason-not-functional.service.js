/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name reason-not-functional.ReasonNotFunctionalService
     *
     * @description
     * Responsible for retrieving all reason not functional information from server.
     */
    angular
        .module('reason-not-functional')
        .service('reasonNotFunctionalService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/reasonsNotFunctional/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/reasonsNotFunctional'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/reasonsNotFunctional/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;

        /**
             * @ngdoc method
             * @methodOf reason-not-functional.ReasonNotFunctionalService
             * @name get
             *
             * @description
             * Retrieves reason not functional by id.
             *
             * @param  {String}  reasonNotFunctionalId reasonNotFunctional UUID
             * @return {Promise}                reasonNotFunctional promise
             */
        function get(reasonNotFunctionalId) {
            return resource.get({
                id: reasonNotFunctionalId
            }).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf reason-not-functional.ReasonNotFunctionalService
             * @name getAll
             *
             * @description
             * Retrieves all reason not functionals by ids.
             *
             * @param  {Object}  queryParams the search parameters
             * @return {Promise} Page of reason not functionals
             */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf reason-not-functional.ReasonNotFunctionalService
             * @name update
             *
             * @description
             * Updates existing reason not functional.
             *
             * @param  {Object}  reasonNotFunctional reason not functional that will be saved
             * @return {Promise}              updated reason not functional
             */
        function update(reasonNotFunctional) {
            return resource.update({
                id: reasonNotFunctional.id
            }, reasonNotFunctional).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf reason-not-functional.ReasonNotFunctionalService
             * @name create
             *
             * @description
             * Creates new reason not functional.
             *
             * @param  {Object}  reasonNotFunctional reason not functional that will be created
             * @return {Promise}              created reason not functional
             */
        function create(reasonNotFunctional) {
            return resource.save(reasonNotFunctional).$promise;
        }
    }
})();
