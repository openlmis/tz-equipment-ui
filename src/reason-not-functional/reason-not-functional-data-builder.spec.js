/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('reason-not-functional')
        .factory('ReasonNotFunctionalDataBuilder', ReasonNotFunctionalDataBuilder);

    ReasonNotFunctionalDataBuilder.$inject = ['ReasonNotFunctional'];

    function ReasonNotFunctionalDataBuilder(ReasonNotFunctional) {

        ReasonNotFunctionalDataBuilder.prototype.build = build;
        ReasonNotFunctionalDataBuilder.prototype.withId = withId;
        ReasonNotFunctionalDataBuilder.prototype.withName = withName;
        ReasonNotFunctionalDataBuilder.prototype.withCode = withCode;
        ReasonNotFunctionalDataBuilder.prototype.withDisplayOrder = withDisplayOrder;

        return ReasonNotFunctionalDataBuilder;

        function ReasonNotFunctionalDataBuilder() {
            ReasonNotFunctionalDataBuilder.instanceNumber = (ReasonNotFunctionalDataBuilder.instanceNumber || 0) + 1;

            this.id = 'reason-not-functional-id-' + ReasonNotFunctionalDataBuilder.instanceNumber;
            this.code = 'Test Reason Code';
            this.name = 'Test Reason Name';
            this.displayOrder = 0;
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withDisplayOrder(displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        function build() {
            return new ReasonNotFunctional(
                this.id,
                this.code,
                this.name,
                this.displayOrder
            );
        }

    }

})();