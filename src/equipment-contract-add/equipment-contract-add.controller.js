/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */
(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-contract-add.controller:EquipmentContractAddController
     *
     * @description
     * Exposes method for creating/updating equipmentContract to the modal view.
     */
    angular
        .module('equipment-contract-add')
        .controller('EquipmentContractAddController', controller);

    controller.$inject = ['contracts', 'equipmentContract', 'facilities', 'facilitiesMap',
        'vendors', 'equipments', 'serviceTypes',
        'EquipmentContract', 'equipmentContractService', 'stateTrackerService',
        'loadingModalService', 'notificationService'];

    function controller(equipmentContracts, equipmentContract, facilities, facilitiesMap,
                        vendors, equipments, serviceTypes,
                        EquipmentContract, equipmentContractService,
                        stateTrackerService, loadingModalService, notificationService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.addFacility = addFacility;
        vm.removeFacility = removeFacility;
        vm.addEquipment = addEquipment;
        vm.removeEquipment = removeEquipment;
        vm.addServiceType = addServiceType;
        vm.removeServiceType = removeServiceType;
        vm.toFacilityIdName = toFacilityIdName;
        vm.save = save;
        vm.validateEquipmentContractIdentifier = validateEquipmentContractIdentifier;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-add.controller:EquipmentContractAddController
         * @type {EquipmentContract}
         * @name equipmentContract
         *
         * @description
         * EquipmentContract that is being created.
         */
        vm.equipmentContract = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-add.controller:EquipmentContractAddController
         * @type {Array}
         * @name facilities
         *
         * @description
         * The list of facilities available in the system.
         */
        vm.facilities = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-add.controller:EquipmentContractAddController
         * @type {Object}
         * @name facilitiesMap
         *
         * @description
         * The map of facilities by ids.
         */
        vm.facilitiesMap = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-add.controller:EquipmentContractAddController
         * @type {Array}
         * @name vendors
         *
         * @description
         * The list of vendors available in the system.
         */
        vm.vendors = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-add.controller:EquipmentContractAddController
         * @type {Array}
         * @name equipments
         *
         * @description
         * The list of equipments available in the system.
         */
        vm.equipments = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-add.controller:EquipmentContractAddController
         * @type {Array}
         * @name serviceTypes
         *
         * @description
         * The list of serviceTypes available in the system.
         */
        vm.serviceTypes = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentContractAddController.
         */
        function onInit() {
            vm.facilitiesMap = facilitiesMap;
            vm.equipmentContract = equipmentContract;
            vm.facilities = facilities;
            vm.vendors = vendors;
            vm.equipments = equipments;
            vm.serviceTypes = serviceTypes;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name toFacilityIdName
         *
         * @description
         * Gets facility's name using its id.
         *
         * @return {String} the name of the facility.
         */
        function toFacilityIdName(facilityId) {
            var facility = vm.facilitiesMap[facilityId];
            return facility.code + ' : ' + facility.name;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name addFacility
         *
         * @description
         * Adds valid equipmentContract assignment.
         *
         * @return {Promise} the promise resolving to the added assignment.
         */
        function addFacility() {
            return vm.equipmentContract.addFacility(vm.selectedFacility.id)
                .then(function() {
                    vm.selectedFacility = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name removeFacility
         *
         * @description
         * Remove equipmentContract associated facility.
         *
         * @return {Promise} the promise resolving to the removed facility.
         */
        function removeFacility(facilityId) {
            return vm.equipmentContract.removeFacility(facilityId)
                .then(function() {
                    vm.selectedFacility = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name addEquipment
         *
         * @description
         * Adds valid equipmentContract assignment.
         *
         * @return {Promise} the promise resolving to the added assignment.
         */
        function addEquipment() {
            return vm.equipmentContract.addEquipment(vm.selectedEquipment)
                .then(function() {
                    vm.selectedEquipment = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name removeEquipment
         *
         * @description
         * Remove equipmentContract associated equipment.
         *
         * @return {Promise} the promise resolving to the removed equipment.
         */
        function removeEquipment(equipment) {
            return vm.equipmentContract.removeEquipment(equipment)
                .then(function() {
                    vm.selectedEquipment = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name addServiceType
         *
         * @description
         * Adds valid equipmentContract assignment.
         *
         * @return {Promise} the promise resolving to the added assignment.
         */
        function addServiceType() {
            return vm.equipmentContract.addServiceType(vm.selectedServiceType)
                .then(function() {
                    vm.selectedServiceType = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name removeServiceType
         *
         * @description
         * Remove equipmentContract associated serviceType.
         *
         * @return {Promise} the promise resolving to the removed serviceType.
         */
        function removeServiceType(serviceType) {
            return vm.equipmentContract.removeServiceType(serviceType)
                .then(function() {
                    vm.selectedServiceType = undefined;
                });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name validateEquipmentContractIdentifier
         *
         * @description
         * Validates the entered equipmentContract name.
         *
         * @return {String} the error message key, undefined if equipmentContract is valid
         */
        function validateEquipmentContractIdentifier() {
            if (isEquipmentContractIdentifierDuplicated()) {
                return 'equipmentContractAdd.equipmentContractIdentifierDuplicated';
            }
        }

        function isEquipmentContractIdentifierDuplicated() {
            if (!vm.equipmentContract || !vm.equipmentContract.identifier) {
                return false;
            }

            return equipmentContracts.filter(function(equipmentContract) {
                return (
                    (equipmentContract.identifier.toUpperCase() === vm.equipmentContract.identifier.toUpperCase())
                    && equipmentContract.id !== vm.equipmentContract.id
                );
            }).length;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-add.controller:EquipmentContractAddController
         * @name save
         *
         * @description
         * Saves the contract and takes user back to the previous state.
         */
        function save() {
            loadingModalService.open();
            return equipmentContractService.save(vm.equipmentContract)
                .then(function(contract) {
                    notificationService.success('equipmentContractAdd.equipmentContractSavedSuccessfully');
                    stateTrackerService.goToPreviousState();
                    return contract;
                })
                .catch(function() {
                    notificationService.error('equipmentContractAdd.equipmentContractFailedToSave');
                    loadingModalService.close();
                });
        }

    }
})();