/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular.module('equipment-contract-add').config(routes);

    routes.$inject = ['$stateProvider', 'EQUIPMENT_RIGHTS', 'ADMINISTRATION_RIGHTS'];

    function routes($stateProvider, EQUIPMENT_RIGHTS, ADMINISTRATION_RIGHTS) {
        $stateProvider.state('openlmis.administration.equipmentContractsAdd', {
            controller: 'EquipmentContractAddController',
            controllerAs: 'vm',
            label: 'equipmentContractAdd.add',
            templateUrl: 'equipment-contract-add/equipment-contract-add.html',
            url: '/contractsAdd',
            accessRights: [EQUIPMENT_RIGHTS.EQUIPMENT_MANAGE, ADMINISTRATION_RIGHTS.FACILITIES_MANAGE],
            resolve: {
                equipmentContract: function(EquipmentContract) {
                    return new EquipmentContract({});
                },
                facilities: function(facilityService) {
                    return facilityService.getAllMinimal();
                },
                facilitiesMap: function(facilities, ObjectMapper) {
                    return new ObjectMapper().map(facilities);
                },
                vendors: function(equipmentVendorService) {
                    return equipmentVendorService
                        .query()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                },
                equipments: function(equipmentService) {
                    return equipmentService
                        .query()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                },
                serviceTypes: function(equipmentServiceTypeService) {
                    return equipmentServiceTypeService
                        .query()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                },
                contracts: function(equipmentContractService) {
                    return equipmentContractService
                        .query()
                        .then(
                            function(response) {
                                return response.content;
                            }
                        );
                }
            }
        });
    }

})();