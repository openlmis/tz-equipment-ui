/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentContractAddController', function() {

    var vm, $controller, equipmentContract, facilities, facilitiesMap,
        vendors, equipments, serviceTypes, $rootScope,
        EquipmentVendorDataBuilder, EquipmentDataBuilder, EquipmentServiceTypeDataBuilder,
        FacilityDataBuilder, EquipmentContractDataBuilder, contracts, $q;

    beforeEach(function() {
        module('equipment-contract-add');

        inject(function($injector) {
            $q = $injector.get('$q');
            $controller = $injector.get('$controller');
            $rootScope = $injector.get('$rootScope');
            this.equipmentContractService = $injector.get('equipmentContractService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            FacilityDataBuilder = $injector.get('FacilityDataBuilder');
            EquipmentContractDataBuilder = $injector.get('EquipmentContractDataBuilder');
            EquipmentVendorDataBuilder = $injector.get('EquipmentVendorDataBuilder');
            EquipmentDataBuilder = $injector.get('EquipmentDataBuilder');
            EquipmentServiceTypeDataBuilder = $injector.get('EquipmentServiceTypeDataBuilder');
        });

        facilities = [
            new FacilityDataBuilder().build(),
            new FacilityDataBuilder().build()
        ];

        facilitiesMap = {};
        facilitiesMap[facilities[0].id] = facilities[0];
        facilitiesMap[facilities[1].id] = facilities[1];

        vendors = [
            new EquipmentVendorDataBuilder().build(),
            new EquipmentVendorDataBuilder().build()
        ];

        equipments = [
            new EquipmentDataBuilder().build(),
            new EquipmentDataBuilder().build()
        ];

        serviceTypes = [
            new EquipmentServiceTypeDataBuilder().build(),
            new EquipmentServiceTypeDataBuilder().build()
        ];

        contracts = [
            new EquipmentContractDataBuilder().build(),
            new EquipmentContractDataBuilder().build()
        ];

        equipmentContract = new EquipmentContractDataBuilder().build();

        vm = $controller('EquipmentContractAddController', {
            contracts: contracts,
            facilities: facilities,
            facilitiesMap: facilitiesMap,
            equipmentContract: equipmentContract,
            vendors: vendors,
            equipments: equipments,
            serviceTypes: serviceTypes
        });
    });

    describe('$onInit', function() {

        it('should init properly', function() {
            vm.$onInit();

            expect(vm.equipmentContract).toEqual(equipmentContract);
            expect(vm.facilities).toEqual(facilities);
            expect(vm.facilitiesMap).toEqual(facilitiesMap);
        });

    });

    describe('validateEquipmentContractIdentifier', function() {

        beforeEach(function() {
            vm.$onInit();
        });

        it('should return undefined if the equipmentContract identifier is empty', function() {
            vm.equipmentContract.identifier = undefined;

            expect(vm.validateEquipmentContractIdentifier()).toBeUndefined();
        });

        it('should return message key if equipmentContract identifier is duplicated', function() {
            vm.equipmentContract.identifier = contracts[0].identifier;

            expect(vm.validateEquipmentContractIdentifier())
                .toEqual('equipmentContractAdd.equipmentContractIdentifierDuplicated');
        });

        it('should return undefined if the equipmentContract is edited', function() {
            vm.equipmentContract.id = contracts[0].id;
            vm.equipmentContract.identifier = contracts[0].identifier;

            expect(vm.validateEquipmentContractIdentifier()).toBeUndefined();
        });

        it('should return undefined if equipmentContract identifier is not duplicated', function() {
            vm.equipmentContract.identifier = 'Some different equipmentContract identifier';

            expect(vm.validateEquipmentContractIdentifier()).toBeUndefined();
        });

    });

    describe('addFacility', function() {

        beforeEach(function() {
            vm.$onInit();
            spyOn(equipmentContract, 'addFacility');
        });

        it('should clear form after facility was added', function() {
            equipmentContract.addFacility.andReturn($q.resolve());

            vm.selectedFacility = vm.facilities[0];

            vm.addFacility();

            expect(vm.selectedFacility).toEqual(vm.facilities[0]);

            $rootScope.$apply();

            expect(vm.selectedFacility).toBeUndefined();
        });

        it('should not clear form if facility failed to be added', function() {
            equipmentContract.addFacility.andReturn($q.reject());

            vm.selectedFacility = vm.facilities[0];

            vm.addFacility();
            $rootScope.$apply();

            expect(vm.selectedFacility).toEqual(vm.facilities[0]);
        });

    });

});