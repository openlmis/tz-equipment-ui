/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-category.EquipmentCategory
     *
     * @description
     * Represents a single equipment category.
     */
    angular
        .module('equipment-category')
        .factory('EquipmentCategory', EquipmentCategory);

    EquipmentCategory.$inject = ['$q'];

    function EquipmentCategory($q) {

        EquipmentCategory.prototype.addDiscipline = addDiscipline;
        EquipmentCategory.prototype.removeDiscipline = removeDiscipline;
        EquipmentCategory.prototype.validateCategoryDisciplineExists = validateCategoryDisciplineExists;
        EquipmentCategory.prototype.isCategoryDisciplineDuplicated = isCategoryDisciplineDuplicated;
        EquipmentCategory.prototype.validateCategoryDisciplineDoesNotExist = validateCategoryDisciplineDoesNotExist;
        return EquipmentCategory;

        /**
         * @ngdoc method
         * @methodOf equipment-category.EquipmentCategory
         * @name EquipmentCategory
         *
         * @description
         * Creates a new instance of the Equipment category class.
         *
         * @param  {Object}  json the json representation of the equipment category
         */
        function EquipmentCategory(json) {
            if (!json) {
                json = {};
            }

            this.id = json.id;
            this.name = json.name;
            this.code = json.code;
            this.displayOrder = json.displayOrder;
            this.enabled = json.enabled;
            this.disciplines = json.disciplines ? json.disciplines : [];
        }

        /**
         * @ngdoc method
         * @methodOf equipment-category.EquipmentCategory
         * @name addDiscipline
         *
         * @description
         * Adds the given discipline to the list of equipmentCategory discipline. Also stores information if
         * the discipline has been previously removed and not saved. If an already existing
         * discipline is added this method will return a rejected promise.
         *
         * @param  {Object}  discipline the discipline to add
         * @return {Promise}            the promise resolved when discipline was successfully added
         */
        function addDiscipline(discipline) {
            var error = this.validateCategoryDisciplineDoesNotExist(discipline);
            if (error) {
                return $q.reject(error);
            }

            this.disciplines.push(discipline);

            return $q.resolve();
        }

        /**
         * @ngdoc method
         * @methodOf equipment-category.EquipmentCategory
         * @name removeDiscipline
         *
         * @description
         * Removes the given discipline from the list of equipmentCategory disciplines. Also stores information
         * if the discipline has been previously added and not saved.
         *
         * @param {Object} discipline the discipline to be removed
         */
        function removeDiscipline(discipline) {
            var error = this.validateCategoryDisciplineExists(discipline);
            if (error) {
                return $q.reject(error);
            }

            var index = this.disciplines.findIndex(function(existing) {
                return existing.id === discipline.id;
            });
            this.disciplines.splice(index, 1);
            return $q.resolve();
        }

        function validateCategoryDisciplineDoesNotExist(discipline) {
            if (this.isCategoryDisciplineDuplicated(discipline)) {
                return 'equipmentCategory.disciplineDuplicated';
            }
        }

        function validateCategoryDisciplineExists(discipline) {
            var index = this.disciplines.findIndex(function(existing) {
                return existing.id === discipline.id;
            });
            if (index === -1) {
                return 'equipmentCategory.disciplineNotAdded';
            }
        }

        function isCategoryDisciplineDuplicated(newDiscipline) {
            return this.disciplines.filter(function(discipline) {
                return discipline.id === newDiscipline.id;
            }).length;
        }

    }

})();
