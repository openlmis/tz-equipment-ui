/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-contract-list.controller:ContractListController
     *
     * @description
     * Controller for managing contracts list screen.
     */
    angular
        .module('equipment-contract-view-list')
        .controller('EquipmentContractViewListController', controller);

    controller.$inject = ['contracts'];

    function controller(contracts) {
        var vm = this;

        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-list.controller:ContractListController
         * @type {Array}
         * @name contracts
         *
         * @description
         * The list of all available contracts in the system.
         */
        vm.contracts = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-contract-list.controller:ContractListController
         * @name $onInit
         *
         * @description
         * Initialization method of the ContractListController.
         */
        function onInit() {
            vm.contracts = contracts;
        }

    }

})();