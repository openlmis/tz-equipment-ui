/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-inventory-item-request.controller:InventoryItemRequestController
     *
     * @description
     * Exposes inventory item request to the view.
     */
    angular
        .module('equipment-inventory-item-request')
        .controller('InventoryItemRequestController', InventoryItemRequestController);

    InventoryItemRequestController.$inject = [
        'inventoryItem', '$state', 'vendors', 'maintenanceReasons', 'loadingModalService',
        'inventoryItemService', 'notificationService', 'stateTrackerService'
    ];

    function InventoryItemRequestController(inventoryItem, $state, vendors,
                                            maintenanceReasons, loadingModalService,
                                            inventoryItemService, notificationService, stateTrackerService) {
        var vm = this;

        vm.$onInit = onInit;

        vm.sendRequest = sendRequest;

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-item-request.controller:InventoryItemRequestController
         * @name inventoryItemMaintenanceRequest
         * @type {Object}
         *
         * @description
         * Object holds all maintenance request  values.
         */
        vm.inventoryItemMaintenanceRequest = {};

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-item-request.controller:InventoryItemRequestController
         * @name vendors
         * @type {Array}
         *
         * @description
         * Array object providing the list of vendors for the equipment maintenance request.
         */
        vm.vendors = [];

        /**
         * @ngdoc property
         * @propertyOf equipment-inventory-item-request.controller:InventoryItemRequestController
         * @name maintenanceReasons
         * @type {Array}
         *
         * @description
         * Array object providing the list of reason for the equipment maintenance request.
         */
        vm.maintenanceReasons = [];

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item-request.controller:InventoryItemRequestController
         * @name $onInit
         *
         * @description
         * Initialization method of the InventoryItemRequestController.
         */
        function onInit() {
            vm.inventoryItem = inventoryItem;
            vm.vendors = vendors;
            vm.maintenanceReasons = maintenanceReasons;
            vm.inventoryItemMaintenanceRequest.initChecked = true;

            vm.minDate = new Date();
            vm.maxDate = new Date();
            vm.maxDate.setHours(23, 59, 59, 999);
            vm.minDate.setHours(23, 59, 59, 999);
        }

        /**
         * @ngdoc method
         * @methodOf equipment-inventory-item-request.controller:InventoryItemRequestController
         * @name sendRequest
         *
         * @param   {Object}    inventoryItemMaintenanceRequest  the inventory item maintenance request object
         */
        function sendRequest(inventoryItemMaintenanceRequest) {
            inventoryItemMaintenanceRequest.inventory
                = angular.copy(vm.inventoryItem);
            inventoryItemMaintenanceRequest.resolved = false;
            inventoryItemMaintenanceRequest.requestDate
                = inventoryItemMaintenanceRequest.breakDownDate;

            loadingModalService.open();
            return new inventoryItemService
                .saveMaintenanceRequest(inventoryItemMaintenanceRequest)
                .then(function(request) {
                    notificationService.success('inventoryItemMaintenanceRequest.sent');
                    stateTrackerService.goToPreviousState();
                    return request;
                })
                .catch(function() {
                    notificationService.error('inventoryItemMaintenanceRequest.failed');
                    loadingModalService.close();
                });
        }
    }

})();
