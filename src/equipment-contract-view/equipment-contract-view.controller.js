/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */
(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-contract-view.controller:EquipmentContractViewController
     *
     * @description
     * Exposes method for creating/updating equipmentContract to the modal view.
     */
    angular
        .module('equipment-contract-view')
        .controller('EquipmentContractViewController', controller);

    controller.$inject = ['equipmentContract', 'facilities', 'facilitiesMap'];

    function controller(equipmentContract, facilities, facilitiesMap) {
        var vm = this;

        vm.$onInit = onInit;
        vm.toFacilityIdName = toFacilityIdName;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-view.controller:EquipmentContractViewController
         * @type {EquipmentContract}
         * @name equipmentContract
         *
         * @description
         * EquipmentContract that is being created.
         */
        vm.equipmentContract = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-view.controller:EquipmentContractViewController
         * @type {Array}
         * @name facilities
         *
         * @description
         * The list of facilities available in the system.
         */
        vm.facilities = undefined;

        /**
         * @ngdoc property
         * @propertyOf equipment-contract-view.controller:EquipmentContractViewController
         * @type {Object}
         * @name facilitiesMap
         *
         * @description
         * The map of facilities by ids.
         */
        vm.facilitiesMap = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-contract-view.controller:EquipmentContractViewController
         * @name $onInit
         *
         * @description
         * Initialization method of the EquipmentContractViewController.
         */
        function onInit() {
            vm.facilitiesMap = facilitiesMap;
            vm.equipmentContract = equipmentContract;
            vm.facilities = facilities;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-contract-view.controller:EquipmentContractViewController
         * @name toFacilityIdName
         *
         * @description
         * Gets facility's name using its id.
         *
         * @return {String} the name of the facility.
         */
        function toFacilityIdName(facilityId) {
            var facility = vm.facilitiesMap[facilityId];
            return facility.name;
        }

    }
})();