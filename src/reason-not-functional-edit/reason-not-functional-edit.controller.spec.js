/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('ReasonNotFunctionalEditController', function() {

    beforeEach(function() {
        module('reason-not-functional-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.confirmService = $injector.get('confirmService');
            this.$q = $injector.get('$q');
            this.reasonNotFunctionalService = $injector.get('reasonNotFunctionalService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.ReasonNotFunctionalDataBuilder = $injector.get('ReasonNotFunctionalDataBuilder');
        });

        this.reasonNotFunctional = new this.ReasonNotFunctionalDataBuilder().build();
        this.confirmDeferred = this.$q.defer();
        this.saveDeferred = this.$q.defer();

        spyOn(this.confirmService, 'confirm').andReturn(this.confirmDeferred.promise);
        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.reasonNotFunctionalService, 'update').andReturn(this.saveDeferred.promise);
        spyOn(this.reasonNotFunctionalService, 'create').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('ReasonNotFunctionalEditController', {
            reasonNotFunctional: this.reasonNotFunctional
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose reasonNotFunctional', function() {
            expect(this.vm.reasonNotFunctional).toEqual(this.reasonNotFunctional);
        });

        it('should expose resolved fields', function() {
            expect(this.vm.editMode).toEqual(true);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.confirmDeferred.resolve();
            this.saveDeferred.resolve(this.reasonNotFunctional);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            beforeEach(function() {
                this.vm.editMode = true;
            });

            it('should prompt user to confirm save', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'reasonNotFunctionalEdit.save.question',
                    'reasonNotFunctionalEdit.save'
                );
            });

            it('should not save reason not functional if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.reasonNotFunctionalService.update).not.toHaveBeenCalled();
                expect(this.reasonNotFunctionalService.create).not.toHaveBeenCalled();
            });

            it('should save reason not functional and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.reasonNotFunctionalService.update).toHaveBeenCalledWith(this.vm.reasonNotFunctional);
                expect(this.reasonNotFunctionalService.create).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if reason not functional was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.reasonNotFunctional);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('reasonNotFunctionalEdit.save.success');
            });

            it('should show notification if energy type save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('reasonNotFunctionalEdit.save.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });

        describe('while creating', function() {

            beforeEach(function() {
                this.vm.editMode = false;
            });

            it('should prompt user to save energy type', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'reasonNotFunctionalEdit.create.question',
                    'reasonNotFunctionalEdit.create'
                );
            });

            it('should not save reason not functional if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.reasonNotFunctionalService.create).not.toHaveBeenCalled();
                expect(this.reasonNotFunctionalService.update).not.toHaveBeenCalled();
            });

            it('should save reason not functional and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.reasonNotFunctionalService.create).toHaveBeenCalledWith(this.vm.reasonNotFunctional);
                expect(this.reasonNotFunctionalService.update).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if reason not functional was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.reasonNotFunctional);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('reasonNotFunctionalEdit.create.success');
            });

            it('should show notification if energy type save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('reasonNotFunctionalEdit.create.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });

    describe('goToPreviousState', function() {

        it('should redirect to reason not functional list screen', function() {
            this.vm.goToPreviousState();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });
    });
});
