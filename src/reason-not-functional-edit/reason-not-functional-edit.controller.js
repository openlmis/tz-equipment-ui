/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name reason-not-functional-edit.controller:ReasonNotFunctionalEditController
     *
     * @description
     * Provides methods for edit reason not functional modal. Allows returning to previous state and editing reason
     * not functional.
     */
    angular
        .module('reason-not-functional-edit')
        .controller('ReasonNotFunctionalEditController', ReasonNotFunctionalEditController);

    ReasonNotFunctionalEditController.$inject = [
        'reasonNotFunctional', 'confirmService', 'reasonNotFunctionalService', 'stateTrackerService',
        '$state', 'loadingModalService', 'notificationService'
    ];

    function ReasonNotFunctionalEditController(reasonNotFunctional, confirmService, reasonNotFunctionalService,
                                               stateTrackerService, $state, loadingModalService, notificationService) {
        var vm = this;

        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @methodOf reason-not-functional-edit.controller:ReasonNotFunctionalEditController
         * @name reasonNotFunctional
         * @type {Object}
         *
         * @description
         * Current reason not functional.
         */
        vm.reasonNotFunctional = undefined;

        /**
         * @ngdoc property
         * @methodOf reason-not-functional-edit.controller:ReasonNotFunctionalEditController
         * @name editMode
         * @type {boolean}
         *
         * @description
         * Indicates if reason not functional is already created.
         */
        vm.editMode = undefined;

        /**
         * @ngdoc method
         * @methodOf reason-not-functional-edit.controller:ReasonNotFunctionalEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating ReasonNotFunctionalEditController.
         */
        function onInit() {
            vm.reasonNotFunctional = reasonNotFunctional ? reasonNotFunctional : {};
            vm.editMode = !!reasonNotFunctional;
        }

        /**
         * @ngdoc method
         * @methodOf reason-not-functional-edit.controller:ReasonNotFunctionalEditController
         * @name save
         *
         * @description
         * Saves the reason not functional after confirm.
         */
        function save() {
            confirmService.confirm(
                vm.editMode ? 'reasonNotFunctionalEdit.save.question' : 'reasonNotFunctionalEdit.create.question',
                vm.editMode ? 'reasonNotFunctionalEdit.save' : 'reasonNotFunctionalEdit.create'
            ).then(function() {
                loadingModalService.open();
                getSavePromise()
                    .then(function() {
                        notificationService.success(vm.editMode ?
                            'reasonNotFunctionalEdit.save.success' : 'reasonNotFunctionalEdit.create.success');
                        stateTrackerService.goToPreviousState();
                    })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error(
                            vm.editMode
                                ? 'reasonNotFunctionalEdit.save.failure'
                                : 'reasonNotFunctionalEdit.create.failure'
                        );
                    });
            });
        }

        function getSavePromise() {
            return vm.editMode ?
                reasonNotFunctionalService.update(vm.reasonNotFunctional) :
                reasonNotFunctionalService.create(vm.reasonNotFunctional);
        }
    }
})();
