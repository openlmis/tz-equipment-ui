/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-fund-source-edit.controller:EquipmentFundSourceEditController
     *
     * @description
     * Provides methods for Edit equipment fund source modal. Allows returning to previous state and editing equipment
     * fund source.
     */
    angular
        .module('equipment-fund-source-edit')
        .controller('EquipmentFundSourceEditController', EquipmentFundSourceEditController);

    EquipmentFundSourceEditController.$inject = [
        'equipmentFundSource', 'confirmService', 'equipmentFundSourceService', 'stateTrackerService',
        '$state', 'loadingModalService', 'notificationService'
    ];

    function EquipmentFundSourceEditController(equipmentFundSource, confirmService, equipmentFundSourceService,
                                               stateTrackerService, $state, loadingModalService, notificationService) {
        var vm = this;

        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @methodOf equipment-fund-source-edit.controller:EquipmentFundSourceEditController
         * @name equipmentFundSource
         * @type {Object}
         *
         * @description
         * Current equipment fund source.
         */
        vm.equipmentFundSource = undefined;

        /**
         * @ngdoc property
         * @methodOf equipment-fund-source-edit.controller:EquipmentFundSourceEditController
         * @name editMode
         * @type {boolean}
         *
         * @description
         * Indicates if equipment fund source is already created.
         */
        vm.editMode = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-fund-source-edit.controller:EquipmentFundSourceEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentFundSourceEditController.
         */
        function onInit() {
            vm.equipmentFundSource = equipmentFundSource ? equipmentFundSource : {};
            vm.editMode = !!equipmentFundSource;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-fund-source-edit.controller:EquipmentFundSourceEditController
         * @name save
         *
         * @description
         * Saves the equipment fund source after confirm.
         */
        function save() {
            confirmService.confirm(
                vm.editMode ? 'equipmentFundSourceEdit.save.question' : 'equipmentFundSourceEdit.create.question',
                vm.editMode ? 'equipmentFundSourceEdit.save' : 'equipmentFundSourceEdit.create'
            ).then(function() {
                loadingModalService.open();
                getSavePromise()
                    .then(function() {
                        notificationService.success(vm.editMode ?
                            'equipmentFundSourceEdit.save.success' : 'equipmentFundSourceEdit.create.success');
                        stateTrackerService.goToPreviousState();
                    })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error(
                            vm.editMode
                                ? 'equipmentFundSourceEdit.save.failure'
                                : 'equipmentFundSourceEdit.create.failure'
                        );
                    });
            });
        }

        function getSavePromise() {
            return vm.editMode ?
                equipmentFundSourceService.update(vm.equipmentFundSource) :
                equipmentFundSourceService.create(vm.equipmentFundSource);
        }
    }
})();
