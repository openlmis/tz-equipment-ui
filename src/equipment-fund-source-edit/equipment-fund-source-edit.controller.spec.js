/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentFundSourceEditController', function() {

    beforeEach(function() {
        module('equipment-fund-source-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.confirmService = $injector.get('confirmService');
            this.$q = $injector.get('$q');
            this.equipmentFundSourceService = $injector.get('equipmentFundSourceService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentFundSourceDataBuilder = $injector.get('EquipmentFundSourceDataBuilder');
        });

        this.equipmentFundSource = new this.EquipmentFundSourceDataBuilder().build();
        this.confirmDeferred = this.$q.defer();
        this.saveDeferred = this.$q.defer();

        spyOn(this.confirmService, 'confirm').andReturn(this.confirmDeferred.promise);
        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.equipmentFundSourceService, 'update').andReturn(this.saveDeferred.promise);
        spyOn(this.equipmentFundSourceService, 'create').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('EquipmentFundSourceEditController', {
            equipmentFundSource: this.equipmentFundSource
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose equipmentFundSource', function() {
            expect(this.vm.equipmentFundSource).toEqual(this.equipmentFundSource);
        });

        it('should expose resolved fields', function() {
            expect(this.vm.editMode).toEqual(true);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.confirmDeferred.resolve();
            this.saveDeferred.resolve(this.equipmentFundSource);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            beforeEach(function() {
                this.vm.editMode = true;
            });

            it('should prompt user to confirm save', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentFundSourceEdit.save.question',
                    'equipmentFundSourceEdit.save'
                );
            });

            it('should not save equipment fund source if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentFundSourceService.update).not.toHaveBeenCalled();
                expect(this.equipmentFundSourceService.create).not.toHaveBeenCalled();
            });

            it('should save equipment fund source and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentFundSourceService.update).toHaveBeenCalledWith(this.vm.equipmentFundSource);
                expect(this.equipmentFundSourceService.create).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment fund source was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentFundSource);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentFundSourceEdit.save.success');
            });

            it('should show notification if fund source save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentFundSourceEdit.save.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });

        describe('while creating', function() {

            beforeEach(function() {
                this.vm.editMode = false;
            });

            it('should prompt user to save fund source', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentFundSourceEdit.create.question',
                    'equipmentFundSourceEdit.create'
                );
            });

            it('should not save equipment fund source if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentFundSourceService.create).not.toHaveBeenCalled();
                expect(this.equipmentFundSourceService.update).not.toHaveBeenCalled();
            });

            it('should save equipment fund source and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentFundSourceService.create).toHaveBeenCalledWith(this.vm.equipmentFundSource);
                expect(this.equipmentFundSourceService.update).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment fund source was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentFundSource);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentFundSourceEdit.create.success');
            });

            it('should show notification if fund source save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentFundSourceEdit.create.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });

    describe('goToPreviousState', function() {

        it('should redirect to equipment fund source list screen', function() {
            this.vm.goToPreviousState();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });
    });
});
