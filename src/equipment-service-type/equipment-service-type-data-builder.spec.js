/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-service-type')
        .factory('EquipmentServiceTypeDataBuilder', EquipmentServiceTypeDataBuilder);

    EquipmentServiceTypeDataBuilder.$inject = ['EquipmentServiceType'];

    function EquipmentServiceTypeDataBuilder(EquipmentServiceType) {

        EquipmentServiceTypeDataBuilder.prototype.build = build;
        EquipmentServiceTypeDataBuilder.prototype.withId = withId;
        EquipmentServiceTypeDataBuilder.prototype.withName = withName;
        EquipmentServiceTypeDataBuilder.prototype.withDescription = withDescription;

        return EquipmentServiceTypeDataBuilder;

        function EquipmentServiceTypeDataBuilder() {
            EquipmentServiceTypeDataBuilder.instanceNumber = (EquipmentServiceTypeDataBuilder.instanceNumber || 0) + 1;

            this.id = 'equipment-service-type-id-' + EquipmentServiceTypeDataBuilder.instanceNumber;
            this.description = '123';
            this.name = 'Test Service Type';
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withDescription(description) {
            this.description = description;
            return this;
        }

        function build() {
            return new EquipmentServiceType(
                this.id,
                this.description,
                this.name
            );
        }

    }

})();