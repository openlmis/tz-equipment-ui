/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-service-type.EquipmentServiceType
     *
     * @description
     * Represents a single equipment service type.
     */
    angular
        .module('equipment-service-type')
        .factory('EquipmentServiceType', EquipmentServiceType);

    function EquipmentServiceType() {

        return EquipmentServiceType;

        /**
         * @ngdoc method
         * @methodOf equipment-service-type.EquipmentServiceType
         * @name EquipmentServiceType
         *
         * @description
         * Creates a new instance of the EquipmentServiceType class.
         *
         * @param  {String}  id             the UUID of the equipment service type to be created
         * @param  {String}  description           the description of the equipment service type to be created
         * @param  {String}  name           the name of the equipment service type to be created
         */
        function EquipmentServiceType(id, description, name) {
            this.id = id;
            this.description = description;
            this.name = name;
        }

    }

})();
