/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-service-type.EquipmentServiceTypeService
     *
     * @description
     * Responsible for retrieving all equipment service type information from server.
     */
    angular
        .module('equipment-service-type')
        .service('equipmentServiceTypeService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/serviceTypes/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/serviceTypes'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/serviceTypes/:id'),
                method: 'PUT'
            },
            delete: {
                url: referencedataUrlFactory('/api/serviceTypes/:id'),
                method: 'DELETE'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;
        this.deleteServiceType = deleteServiceType;

        /**
             * @ngdoc method
             * @methodOf equipment-service-type.EquipmentServiceTypeService
             * @name get
             *
             * @description
             * Retrieves equipment service type by id.
             *
             * @param  {String}  equipmentServiceTypeId equipmentServiceType UUID
             * @return {Promise}                equipmentServiceType promise
             */
        function get(equipmentServiceTypeId) {
            return resource.get({
                id: equipmentServiceTypeId
            }).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-service-type.EquipmentServiceTypeService
             * @name getAll
             *
             * @description
             * Retrieves all equipment service types by ids.
             *
             * @param  {Object}  queryParams the search parameters
             * @return {Promise} Page of equipment service types
             */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-service-type.EquipmentServiceTypeService
             * @name update
             *
             * @description
             * Updates existing equipment service type.
             *
             * @param  {Object}  equipmentServiceType equipment service type that will be saved
             * @return {Promise}              updated equipment service type
             */
        function update(equipmentServiceType) {
            return resource.update({
                id: equipmentServiceType.id
            }, equipmentServiceType).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-service-type.EquipmentServiceTypeService
         * @name deleteServiceType
         *
         * @description
         * Deletes existing equipment service type.
         *
         * @param  {Object}  equipmentServiceType equipment service type that will be deleted
         * @return {Promise}              deleted equipment service type
         */
        function deleteServiceType(equipmentServiceType) {
            return resource.delete({
                id: equipmentServiceType.id
            }, equipmentServiceType).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-service-type.EquipmentServiceTypeService
             * @name create
             *
             * @description
             * Creates new equipment service type.
             *
             * @param  {Object}  equipmentServiceType equipment service type that will be created
             * @return {Promise}              created equipment service type
             */
        function create(equipmentServiceType) {
            return resource.save(equipmentServiceType).$promise;
        }
    }
})();
