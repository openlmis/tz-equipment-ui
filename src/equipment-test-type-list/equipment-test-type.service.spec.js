/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('equipmentTestTypeService', function() {

    var $rootScope, $httpBackend;
    var equipmentUrlFactory, equipmentTestTypeService, EquipmentTestTypeDataBuilder;
    var equipmentTestTypes, equipmentTestType, EQUIPMENT_TEST_TYPE_ID;

    beforeEach(function() {
        module('equipment-test-type-list');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            equipmentUrlFactory = $injector.get('equipmentUrlFactory');
            equipmentTestTypeService = $injector.get('equipmentTestTypeService');
            EquipmentTestTypeDataBuilder = $injector.get('EquipmentTestTypeDataBuilder');
        });

        prepareTestData();
    });

    describe('get', function() {

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentTestTypes/' + equipmentTestTypes[0].id))
                .respond(200, equipmentTestTypes[0]);
        });

        it('should return promise', function() {
            var result = equipmentTestTypeService.get(equipmentTestTypes[0].id);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentTestType', function() {
            var result;

            equipmentTestTypeService.get(equipmentTestTypes[0].id).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentTestTypes[0]);

            expect(angular.toJson(result)).toEqual(angular.toJson(expected));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentTestTypes/' + equipmentTestTypes[0].id));

            equipmentTestTypeService.get(equipmentTestTypes[0].id);
            $httpBackend.flush();
        });
    });

    describe('getAll', function() {

        var parameters = {
            page: 0,
            size: 10
        };

        beforeEach(function() {
            $httpBackend.when('GET', equipmentUrlFactory('/api/equipmentTestTypes?page=' + parameters.page +
                '&size=' + parameters.size)).respond(200, {
                content: equipmentTestTypes
            });
        });

        it('should return promise', function() {
            var result = equipmentTestTypeService.getAll(parameters);
            $httpBackend.flush();

            expect(result.then).not.toBeUndefined();
        });

        it('should resolve to equipmentTestTypes', function() {
            var result;

            equipmentTestTypeService.getAll(parameters).then(function(data) {
                result = data;
            });
            $httpBackend.flush();
            $rootScope.$apply();

            var expected = angular.copy(equipmentTestTypes);

            expect(angular.toJson(result)).toEqual(angular.toJson({
                content: expected
            }));
        });

        it('should make a proper request', function() {
            $httpBackend.expectGET(equipmentUrlFactory('/api/equipmentTestTypes?page=' + parameters.page +
                '&size=' + parameters.size));

            equipmentTestTypeService.getAll(parameters);
            $httpBackend.flush();
        });
    });

    describe('save', function() {

        var result;

        beforeEach(function() {
            result = undefined;
        });

        it('should create equipmentTestTypes if ID is not given', function() {
            equipmentTestType = new EquipmentTestTypeDataBuilder().withoutId()
                .build();

            var returned = angular.copy(equipmentTestType);

            returned.id = EQUIPMENT_TEST_TYPE_ID;

            $httpBackend.expect(
                'POST', equipmentUrlFactory('/api/equipmentTestTypes'), equipmentTestType
            ).respond(200, returned);

            equipmentTestTypeService.save(equipmentTestType).then(function(equipmentTestType) {
                result = equipmentTestType;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(returned));
        });

        it('should update equipmentTestType if it has ID', function() {
            equipmentTestType = new EquipmentTestTypeDataBuilder().withId(EQUIPMENT_TEST_TYPE_ID)
                .build();

            $httpBackend.expect(
                'PUT', equipmentUrlFactory('/api/equipmentTestTypes/' + EQUIPMENT_TEST_TYPE_ID), equipmentTestType
            ).respond(200, equipmentTestType);

            equipmentTestTypeService.save(equipmentTestType).then(function(equipmentTestType) {
                result = equipmentTestType;
            });

            $httpBackend.flush();
            $rootScope.$apply();

            expect(angular.toJson(result)).toEqual(angular.toJson(equipmentTestType));
        });

    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    function prepareTestData() {
        EQUIPMENT_TEST_TYPE_ID = 'some-equipment-test-type-id';

        equipmentTestTypes = [
            new EquipmentTestTypeDataBuilder().withId('1')
                .build(),
            new EquipmentTestTypeDataBuilder().withId('2')
                .build()
        ];
    }
});
