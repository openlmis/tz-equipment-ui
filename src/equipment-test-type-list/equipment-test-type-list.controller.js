/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-test-type-list.controller:EquipmentTestTypeListController
     *
     * @description
     * Controller for lab equipment test type list.
     */
    angular
        .module('equipment-test-type-list')
        .controller('EquipmentTestTypeListController', EquipmentTestTypeListController);

    EquipmentTestTypeListController.$inject = [
        '$state',  '$stateParams', 'EQUIPMENT_RIGHTS', 'messageService', 'equipmentTestTypes'
    ];

    function EquipmentTestTypeListController($state, $stateParams, EQUIPMENT_RIGHTS, messageService,
                                             equipmentTestTypes) {
        var vm = this;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @propertyOf equipment-test-type-list.controller:EquipmentTestTypeListController
         * @name equipmentTestTypes
         * @type {Array}
         *
         * @description
         * Contains page of Equipment Test Types.
         */
        vm.equipmentTestTypes = undefined;

        vm.goToAddEquipmentTestTypePage = goToAddEquipmentTestTypePage;

        /**
         * @ngdoc method
         * @methodOf equipment-test-type-list.controller:EquipmentTestTypeListController
         * @name onInit
         *
         * @description
         * Init method for EquipmentTestTypeListController.
         */
        function onInit() {
            vm.equipmentTestTypes = equipmentTestTypes;
        }

        function goToAddEquipmentTestTypePage() {
            $state.go('openlmis.administration.equipmentTestType.add');
        }

    }

})();
