/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipmentTestTypeService
     *
     * @description
     * Responsible for retrieving equipment test types from the server.
     */
    angular
        .module('equipment-test-type-list')
        .service('equipmentTestTypeService', service);

    service.$inject = ['equipmentUrlFactory', '$resource'];

    function service(equipmentUrlFactory, $resource) {

        var resource = $resource(equipmentUrlFactory('/api/equipmentTestTypes/:id'), {}, {
            getAll: {
                url: equipmentUrlFactory('/api/equipmentTestTypes'),
                method: 'GET'
            },
            query: {
                url: equipmentUrlFactory('/api/equipmentTestTypes'),
                method: 'GET',
                isArray: false
            },
            get: {
                url: equipmentUrlFactory('/api/equipmentTestTypes/:id'),
                method: 'GET'
            },
            update: {
                method: 'PUT'
            }
        });
        this.getAll = getAll;
        this.get = get;
        this.query = query;
        this.save = save;

        function getAll(paginationParams) {
            return resource.getAll(paginationParams).$promise;
        }

        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        function get(id) {
            return resource.get({
                id: id
            }).$promise;
        }

        function save(type) {
            if (type.id) {
                return resource.update({
                    id: type.id
                }, type).$promise;
            }
            return resource.save({}, type).$promise;
        }

    }
})();
