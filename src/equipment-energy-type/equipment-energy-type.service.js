/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-energy-type.EquipmentEnergyTypeService
     *
     * @description
     * Responsible for retrieving all equipment energy type information from server.
     */
    angular
        .module('equipment-energy-type')
        .service('equipmentEnergyTypeService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/equipmentEnergyTypes/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/equipmentEnergyTypes'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/equipmentEnergyTypes/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;

        /**
             * @ngdoc method
             * @methodOf equipment-energy-type.EquipmentEnergyTypeService
             * @name get
             *
             * @description
             * Retrieves equipment energy type by id.
             *
             * @param  {String}  equipmentEnergyTypeId equipmentEnergyType UUID
             * @return {Promise}                equipmentEnergyType promise
             */
        function get(equipmentEnergyTypeId) {
            return resource.get({
                id: equipmentEnergyTypeId
            }).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-energy-type.EquipmentEnergyTypeService
             * @name getAll
             *
             * @description
             * Retrieves all equipment energy types by ids.
             *
             * @param  {Object}  queryParams the search parameters
             * @return {Promise} Page of equipment energy types
             */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-energy-type.EquipmentEnergyTypeService
             * @name update
             *
             * @description
             * Updates existing equipment energy type.
             *
             * @param  {Object}  equipmentEnergyType equipment energy type that will be saved
             * @return {Promise}              updated equipment energy type
             */
        function update(equipmentEnergyType) {
            return resource.update({
                id: equipmentEnergyType.id
            }, equipmentEnergyType).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-energy-type.EquipmentEnergyTypeService
             * @name create
             *
             * @description
             * Creates new equipment energy type.
             *
             * @param  {Object}  equipmentEnergyType equipment energy type that will be created
             * @return {Promise}              created equipment energy type
             */
        function create(equipmentEnergyType) {
            return resource.save(equipmentEnergyType).$promise;
        }
    }
})();
