/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-energy-type')
        .factory('EquipmentEnergyTypeDataBuilder', EquipmentEnergyTypeDataBuilder);

    EquipmentEnergyTypeDataBuilder.$inject = ['EquipmentEnergyType'];

    function EquipmentEnergyTypeDataBuilder(EquipmentEnergyType) {

        EquipmentEnergyTypeDataBuilder.prototype.build = build;
        EquipmentEnergyTypeDataBuilder.prototype.withId = withId;
        EquipmentEnergyTypeDataBuilder.prototype.withName = withName;
        EquipmentEnergyTypeDataBuilder.prototype.withCode = withCode;

        return EquipmentEnergyTypeDataBuilder;

        function EquipmentEnergyTypeDataBuilder() {
            EquipmentEnergyTypeDataBuilder.instanceNumber = (EquipmentEnergyTypeDataBuilder.instanceNumber || 0) + 1;

            this.id = 'equipment-energy-type-id-' + EquipmentEnergyTypeDataBuilder.instanceNumber;
            this.code = '123';
            this.name = 'Test Energy Type';
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function build() {
            return new EquipmentEnergyType(
                this.id,
                this.code,
                this.name
            );
        }

    }

})();