/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('FACILITY_EQUIPMENT_STATUS', function() {

    var FACILITY_EQUIPMENT_STATUS;

    beforeEach(function() {
        module('facility-equipment-status');

        inject(function($injector) {
            FACILITY_EQUIPMENT_STATUS = $injector.get('FACILITY_EQUIPMENT_STATUS');
        });
    });

    describe('getLabel', function() {

        it('should return label for valid status', function() {
            expect(
                FACILITY_EQUIPMENT_STATUS.getLabel('ALL_FUNCTIONING')
            ).toEqual('facilityEquipmentStatus.allFunctioning');

            expect(
                FACILITY_EQUIPMENT_STATUS.getLabel('NOT_FULLY_FUNCTIONING')
            ).toEqual('facilityEquipmentStatus.notFullyFunctioning');

            expect(
                FACILITY_EQUIPMENT_STATUS.getLabel('NOT_FUNCTIONING')
            ).toEqual('facilityEquipmentStatus.notFunctioning');

            expect(
                FACILITY_EQUIPMENT_STATUS.getLabel('UNKNOWN')
            ).toEqual('facilityEquipmentStatus.unknown');

            expect(
                FACILITY_EQUIPMENT_STATUS.getLabel('LOADING')
            ).toEqual('facilityEquipmentStatus.loading');

            expect(
                FACILITY_EQUIPMENT_STATUS.getLabel('NO_EQUIPMENT')
            ).toEqual('facilityEquipmentStatus.noEquipment');
        });

        it('should throw exception for invalid status', function() {
            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getLabel('NON_EXISTENT_SOURCE');
            }).toThrow('Invalid status');

            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getLabel(undefined);
            }).toThrow('Invalid status');

            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getLabel(null);
            }).toThrow('Invalid status');

            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getLabel('');
            }).toThrow('Invalid status');
        });

    });

    describe('getClass', function() {

        it('should return css class for valid status', function() {
            expect(
                FACILITY_EQUIPMENT_STATUS.getClass('ALL_FUNCTIONING')
            ).toEqual('is-functioning');

            expect(
                FACILITY_EQUIPMENT_STATUS.getClass('NOT_FULLY_FUNCTIONING')
            ).toEqual('is-not-fully-functioning');

            expect(
                FACILITY_EQUIPMENT_STATUS.getClass('NOT_FUNCTIONING')
            ).toEqual('is-non-functioning');

            expect(
                FACILITY_EQUIPMENT_STATUS.getClass('UNKNOWN')
            ).toEqual('is-unknown');

            expect(
                FACILITY_EQUIPMENT_STATUS.getClass('LOADING')
            ).toEqual('is-loading-status');

            expect(
                FACILITY_EQUIPMENT_STATUS.getClass('NO_EQUIPMENT')
            ).toEqual('no-equipment');
        });

        it('should throw exception for invalid status', function() {
            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getClass('NON_EXISTENT_SOURCE');
            }).toThrow('Invalid status');

            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getClass(undefined);
            }).toThrow('Invalid status');

            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getClass(null);
            }).toThrow('Invalid status');

            expect(function() {
                FACILITY_EQUIPMENT_STATUS.getClass('');
            }).toThrow('Invalid status');
        });
    });

});
