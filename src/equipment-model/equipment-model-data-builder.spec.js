/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-model')
        .factory('EquipmentModelDataBuilder', EquipmentModelDataBuilder);

    EquipmentModelDataBuilder.$inject = ['EquipmentModel'];

    function EquipmentModelDataBuilder(EquipmentModel) {

        EquipmentModelDataBuilder.prototype.build = build;
        EquipmentModelDataBuilder.prototype.withId = withId;
        EquipmentModelDataBuilder.prototype.withName = withName;
        EquipmentModelDataBuilder.prototype.withCode = withCode;
        EquipmentModelDataBuilder.prototype.withEquipmentType = withEquipmentType;

        return EquipmentModelDataBuilder;

        function EquipmentModelDataBuilder() {
            EquipmentModelDataBuilder.instanceNumber = (EquipmentModelDataBuilder.instanceNumber || 0) + 1;

            this.id = 'equipment-model-id-' + EquipmentModelDataBuilder.instanceNumber;
            this.code = '123';
            this.name = 'Test Model';
            this.equipmentType = {
                id: 'equipment-type-id-123',
                name: 'Test Type',
                code: '1234'
            };
        }

        function withId(id) {
            this.id = id;
            return this;
        }

        function withName(name) {
            this.name = name;
            return this;
        }

        function withCode(code) {
            this.code = code;
            return this;
        }

        function withEquipmentType(equipmentType) {
            this.equipmentType = equipmentType;
            return this;
        }

        function build() {
            return new EquipmentModel(
                this.id,
                this.code,
                this.name,
                this.equipmentType
            );
        }

    }

})();