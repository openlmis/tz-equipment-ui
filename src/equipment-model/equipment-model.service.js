/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name equipment-model.equipmentModelService
     *
     * @description
     * Responsible for retrieving all equipmentModel information from server.
     */
    angular
        .module('equipment-model')
        .service('equipmentModelService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/equipmentModels/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/equipmentModels'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/equipmentModels/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;

        /**
             * @ngdoc method
             * @methodOf equipment-model.equipmentModelService
             * @name get
             *
             * @description
             * Retrieves equipment model by id.
             *
             * @param  {String}  equipmentModelId equipmentModel UUID
             * @return {Promise}                equipmentModel promise
             */
        function get(equipmentModelId) {
            return resource.get({
                id: equipmentModelId
            }).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-model.equipmentModelService
             * @name getAll
             *
             * @description
             * Retrieves all equipment models by ids.
             *
             * @param  {Object}  queryParams the search parameters
             * @return {Promise} Page of equipment models
             */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-model.equipmentModelService
             * @name update
             *
             * @description
             * Updates eixisting Equipment model.
             *
             * @param  {Object}  equipmentModel equipment model that will be saved
             * @return {Promise}              updated equipment model
             */
        function update(equipmentModel) {
            return resource.update({
                id: equipmentModel.id
            }, equipmentModel).$promise;
        }

        /**
             * @ngdoc method
             * @methodOf equipment-model.equipmentModelService
             * @name create
             *
             * @description
             * Creates new Equipment model.
             *
             * @param  {Object}  equipmentModel equipment model that will be created
             * @return {Promise}              created equipment model
             */
        function create(equipmentModel) {
            return resource.save(equipmentModel).$promise;
        }
    }
})();
