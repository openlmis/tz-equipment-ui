/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('equipment-orderable')
        .factory('EquipmentOrderableDataBuilder', EquipmentOrderableDataBuilder);

    EquipmentOrderableDataBuilder.$inject = ['EquipmentOrderable'];

    function EquipmentOrderableDataBuilder(EquipmentOrderable) {

        EquipmentOrderableDataBuilder.prototype.build = build;
        EquipmentOrderableDataBuilder.prototype.buildJson = buildJson;
        EquipmentOrderableDataBuilder.prototype.withId = withId;
        EquipmentOrderableDataBuilder.prototype.withOrderableId = withOrderableId;
        EquipmentOrderableDataBuilder.prototype.withEquipmentId = withEquipmentId;
        EquipmentOrderableDataBuilder.prototype.withoutId = withoutId;

        return EquipmentOrderableDataBuilder;

        function EquipmentOrderableDataBuilder() {
            this.id = '35b8eeca-bfad-47f3-b966-c9cb726b872f';
            this.equipmentId = {
                id: 'equipment-type-id-123',
                name: 'Equipment Type'
            };
            this.orderableId = {
                id: 'orderable-id-123',
                name: 'Orderable'
            };

        }

        function build() {
            return new EquipmentOrderable(this.buildJson());
        }

        function withId(newId) {
            this.id = newId;
            return this;
        }

        function withoutId() {
            this.id = undefined;
            return this;
        }

        function withEquipmentId(equipmentId) {
            this.equipmentId = equipmentId;
            return this;
        }

        function withOrderableId(orderableId) {
            this.orderableId = orderableId;
            return this;
        }

        function buildJson() {
            return {
                id: this.id,
                equipmentId: this.equipmentId,
                orderableId: this.orderableId
            };
        }
    }

})();
