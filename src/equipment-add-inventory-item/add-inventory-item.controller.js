/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-add-inventory-item.controller:AddInventoryItemController
     *
     * @description
     * Manages Add Inventory Item modal.
     */
    angular
        .module('equipment-add-inventory-item')
        .controller('AddInventoryItemController', controller);

    controller.$inject = ['$state', 'equipments', '$scope', 'confirmService'];

    function controller($state, equipments, $scope, confirmService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.addInventoryItem = addInventoryItem;
        vm.goToInventoryList = goToInventoryList;

        /**
         * @ngdoc property
         * @propertyOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @type {Array}
         * @name equipments
         *
         * @description
         * The list of available equipments;
         */
        vm.equipments = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @name $onInit
         *
         * @description
         * Initialization method of the AddInventoryItemController.
         */
        function onInit() {
            vm.equipments = equipments;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @name addInventoryItem
         *
         * @description
         * Redirects the user to the edit modal.
         */
        function addInventoryItem() {
            $state.go('openlmis.equipment.inventory.edit', {
                inventoryItem: {
                    facilityId: vm.facility.id,
                    facility: vm.facility,
                    programId: vm.program.id,
                    program: vm.program,
                    equipment: vm.equipment
                }
            });
        }

        /**
         * @ngdoc method
         * @methodOf equipment-add-inventory-item.controller:AddInventoryItemController
         * @name goToInventoryList
         *
         * @description
         * Takes the user to the inventory item list screen. Will open a confirmation modal if user
         * interacted with the form.
         */
        function goToInventoryList() {
            if ($scope.addInventoryItemForm.$dirty) {
                confirmService.confirm(
                    'equipmentAddInventoryItem.closeAddInventoryItemModal',
                    'equipmentAddInventoryItem.yes',
                    'equipmentAddInventoryItem.no'
                ).then(function() {
                    $state.go('openlmis.equipment.inventory');
                });
            } else {
                $state.go('openlmis.equipment.inventory');
            }
        }

    }

})();
