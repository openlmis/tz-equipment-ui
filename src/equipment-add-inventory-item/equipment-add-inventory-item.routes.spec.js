/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('openlmis.inventoryItems.add state', function() {

    var $state, $q, openlmisModalService, equipmentService, EQUIPMENT_RIGHTS,
        state, dialogSpy, equipments, EquipmentDataBuilder, $rootScope;

    beforeEach(function() {
        module('openlmis-main-state');
        module('equipment');
        module('equipment-inventory-list');
        module('equipment-add-inventory-item');

        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $state = $injector.get('$state');
            $q = $injector.get('$q');
            openlmisModalService = $injector.get('openlmisModalService');
            EQUIPMENT_RIGHTS = $injector.get('EQUIPMENT_RIGHTS');
            equipmentService = $injector.get('equipmentService');
            EquipmentDataBuilder = $injector.get('EquipmentDataBuilder');
        });

        equipments = [
            new EquipmentDataBuilder()
                .build(),
            new EquipmentDataBuilder()
                .build(),
            new EquipmentDataBuilder()
                .build()
        ];

        spyOn(equipmentService, 'query').andReturn($q.when({
            content: equipments,
            last: true,
            totalElements: 0,
            totalPages: 0,
            sort: null,
            first: true,
            numberOfElements: 0,
            size: 2000,
            number: 0
        }));

        dialogSpy = jasmine.createSpyObj('dialog', ['hide']);

        spyOn(openlmisModalService, 'createDialog').andReturn(dialogSpy);

        state = $state.get('openlmis.equipment.inventory.add');
    });

    describe('onEnter', function() {

        it('should open modal', function() {
            state.onEnter(openlmisModalService);

            expect(openlmisModalService.createDialog).toHaveBeenCalled();
        });

        it('should reopen the modal if state was reentered', function() {
            //enter the state
            state.onEnter(openlmisModalService);

            expect(openlmisModalService.createDialog.calls.length).toBe(1);

            //reenter the state
            state.onExit();
            state.onEnter(openlmisModalService);

            expect(openlmisModalService.createDialog.calls.length).toBe(2);
        });

    });

    describe('modal', function() {

        var modal;

        beforeEach(function() {
            state.onEnter(
                openlmisModalService,
                equipments
            );

            modal = openlmisModalService.createDialog.calls[0].args[0];
        });

        it('should get a list of equipments', function() {
            expect(modal.resolve.equipments()).toEqual(equipments);
        });

    });

    it('should fetch a list of equipments', function() {
        var result;

        state.resolve.equipments(equipmentService).then(function(equipments) {
            result = equipments;
        });
        $rootScope.$apply();

        expect(result).toEqual(equipments);
        expect(equipmentService.query).toHaveBeenCalled();
    });

    it('onExit should close modal', function() {
        //enter the state
        state.onEnter(openlmisModalService);

        //exit the state
        state.onExit();

        expect(dialogSpy.hide).toHaveBeenCalled();
    });

    it('should require EQUIPMENT_INVENTORY_EDIT right to enter', function() {
        expect(state.accessRights).toEqual([EQUIPMENT_RIGHTS.EQUIPMENT_INVENTORY_EDIT]);
    });

});
