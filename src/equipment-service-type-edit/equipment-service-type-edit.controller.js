/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name equipment-service-type-edit.controller:EquipmentServiceTypeEditController
     *
     * @description
     * Provides methods for Edit equipment service type modal. Allows returning to previous state and editing equipment
     * service type.
     */
    angular
        .module('equipment-service-type-edit')
        .controller('EquipmentServiceTypeEditController', EquipmentServiceTypeEditController);

    EquipmentServiceTypeEditController.$inject = [
        'equipmentServiceType', 'confirmService', 'equipmentServiceTypeService', 'stateTrackerService',
        '$state', 'loadingModalService', 'notificationService'
    ];

    function EquipmentServiceTypeEditController(equipmentServiceType, confirmService,
                                                equipmentServiceTypeService, stateTrackerService,
                                                $state, loadingModalService, notificationService) {
        var vm = this;

        vm.save = save;
        vm.goToPreviousState = stateTrackerService.goToPreviousState;
        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @methodOf equipment-service-type-edit.controller:EquipmentServiceTypeEditController
         * @name equipmentServiceType
         * @type {Object}
         *
         * @description
         * Current equipment service type.
         */
        vm.equipmentServiceType = undefined;

        /**
         * @ngdoc property
         * @methodOf equipment-service-type-edit.controller:EquipmentServiceTypeEditController
         * @name editMode
         * @type {boolean}
         *
         * @description
         * Indicates if equipment service type is already created.
         */
        vm.editMode = undefined;

        /**
         * @ngdoc method
         * @methodOf equipment-service-type-edit.controller:EquipmentServiceTypeEditController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating EquipmentServiceTypeEditController.
         */
        function onInit() {
            vm.equipmentServiceType = equipmentServiceType ? equipmentServiceType : {};
            vm.editMode = !!equipmentServiceType;
        }

        /**
         * @ngdoc method
         * @methodOf equipment-service-type-edit.controller:EquipmentServiceTypeEditController
         * @name save
         *
         * @description
         * Saves the equipment service type after confirm.
         */
        function save() {
            confirmService.confirm(
                vm.editMode ? 'equipmentServiceTypeEdit.save.question' : 'equipmentServiceTypeEdit.create.question',
                vm.editMode ? 'equipmentServiceTypeEdit.save' : 'equipmentServiceTypeEdit.create'
            ).then(function() {
                loadingModalService.open();
                getSavePromise()
                    .then(function() {
                        notificationService.success(vm.editMode ?
                            'equipmentServiceTypeEdit.save.success' : 'equipmentServiceTypeEdit.create.success');
                        stateTrackerService.goToPreviousState();
                    })
                    .catch(function() {
                        loadingModalService.close();
                        notificationService.error(
                            vm.editMode
                                ? 'equipmentServiceTypeEdit.save.failure'
                                : 'equipmentServiceTypeEdit.create.failure'
                        );
                    });
            });
        }

        function getSavePromise() {
            return vm.editMode ?
                equipmentServiceTypeService.update(vm.equipmentServiceType) :
                equipmentServiceTypeService.create(vm.equipmentServiceType);
        }
    }
})();
