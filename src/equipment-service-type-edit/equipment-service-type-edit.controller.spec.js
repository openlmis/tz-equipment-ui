/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('EquipmentServiceTypeEditController', function() {

    beforeEach(function() {
        module('equipment-service-type-edit');

        inject(function($injector) {
            this.$controller = $injector.get('$controller');
            this.$rootScope = $injector.get('$rootScope');
            this.confirmService = $injector.get('confirmService');
            this.$q = $injector.get('$q');
            this.equipmentServiceTypeService = $injector.get('equipmentServiceTypeService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.notificationService = $injector.get('notificationService');
            this.EquipmentServiceTypeDataBuilder = $injector.get('EquipmentServiceTypeDataBuilder');
        });

        this.equipmentServiceType = new this.EquipmentServiceTypeDataBuilder().build();
        this.confirmDeferred = this.$q.defer();
        this.saveDeferred = this.$q.defer();

        spyOn(this.confirmService, 'confirm').andReturn(this.confirmDeferred.promise);
        spyOn(this.stateTrackerService, 'goToPreviousState').andReturn(true);
        spyOn(this.equipmentServiceTypeService, 'update').andReturn(this.saveDeferred.promise);
        spyOn(this.equipmentServiceTypeService, 'create').andReturn(this.saveDeferred.promise);
        spyOn(this.loadingModalService, 'open').andReturn(true);
        spyOn(this.loadingModalService, 'close').andReturn(true);
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');

        this.vm = this.$controller('EquipmentServiceTypeEditController', {
            equipmentServiceType: this.equipmentServiceType
        });
        this.vm.$onInit();
    });

    describe('$onInit', function() {

        it('should expose equipmentServiceType', function() {
            expect(this.vm.equipmentServiceType).toEqual(this.equipmentServiceType);
        });

        it('should expose resolved fields', function() {
            expect(this.vm.editMode).toEqual(true);
        });
    });

    describe('save', function() {

        it('should reload state after successful save', function() {
            this.vm.save();

            this.confirmDeferred.resolve();
            this.saveDeferred.resolve(this.equipmentServiceType);
            this.$rootScope.$apply();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });

        describe('while editing', function() {

            beforeEach(function() {
                this.vm.editMode = true;
            });

            it('should prompt user to confirm save', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentServiceTypeEdit.save.question',
                    'equipmentServiceTypeEdit.save'
                );
            });

            it('should not save equipment service type if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentServiceTypeService.update).not.toHaveBeenCalled();
                expect(this.equipmentServiceTypeService.create).not.toHaveBeenCalled();
            });

            it('should save equipment service type and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentServiceTypeService.update).toHaveBeenCalledWith(this.vm.equipmentServiceType);
                expect(this.equipmentServiceTypeService.create).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment service type was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentServiceType);
                this.$rootScope.$apply();

                expect(this.notificationService.success).toHaveBeenCalledWith('equipmentServiceTypeEdit.save.success');
            });

            it('should show notification if service type save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentServiceTypeEdit.save.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });

        describe('while creating', function() {

            beforeEach(function() {
                this.vm.editMode = false;
            });

            it('should prompt user to save service type', function() {
                this.vm.save();

                expect(this.confirmService.confirm).toHaveBeenCalledWith(
                    'equipmentServiceTypeEdit.create.question',
                    'equipmentServiceTypeEdit.create'
                );
            });

            it('should not save equipment service type if user does not confirm it', function() {
                this.vm.save();

                this.confirmDeferred.reject();
                this.$rootScope.$apply();

                expect(this.equipmentServiceTypeService.create).not.toHaveBeenCalled();
                expect(this.equipmentServiceTypeService.update).not.toHaveBeenCalled();
            });

            it('should save equipment service type and open loading modal after confirm', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.$rootScope.$apply();

                expect(this.equipmentServiceTypeService.create).toHaveBeenCalledWith(this.vm.equipmentServiceType);
                expect(this.equipmentServiceTypeService.update).not.toHaveBeenCalled();
                expect(this.loadingModalService.open).toHaveBeenCalled();
            });

            it('should show notification if equipment service type was saved successfully', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.resolve(this.equipmentServiceType);
                this.$rootScope.$apply();

                expect(this.notificationService.success)
                    .toHaveBeenCalledWith('equipmentServiceTypeEdit.create.success');
            });

            it('should show notification if service type save has failed', function() {
                this.vm.save();

                this.confirmDeferred.resolve();
                this.saveDeferred.reject();
                this.$rootScope.$apply();

                expect(this.notificationService.error).toHaveBeenCalledWith('equipmentServiceTypeEdit.create.failure');
                expect(this.loadingModalService.close).toHaveBeenCalled();
            });
        });
    });

    describe('goToPreviousState', function() {

        it('should redirect to equipment service type list screen', function() {
            this.vm.goToPreviousState();

            expect(this.stateTrackerService.goToPreviousState).toHaveBeenCalled();
        });
    });
});
